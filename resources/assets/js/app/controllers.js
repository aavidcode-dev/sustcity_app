'use strict';

angular.module('lb.controllers', []).
        controller('UserCtrl', function ($scope, $http) {
            $scope.alert = function (type, message) {
                return '<div class="alert alert-' + type + '">\
                            <button type="button" class="close" data-dismiss="alert">\
                                    <span aria-hidden="true">&times;</span>\
                                    <span class="sr-only">Close</span>\
                            </button>\
                            ' + message + '\
                        </div>';
            };
            $scope.user = {};
            $scope.user = {email: $scope.user.email, password: $scope.user.password, user_role: $scope.user.user_role};
            $scope.login = function () {
                $http.post(BASE_URL + 'auth/login', $scope.user)
                        .success(function (response) {
                            if (response.accessGranted) {
                                window.location.href = response.redirect;
                            } else {
                                $(".errors-container .alert").slideUp('fast');
                                $(".errors-container").html($scope.alert('danger', response.error));
                                $(".errors-container .alert").hide().slideDown();
                            }
                        })
                        .error(function (response) {
                            $(".errors-container .alert").slideUp('fast');
                            $(".errors-container").html($scope.alert('danger', response.error));
                            $(".errors-container .alert").hide().slideDown();
                        }
                        );
            };

            $scope.forgotPassword = function () {
                $http.post(BASE_URL + 'auth/forgot-password', {email: $scope.email})
                        .success(function (response) {
                            alert(response.message);
                            if (response.success) {
                                window.location.href = BASE_URL;
                            }
                        });
            };

            /*$scope.logout = function () {
             alert("logout");
             $http.post(BASE_URL + 'auth/logout', $scope.user)
             
             };*/
        }).
        controller('ProductCtrl', function ($scope, $http) {
            $scope.subcategories = {};
            $scope.subsubcategories = {};
            $scope.fetchSubCategories = function () {
                if ($scope.cat_id != '') {
                    $http.get('master/subcategory/list/' + $scope.cat_id).success(function (response) {
                        if (response.success) {
                            $scope.subcategories = response.data;
                        }
                    });
                } else {
                    $scope.subcategories = {};
                    $scope.subsubcategories = {};
                }
            };

            $scope.fetchSubSubCategories = function () {
                if ($scope.sub_id.id != '') {
                    $http.get('master/subsubcategory/list/' + $scope.sub_id.id).success(function (response) {
                        if (response.success) {
                            $scope.subsubcategories = response.data;
                        }
                    });
                } else {
                    $scope.subsubcategories = {};
                }
            };
        })
        .controller('ProductSCartCtrl', function ($scope, $http, $cookieStore, filterFilter) {
            $scope.item = [];
            $scope.addToCart = function (prod_id) {
                $scope.ordered_stock = [];
                $scope.prod_data = {};
                var sizes = {};
                var size_str = '';
                var ordered_sizes = 0;
                var total_stock_ordered = 0;
                $('#prod_modal_' + prod_id + ' input[type="text"]').each(function () {
                    var size_id = $(this).data('size-id');
                    var size_code = $(this).data('size-code');
                    var stock_val = ($(this).val() !== '' ? $(this).val() : 0);
                    sizes[size_id] = stock_val;
                    if (stock_val > 0) {
                        size_str += size_code + ' - ' + stock_val + 'pcs,';
                        ordered_sizes++;
                        total_stock_ordered += parseInt(stock_val);
                    }
                });
                var selectedSeason = $('#selectedSeason').val();
                var selectedStockStatus = $('#selectedStockStatus').val();
                if (selectedSeason == 'current' || selectedStockStatus == 'future') {
                    if (ordered_sizes >= $scope.min_sizes_in_product) {
                        if (total_stock_ordered >= $scope.min_size_quantity) {
                            $scope.addProductToCart(size_str, prod_id, sizes, total_stock_ordered);
                        } else {
                            alert("You should order minimum of " + $scope.min_size_quantity + " quanity for min of " + $scope.min_sizes_in_product + " sizes");
                        }
                    } else {
                        alert("You should order minimum of " + $scope.min_sizes_in_product + " sizes");
                    }
                } else {
                    if (ordered_sizes > 0) {
                        $scope.addProductToCart(size_str, prod_id, sizes, total_stock_ordered);
                    } else {
                        alert("Your order quantity should be more then 0");
                    }
                }
            };

            $scope.addProductToCart = function (size_str, prod_id, sizes, total_stock_ordered) {
                size_str = size_str.substring(0, size_str.length - 1);

                var prod_data_id = $('#prod_details_' + prod_id);
                if ($cookieStore.get('orders') != null) {
                    $scope.ordered_stock = $cookieStore.get('orders');
                }
                var prod_dis_type = $(prod_data_id).find('.dis_type').val();
                var prod_dis_val = $(prod_data_id).find('.dis_val').val();
                var total_price = parseFloat($(prod_data_id).find('.price').text()).toFixed(2);
                var final_price = 0;
                var discount_price = parseFloat(prod_dis_val);

                discount_price = (prod_dis_type == 'amount') ? discount_price : total_price / 100 * discount_price;
                final_price = (total_price - discount_price) * total_stock_ordered;

                var prod = {
                    image_url: $(prod_data_id).find('img').attr('src'),
                    ear_code: $(prod_data_id).find('.ean_code').text(),
                    style_code: $(prod_data_id).find('.style_code').text(),
                    cat_name: $(prod_data_id).find('.cat_name').text(),
                    brick_name: $(prod_data_id).find('.brick_name').text(),
                    total_qty: total_stock_ordered,
                    price: total_price,
                    discount_price: discount_price,
                    final_price: final_price,
                    prod_desc: $(prod_data_id).find('.prod_desc').val()
                };

                $scope.prod_data.id = prod_id;
                $scope.prod_data.prod = prod;
                $scope.prod_data.sizes = sizes;
                $scope.prod_data.size_str = size_str;

                if ($scope.isProductExists(prod_id)) {
                    $scope.removeCartItem(prod_id);
                    $scope.ordered_stock = $cookieStore.get('orders');
                }

                $scope.ordered_stock.push($scope.prod_data);
                $cookieStore.put('orders', $scope.ordered_stock);
                $scope.getTotalCartItems();
                $scope.item[prod_id] = size_str;
                console.log($scope.ordered_stock);
                $('#prod_modal_' + prod_id).modal('hide');
            };

            $scope.removeCartItem = function (prod_id) {
                var cartItems = $cookieStore.get('orders');
                var count = 0;
                var index = 0;
                angular.forEach($cookieStore.get('orders'), function (item, key) {
                    if (item.id == prod_id) {
                        index = count;
                    }
                    count++;
                });
                cartItems.splice(index, 1);
                $cookieStore.put('orders', cartItems);
                $scope.getTotalCartItems();
            };

            $scope.getProductFromCart = function (prod_id) {
                var size_str = '';
                angular.forEach($cookieStore.get('orders'), function (item, key) {
                    if (item.id == prod_id) {
                        size_str = item.size_str;
                    }
                });
                return size_str;
            };

            $scope.showProdModal = function (prod_id) {
                var ordered_stock = $cookieStore.get('orders');
                if (ordered_stock != null) {
                    angular.forEach(ordered_stock, function (item, key) {
                        if (item.id == prod_id) {
                            $('#selectedSizes').show();
                            angular.forEach(item.sizes, function (stock, size_id) {
                                $('#prod_modal_' + prod_id).find('[name="ordered_stock[' + size_id + ']"]').val(stock);
                            });
                        }
                    });
                }
                $('#prod_modal_' + prod_id).modal();
            };

            $scope.checkStock = function ($event) {
                var target = angular.element($event.target);
                var val = target.val();
                var stock = target.data('size-stock');
                if (val != '' || val > 0) {
                    if (val > stock) {
                        alert("You can't enter more than " + stock + " quantity for this size");
                        target.val("");
                    }
                }
            };

            $scope.clearSearch = function () {
                $('[name="cat_id"]').val('');
                $('[name="sub_id"]').val('');
                $('[name="sub_sub_id"]').val('');
                $('[name="colour_id"]').val('');
                $('[name="ean_code"]').val('');
                $('[name="style_code"]').val('');
                $('[name="b_id"]').val('');
                $('[name="sort_by"]').val('');
                $('[name="fit_id"]').val('');
                $('[name="f_id"]').val('');
                $('[name="w_id"]').val('');
                $('[name="range_1"]').val('');
            };

        })
        .controller('ChekoutCtrl', function ($scope, $http, $cookieStore) {
            //$cookieStore.put('orders', null);
            $scope.cartItems = $cookieStore.get('orders');

            $scope.removeCartItem = function (prod_id) {
                $scope.cartItems = $cookieStore.get('orders');
                var count = 0;
                var index = 0;
                angular.forEach($cookieStore.get('orders'), function (item, key) {
                    if (key == prod_id) {
                        index = count;
                    }
                    count++;
                });
                $scope.cartItems.splice(index, 1);
                $cookieStore.put('orders', $scope.cartItems);
                $scope.getTotalCartItems();
                $scope.getCartTotal();
            };

            $scope.submitOrder = function () {
                var min_order_quantity = $('#min_order_quantity').val();
                if ($scope.cart_qty < min_order_quantity) {
                    if (!confirm("The quantity is less than " + min_order_quantity + "pcs, the freight charges will be applicable. Do you want to continue?")) {
                        return;
                    }
                }

                $scope.loader(true);
                var data = $cookieStore.get('orders');
                $http.post('customer/order', data).success(function (response) {
                    if (response.success) {
                        $cookieStore.put('orders', null);
                        alert('Your order has been submitted successfully');
                        window.location.href = "customer/products/ready/current";
                        $scope.loader(false);
                    }
                }).error(function (response) {
                    alert('error: ' + response.error);
                });
            };

            $scope.cart_total_price = 0;
            $scope.cart_discount = 0;
            $scope.cart_qty = 0;
            $scope.cart_final_price = 0;
            $scope.getCartTotal = function () {
                var total = 0;
                angular.forEach($scope.cartItems, function (citem, key) {
                    total += citem.prod.final_price;
                    $scope.cart_qty += citem.prod.total_qty;
                });
                $scope.cart_total_price = total;
                $scope.cart_final_price = total;
                $http.post('customer/order/discount-price', {amount: total}).success(function (response) {
                    if (response.dis_type != undefined) {
                        var type = response.dis_type;
                        var discount_price = response.dis_val;
                        $scope.cart_discount = (type == 'amount') ? discount_price : total / 100 * discount_price;
                        $scope.cart_final_price = total - $scope.cart_discount;
                    } else {

                    }
                });
            };
        })
        .controller('MainCtrl', function ($scope, $http, $cookieStore) {

            $scope.totalcartitems = 0;

            $scope.getTotalCartItems = function () {
                if ($cookieStore.get('orders') != null) {
                    var ordered_stock = $cookieStore.get('orders');
                    var totalItems = Object.keys(ordered_stock).length;
                    $scope.totalcartitems = totalItems;
                }
            };

            $scope.isProductExists = function (prod_id) {
                var flag = false;
                angular.forEach($cookieStore.get('orders'), function (item, key) {
                    if (item.id == prod_id) {
                        flag = true;
                    }
                });
                return flag;
            };

            $scope.loader = function (show) {
                if (show) {
                    $(".loader").delay(700).fadeIn();
                    $("#pageloader").delay(800).fadeIn("slow");
                } else {
                    $(".loader").delay(700).fadeOut();
                    $("#pageloader").delay(800).fadeOut("slow");
                }
            };

        })
        .controller('SlabCtrl', function ($scope, $http) {
            $scope.slabs = [];

            $scope.removeSlab = function (index) {
                $scope.slabs.splice(index, 1);
            };

            $scope.addSlab = function () {
                var slab = {};
                var len = $scope.slabs.length;
                if (len > 0) {
                    slab.from_price = parseInt($scope.slabs[len - 1].to_price) + 1;
                }
                $scope.slabs.push(slab);
            };

            $scope.fetchSlabs = function () {
                $http.get('master/discount/price/slabs').success(function (response) {
                    $scope.slabs = response;
                });
            };

            $scope.changeAmount = function (slab, $index) {
                if (slab.to_price > 0 && slab.to_price > slab.from_price) {
                    var len = $scope.slabs.length;
                    if (len - 1 > $index) {
                        var amount = parseInt(slab.to_price) + 1;
                        $scope.slabs[$index + 1].from_price = amount;
                    }
                } else {
                    slab.to_price = '';
                    alert("Invalid to amount entered");
                }
            };

            $scope.validateToAmount = function (slab) {
                if (slab.from_price < 0 || slab.from_price > slab.to_price) {
                    slab.from_price = '';
                    alert("Invalid from amount entered");
                }
            };

            $scope.saveSlabs = function (form) {
                if ($('#' + form).validate()) {
                    $http.post('master/discount/price/save', $scope.slabs).success(function (response) {
                        if (response.success) {
                            $('html,body').animate({scrollTop: 0}, 600);
                            alert('Details has been saved successfully');
                        }
                    });
                }
            };
        })

        .controller('ReportCtrl', function ($scope, $http) {
            $scope.search = {};
            $scope.customers = {};
            $scope.currentPage = 1;
            $scope.searchCustomer = function (page) {
                $http.post('master/customer/search?req=api&page=' + page, $scope.search).success(function (response) {
                    $scope.customers = response.data;
                    $scope.currentPage = response.current_page;
                    $scope.pageSize = response.per_page;
                    $scope.total = response.total;
                });
            };
        })

        .controller('DiscountProductCtrl', function ($scope, $http) {
            $scope.search = {};
            $scope.products = {};
            $scope.currentPage = 1;
            $scope.searchDiscount = function (page) {
                $http.post('master/discount/product/search?page=' + page, $scope.search).success(function (response) {
                    $scope.products = response.data;
                    $scope.currentPage = response.current_page;
                    $scope.pageSize = response.per_page;
                    $scope.total = response.total;
                });
            };

            $scope.saveDiscountProduct = function (form) {
                $scope.dis_products = [];
                angular.forEach($scope.products, function (value, key) {
                    if (value.discount != null && value.discount.dis_value != undefined && value.discount.dis_value != '') {
                        var discount = {prod_id: value.id, dis_value: value.discount.dis_value, dis_type: value.discount.dis_type}
                        $scope.dis_products.push(discount);
                    }
                });
                $http.post('master/discount/product/save', $scope.dis_products).success(function (response) {
                    if (response.success) {
                        $('html,body').animate({scrollTop: 0}, 600);
                        alert('Details has been saved successfully');
                    }
                });
            };
        });

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard-Sustcity</title>
    
    <script src="{!! asset('public/js/ckeditor.js') !!}"></script>   
    <!-- <script src="{!! asset('public/js/sample.js') !!}"></script> --> 
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    
    <!-- Bootstrap Core Css -->
    <link href="{!! asset('public/admin-material/plugins/bootstrap/css/bootstrap.css')!!}" rel="stylesheet">
   <link href="http://www.aavidcode.in/assignments/mahesh/mahesh_laravel_task/public/fonts/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="{!! asset('public/admin-material/plugins/node-waves/waves.css')!!}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{!! asset('public/admin-material/plugins/animate-css/animate.css')!!}" rel="stylesheet" />

    <!-- Preloader Css -->
    <link href="{!! asset('public/admin-material/plugins/material-design-preloader/md-preloader.css')!!}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{!! asset('public/admin-material/plugins/morrisjs/morris.css')!!}" rel="stylesheet" />
      <link href="{!! asset('public/admin-material/plugins/bootstrap-select/css/bootstrap-select.css')!!}" rel="stylesheet" />
    <link href="{!! asset('public/admin-material/plugins/waitme/waitMe.css')!!}" rel="stylesheet" />
    
    <!-- Custom Css -->
    <link href="{!! asset('public/admin-material/css/style.css')!!}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{!! asset('public/admin-material/css/themes/all-themes.css')!!}" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>    
<!--    <script src="http://www.aavidcode.in/assignments/mahesh/mahesh_laravel_task/public/js/app.js"></script>-->         
    <link rel="stylesheet" type="text/css" href="http://tongaa.com/assets/tonga-content/bootstrap-datetimepicker.min.css">
         
    <style>       
        .fa-btn {
            margin-right: 6px;
        }
        .nav-txt-color{
            color: #FFF;
        }
/*        .material-icons{
            float:right !important;
        }*/
    </style>
</head>
<body class="theme-red">    
    <div class="wrapper">
               @include('layouts.header')
               @include('layouts.navbar')
               @yield('content')
               @include('layouts.footer')
    </div>  
</body>
</html>

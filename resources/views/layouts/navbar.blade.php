<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
<!--            <div class="user-info">
               
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}</div>
                    <div class="email">{{Auth::user()->email}}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>                                               
                            <li><a href="{{ url('auth/logout') }}"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul> 
                    </div>
                </div>
            </div>-->
            <!-- #User Info -->
            <!-- Menu -->

            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="{{ url('/') }}">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>   
                    <li class="utility_category">
                        
                        <a href="{{ url('/utility_category') }}" >
                            <i class="material-icons">album</i>
                            <span>House Category</span>
                        </a>                                
                    </li>
                     <li class="utility_type">
                         <a href="{{ url('/utility_type') }}" >   
                              <i class="material-icons">album</i>
                            <span>Utility Type</span>
                        </a>                                
                    </li>
                    <li class="utility">
                        <a href="{{ url('/utility') }}" >
                             <i class="material-icons">album</i>
                            <span>Utility</span>
                        </a>                                
                    </li>
                    <li class="temperature">
                         <a href="{{ url('/temperature') }}" > 
                              <i class="material-icons">album</i>
                            <span>Temperature</span>
                        </a>                                
                    </li>
                     <li class="city">
                        <a href="{{ url('/city') }}" > 
                             <i class="material-icons">album</i>
                            <span>City</span>
                        </a>                                
                    </li>
                    <li class="housedetails">
                        <a href="{{ url('/housedetails') }}" >
                             <i class="material-icons">album</i>
                            <span>Utility House Details</span>
                        </a>                                
                    </li>
                     <li class="electricconsumption"> 
                        <a href="{{ url('/electricconsumption') }}" >  
                             <i class="material-icons">album</i>
                            <span>Electricity Consumption</span>
                        </a>                                 
                    </li> 
                    <li class="discontinue-paper-bill"> 
                        <a href="{{ url('/discontinue-paper-bill') }}" >  
                             <i class="material-icons">album</i>
                            <span>Discontinued Paper Bill List</span>
                        </a>                                 
                    </li>  
                     <li class="water-comsumption">
                         <a href="{{ url('/water-comsumption') }}" > 
                              <i class="material-icons">album</i>
                            <span>Water Consumption</span>
                        </a>                                
                    </li>
                    <li class="top-credits">
                         <a href="{{ url('/top-credits') }}" > 
                              <i class="material-icons">album</i>
                            <span>Top SR Credits</span>
                        </a>                                
                    </li> 
                    <li class="user-details">
                         <a href="{{ url('/user-details') }}" > 
                              <i class="material-icons">album</i>
                            <span>App User Details</span>
                        </a>                                
                    </li>

                    <li class="faq">
                         <a href="{{ url('/faq') }}" > 
                              <i class="material-icons">album</i>
                            <span>FAQ</span>
                        </a>                                
                    </li >                            
                    <li class="contact_us">
                         <a href="{{ url('/contact_us') }}" > 
                              <i class="material-icons">album</i>
                            <span>Contact us</span>
                        </a>                                
                    </li> 
                    <li class="advertisement">
                         <a href="{{ url('/advertisement') }}" > 
                              <i class="material-icons">album</i>  
                            <span>Advertisement</span> 
                        </a>                                
                    </li>
                     <li class="theft">
                         <a href="{{ url('/theft') }}" > 
                              <i class="material-icons">album</i>  
                            <span>Theft Reports</span> 
                        </a>                                
                    </li>
                     <li class="referal">
                         <a href="{{ url('/referal') }}" > 
                              <i class="material-icons">album</i>  
                            <span>Referals Friends</span> 
                        </a>                                
                    </li>
                     <li class="about_us">
                         <a href="{{ url('/about_us') }}" > 
                              <i class="material-icons">album</i>  
                            <span>About us</span> 
                        </a>                                
                    </li>
                     <li class="rewards">
                         <a href="{{ url('/rewards') }}" > 
                              <i class="material-icons">album</i>  
                            <span>Rewards</span> 
                        </a>                                
                    </li>
                     <li class="bill_notification">
                         <a href="{{ url('/bill_notification') }}" > 
                              <i class="material-icons">album</i>  
                            <span>Bill Notification</span> 
                        </a>                                
                     </li>
                    <li class="graph_details">
                         <a href="{{ url('/graph_details') }}" > 
                              <i class="material-icons">album</i>  
                            <span>GraphDetails</span> 
                        </a>                                
                    </li>
                            
                     <li class="know_more">
                         <a href="{{ url('/know_more') }}" > 
                              <i class="material-icons">album</i>  
                            <span>Know More</span> 
                        </a>                                
                    </li>

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
<!--            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.3
                </div>
            </div>-->
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        
    </section>
    <script type="text/javascript">
               
        $(document).ready(function(){
            var temp = new Array();
            temp = window.location.pathname.split('/');
            if(temp[2] != ''){
                $('.list li.active').removeClass('active');
                $("."+temp[2]).attr('class', temp[2]+' active');
            }
            
        });
    </script>
<footer class="main-footer">
    <div class="pull-right hidden-xs">

    </div>
    <strong>Copyright &copy; 2016</strong> All rights reserved.
</footer>
<!-- jQuery 2.1.4 -->


 <!-- Jquery Core Js -->
    <!--<script src="{!! asset('public/admin-material/plugins/jquery/jquery.min.js') !!}"></script>-->

    <!-- Bootstrap Core Js -->
    <script src="{!! asset('public/admin-material/plugins/bootstrap/js/bootstrap.js') !!}"></script>

    <!-- Select Plugin Js -->
    <script src="{!! asset('public/admin-material/plugins/bootstrap-select/js/bootstrap-select.js') !!}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{!! asset('public/admin-material/plugins/jquery-slimscroll/jquery.slimscroll.js') !!}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{!! asset('public/admin-material/plugins/node-waves/waves.js') !!}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{!! asset('public/admin-material/plugins/jquery-countto/jquery.countTo.js') !!}"></script>

    <!-- Morris Plugin Js -->
    <script src="{!! asset('public/admin-material/plugins/raphael/raphael.min.js') !!}"></script>
    <script src="{!! asset('public/admin-material/plugins/morrisjs/morris.js') !!}"></script>

    <!-- ChartJs -->
    <script src="{!! asset('public/admin-material/plugins/chartjs/Chart.bundle.js') !!}"></script>

    <!-- Flot Charts Plugin Js -->
     <script src="{!! asset('public/admin-material/plugins/flot-charts/jquery.flot.js') !!}"></script>
    <script src="{!! asset('public/admin-material/plugins/flot-charts/jquery.flot.resize.js') !!}"></script>
    <script src="{!! asset('public/admin-material/plugins/flot-charts/jquery.flot.pie.js') !!}"></script>
    <script src="{!! asset('public/admin-material/plugins/flot-charts/jquery.flot.categories.js') !!}"></script>
    <script src="{!! asset('public/admin-material/plugins/flot-charts/jquery.flot.time.js') !!}"></script> 

    <!-- Sparkline Chart Plugin Js -->
    <script src="{!! asset('public/admin-material/plugins/jquery-sparkline/jquery.sparkline.js') !!}"></script>
    
     <script src="{!! asset('public/admin-material/plugins/autosize/autosize.js') !!}"></script>
   
    <script src="{!! asset('public/admin-material/plugins/momentjs/moment.js') !!}"></script>
   
   <!-- Custom Js -->
    <script src="{!! asset('public/admin-material/js/admin.js') !!}"></script>
    <script src="{!! asset('public/admin-material/js/pages/index.js') !!}"></script>
    <script src="{!! asset('public/admin-material/js/pages/forms/basic-form-elements.js') !!}"></script>
    */ ?>
    <!-- Demo Js -->
    <script src="{!! asset('public/admin-material/js/demo.js') !!}"></script>

    
    <!--<script type="text/javascript" src="{!! asset('public/js/ckeditor.js') !!}"></script>-->
    <!--<script type="text/javascript" src="{!! asset('public/js/tinymce.js') !!}"></script>-->
    
    
    <!--<script type="text/javascript" src="{!! asset('public/js/editors.js') !!}"></script>-->
    <script type="text/javascript" src="{!! asset('public/js/jquery.validate.min.js') !!}"></script>
    
 <script type="text/javascript" src="http://tongaa.com/assets/tonga-content/moment.js"></script>
<script type="text/javascript" src="http://tongaa.com/assets/tonga-content/bootstrap-datetimepicker.min.js"></script>
@yield('pageFooterSpecificPlugin')

@yield('pageFooterSpecificJS')
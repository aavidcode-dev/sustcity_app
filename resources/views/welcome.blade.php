@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->
<style>
    section.content{
        margin-top: 0px !important;
    }
    .box-cnt{
            background: #FFF;
    height: 100px;
    margin-left: 20px;
    padding: 5px;
    }
    </style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Dashboard
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <!-- TABLE: LATEST ORDERS -->
               
                <div class="box box-info">
                   
                    <div class="box-body">                     
                        <h3><label>Dashboard</label></h3><br/>
                        
                        <div class="row clearfix">
                        <a href='{{URL('/user-details')}}' style="cursor:pointer;"> 
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                             
                            <div class="info-box bg-pink hover-expand-effect" style="cursor:pointer;">
                              
                                <div class="icon">
                                     
                                    <i class="material-icons">person_add</i>
                                   
                                </div>
                                  
                              
                                <div class="content">
                                    <div class="text">TOTAL USERS</div>
                                    <div class="number count-to" data-from="0" data-to="{{$count['user']}}" data-speed="15" data-fresh-interval="20">{{$count['user']}}</div>
                                </div>
                                  
                            </div>
                            
                        </div>
                           </a>    
                  <a href='{{URL('/housedetails')}}' style="cursor:pointer;">            
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect" style="cursor:pointer;">
                        <div class="icon">
                            <i class="material-icons">home</i>
                        </div>
                        <div class="content">
                            <div class="text">HOUSE COUNT</div>
                            <div class="number count-to" data-from="0" data-to="{{$count['housedetails']}}" data-speed="1000" data-fresh-interval="20">{{$count['housedetails']}}</div>
                        </div>
                    </div>
                </div>
                 </a>           
                  <a href='{{URL('/utility')}}' style="cursor:pointer;">           
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect" style="cursor:pointer;">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>
                        <div class="content">
                            <div class="text">UTILITY COUNT</div>
                            <div class="number count-to" data-from="0" data-to="{{$count['utility']}}" data-speed="1000" data-fresh-interval="20">{{$count['utility']}}</div>
                        </div>
                    </div>
                </div>
                      </a>  
<!--                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">NEW VISITORS</div>
                            <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20">1225</div>
                        </div>
                    </div>
                </div>-->
            </div>
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
               
            </div><!-- /.col -->
        </div>

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@endsection



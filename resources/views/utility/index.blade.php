@extends('layouts.app')

@section('content')
<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Utility</h2>
                         <div class="box-header with-border">
                        <div class="row">
                            <div class="col-lg-10">
                               
                            </div>
                            <div class="col-lg-2 text-right">
                                  <a class="btn btn-primary pull-right" href="<?php echo 'utility/create'; ?>">Add Utility</a>
                            </div>
                        </div>
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                 <form id="create_form" name="create_form" class="form-horizontal" action="" method="post">
                        <div class="box-body">
                            <div class="pull-right"></div>
                            <div class="col-lg-2 pull-right" style="margin-top: 20px; padding-left: 0">
                               
                            </div>
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table no-margin table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>    
                                                <th>Utility Name</th>     
                                                <th>Utility Type</th> 
                                                <th>Status</th> 
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                         
                                            <?php $count =0; ?>
                                            @foreach ($data as $row) 
                                            <tr>
                                                <td>{{++$count}}</td>
                                                <td>{{$row->utility_name}}</td> 
                                                <td>{{$row->utility_type->name}}</td>
                                                 <td>
                                                    <?php if($row->status == 1){  ?>
                                                        <span class="label label-success">Active</span>
                                                    <?php } else{ ?>
                                                        <span class="label label-danger">De-Active</span>
                                                    <?php } ?>
                                                </td>
                                                <td> 
                                                    <a class="btn btn-warning" href="<?php echo 'utility/' . $row->id ?>/edit">Edit</a>
                                                    <?php if($row->status == 1){  ?>   
                                                    <a href="#my_modal" data-toggle="modal" data-book-id="{{$row->id}}"><i class="material-icons">clear</i></a>                                                    
                                                    <?php } else{ ?>
                                                    <a href="#my_modal1" data-toggle="modal" data-book-id="{{$row->id}}"><i class="material-icons">done</i></a>                                                     
                                                    <?php } ?> 
                                                </td>
                                            </tr>
                                            @endforeach                                     
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

@endsection

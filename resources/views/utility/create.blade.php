@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Utility</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                  {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'utility','enctype' => 'multipart/form-data']) !!}
                        <div class="box-body">                                                                                                                                                          
                                <div class="body">                            
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="utility_name" id="utility_name" placeholder="Utility Name" value="{{old('utility_name')}}" />
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('utility_name') }}</p>
                                            </div>
                                            <div class="form-group">
                                                 <label>Utility Type</label>
                                                <select class="form-control required" name="utility_type_id" id="name">
                                                    <option value=""></option>
                                                    @foreach($data1 as $type)
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                    @endforeach
                                                </select>
                                                  <p style="color:red;">{{ $errors->first('utility_type_id') }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Premium</label>
                                                <select class="form-control required" name="premium" id="premium">
                                                    <option value=""></option>                                                    
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                                   <p style="color:red;">{{ $errors->first('premium') }}</p>
                                            </div>
                                            <div class="form-group">
                                            <h2 class="card-inside-title">Categories</h2>
                                            <div class="demo-checkbox"> 
                                                <?php $i=1; ?>
                                                @foreach($data2 as $type)  
                                                <input type="checkbox" name="cat_id[]" value="{{$type->id}}" id="md_checkbox_<?php echo $i; ?>" class="filled-in chk-col-grey" />
                                                    <label for="md_checkbox_<?php echo $i++; ?>">{{$type->name}}</label>  
                                                @endforeach  
                                                <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                            </div>  
                                            <p style="color:red;">{{ $errors->first('cat_id') }}</p>
                                            </div>
                                        </div>
                                    </div>                                   
                                </div>                                                   
                            <br/><br/>
                            
                          
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Add Utility">
                            </div>
                        </div><!-- /.box-body -->
                       {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();   
    
   $('[name="utility_type_id"]').val('{{old("utility_type_id")}}');
   $('[name="premium"]').val('{{old("premium")}}');
   $('[name="cat_id"]').val('{{old("cat_id")}}');
});
</script>  
@endsection





@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Utility
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12">
                    <!-- TABLE: LATEST ORDERS -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <p style="color:#32CD32;"><?php echo Session::get('message'); ?></p><br/>
                           
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->                        
                            <div class="box-body">
                                <div class="col-md-2">
                                    <label>Utility Name:</label>
                                </div>
                                <div class="col-md-10">
                                   <?= $row->utility_name; ?><br/>                             
                                </div>                           
                                <div class="col-md-12">
                                    <a class="btn btn-sm btn-success pull-right" id="edit-btn" href="{{ url('utility') }}">Back</a>                                   
                                </div>
                            </div><!-- /.box-body -->                        
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection


@section('pageFooterSpecificPlugin')
@endsection


@section('pageFooterSpecificJS')

<script type="text/javascript" src="{{ asset('public/js/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function()
{
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();
});
</script>
@endsection
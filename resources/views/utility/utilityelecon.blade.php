@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Utility Electric Consumption
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <p style="color:#32CD32;"><?php echo Session::get('message'); ?></p><br/>
                        <h3 class="box-title">Add</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                       {!! Form::open(['name'=>'upload-excel', 'id'=>'upload-excel','files'=>true, 'method'=>'POST', 'url'=>'utility/uploadexcelSave', 'class'=>'form-horizontal']) !!}
                        <div class="box-body">
                            <div class="col-md-2">Upload Excel:</div>
                            <div class="col-md-10"> 
                                <input type="file" class="form-control required" name="uploadexcel" id="uploadexcel"><br/> 
                            </div> 
                            <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                            <p style="color:red;margin: 0 0 10px 18%;">{{ $errors->first('uploadexcel') }}</p>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Upload">
                            </div>
                        </div><!-- /.box-body -->
                       {!! Form::close() !!}
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div>

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@endsection




@section('pageFooterSpecificPlugin')
@endsection


@section('pageFooterSpecificJS')


@endsection


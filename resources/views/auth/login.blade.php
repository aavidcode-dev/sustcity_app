<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Suscity_App</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>                                 
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
     
    <!-- Bootstrap Core Css -->
    <link href="{!! asset('public/admin-material/plugins/bootstrap/css/bootstrap.css')!!}" rel="stylesheet">
   
    <!-- Waves Effect Css -->
    <link href="{!! asset('public/admin-material/plugins/node-waves/waves.css')!!}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{!! asset('public/admin-material/plugins/animate-css/animate.css')!!}" rel="stylesheet" />

    <!-- Preloader Css -->
    <link href="{!! asset('public/admin-material/plugins/material-design-preloader/md-preloader.css')!!}" rel="stylesheet" />

    <!-- Morris Chart Css-->
   
      <link href="{!! asset('public/admin-material/plugins/bootstrap-select/css/bootstrap-select.css')!!}" rel="stylesheet" />
 
    
    <!-- Custom Css -->
  

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    

        <!-- Styles -->
        <style>
            .dialog-login{
                    border: 1px solid #EEEEEE;
                    border-radius: 2px;
                    padding-bottom: 45px;
                    padding-left: 4%;
            }
            .title{
                    font-family: "haptik","Helvetica Neue",Arial,sans-serif;
                    color: #1B2432;
                    line-height: 45px;
                    font-size: 40px;
                    padding: 25px 30px 0px;
                    text-transform: none;
            }         
            #input-txt{
                    background-color: #fff;
                    border: 1px solid #CDCFD2;
                    border-radius: 3px;
                    color: #1B2432;
                    height: 40px;
                    margin: 0;
                    padding: 0 0 0 10px;
                    width: 100%;
            }
            .l-w-txt{
                font-weight: lighter;
                   color: #898E95;
            }
            .form-input{
                margin-top: 20px;
            }
            #grad {
			height: 100px;
			background: red; /* For browsers that do not support gradients */
			background: -webkit-linear-gradient(-90deg, red, yellow); /* For Safari 5.1 to 6.0 */
			background: -o-linear-gradient(-90deg, red, yellow); /* For Opera 11.1 to 12.0 */
			background: -moz-linear-gradient(-90deg, red, yellow); /* For Firefox 3.6 to 15 */
			background: linear-gradient(-90deg, red, yellow); /* Standard syntax (must be last) */
			}
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12" align="center">
                    <label style="font-size:18px;">Suscity</label>
                </div>
            </div>
            <div class="row">              
                <div class="col-sm-4 col-md-4"></div>
                <div class="col-sm-4 col-md-4 dialog-login" align="center">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
							{{ csrf_field() }}
						<label class="title">Login</label>                    
						<div class="col-sm-12 col-md-12 form-input form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label class="l-w-txt pull-left">EMAIL ADDRESS</label>
							<input type="text" name="email" id="input-txt" value="{{ old('email') }}">
							 @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                             @endif
						</div>                    
						<div class="col-sm-12 col-md-12 form-input form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							 <label class="l-w-txt pull-left">PASSWORD</label>
							 <input type="password" name="password" id="input-txt">
							 @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
						</div>
                                    <div class="col-md-12">
                                        <div class="checkbox pull-left">
                                            <label>
                                                <input type="checkbox" name="remember"> Remember Me
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">                                       
                                      <input type="submit" class="btn pull-right" value="Log In" style="border-radius: 2px; background: #1AAFD0 !important; border-color: #1AAFD0 !important;     margin-right: 18px; color: #fff;">
                                            <a class="btn btn-link pull-left"  data-toggle="modal" data-target="#myModal">Forgot Your Password?</a>                                   
                                    </div>
                                     <div class="col-md-12">             
                                                 <p style="color:#32CD32; margin: 20px 0px 0px 0px;"><?php echo Session::get('message'); ?></p>
                                     </div>
					</form>
                </div>   
                <div class="col-sm-4 col-md-4"></div>                                  
            </div>            
        </div>  
        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Forgot Password</h4>
      </div>
         {!! Form::open(['name'=>'forgotpassword', 'id'=>'forgotpassword', 'method'=>'POST', 'url'=>'forgotpassword']) !!} 
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-sm-4"> 
                        <label>Registered Email Id:</label>
                    </div>
                    <div class="col-md-8 col-sm-8"> 
                        <input type="text" name="email" id="input-txt" value="{{ old('email') }}">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" value="Forgot Password" class="btn btn-primary" id="forgot-password">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          {!! Form::close() !!}
    </div>

  </div>
</div>
       
          <script src="{!! asset('public/admin-material/plugins/jquery/jquery.min.js') !!}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{!! asset('public/admin-material/plugins/bootstrap/js/bootstrap.js') !!}"></script>
    
    <script type="text/javascript">
$(document).ready(function()
{    
    $("#forgot-password").removeAttr('disabled');
    var form = $("[name=forgotpassword]"); 
    form.validate();   
});
</script>  
    </body>
</html>


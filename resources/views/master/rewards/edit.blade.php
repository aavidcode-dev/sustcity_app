@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Rewards</h2>
                         <div class="box-header with-border">                      
                            <p style="color:#FFF; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                            <p style="color:#FFF; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                         </div><!-- /.box-header --> 
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="    background: #FFF; padding-bottom: 29px; padding-top: 29px;">
                                 {!! Form::model($row, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'rewards/'.$row->id]) !!}
                                <div class="row clearfix">
                                    <div class="col-sm-12">

                                            <div class="col-md-2 col-sm-2"><label>Rewards Name:</label></div>
                                            <div class="col-md-10 col-sm-10"> 
                                                 <div class="form-group">
                                              <div class="form-line">
                                                 <input type="text" class="form-control required" name="name" value="{{$row->name}}"> 
                                            </div> 
                                                 <p style="color:red;">{{ $errors->first('name') }}</p>
                                            </div>
                                            </div>

                                             <div class="col-md-2 col-sm-2"><label>Value:</label></div>
                                            <div class="col-md-10 col-sm-10">
                                                <div class="form-group">
                                                <div class="form-line">
                                                     <input type="text" class="form-control required" name="value" value="{{$row->value}}"> 
                                                </div>
                                                     <p style="color:red;">{{ $errors->first('name') }}</p>
                                                </div>
                                            </div>

                                             <div class="col-md-2 col-sm-2"><label>Rewards Code:</label></div>
                                            <div class="col-md-10 col-sm-10">
                                             <div class="form-group">
                                                <div class="form-line">
                                                     <input type="text" class="form-control required" name="code" value="{{$row->code}}"> 
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('code') }}</p>
                                            </div>
                                        </div>
                                             <input type="hidden" class="form-control required" name="r_id" value="{{$row->id}}"> 
                                         <div class="col-md-11 col-sm-11">
                                            <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Edit Rewards">
                                        </div>
                                         <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/rewards')}}" class="btn btn-warning">Cancel</a> 
                                        </div>
                                    </div>
                                </div>   
                                   {!! Form::close() !!}                                                                                
                            <br/><br/>                                                      
                          
                        
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=create]");
    form.validate();   
   
});
</script>  
@endsection




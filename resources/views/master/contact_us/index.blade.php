@extends('layouts.app')

@section('content')

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header" style="padding: 35px;">
                            <div class="col-lg-6">   
                                <h2>Contact us List</h2>
                            </div>
                            <div class="col-lg-4 text-right">    
                            </div>
                             <div class="col-lg-2 text-right">                                    
                                     <a class="btn btn-warning pull-right" href="<?php echo 'contact_us/excel_report'; ?>">Download Data</a>
                                </div>
                         <div class="box-header with-border">                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="row">
                           <div class="col-md-6 col-lg-6 col-sm-6" style="margin-top: 0px; margin-bottom: 0px;">
                                <div class="pull-right">{!!$data->appends($post)->render()!!}</div>
                           </div>
                           <div class="col-md-2 col-lg-2 col-sm-2"> </div>
                           <div class="col-md-2 col-lg-2 col-sm-2">
                               <select name="select_status" class="form-control" id="select_status">
                                    <option value="">Select Status</option>
                                    <option value="1">Resolved</option> 
                                    <option value="0">Un-Resolved</option>
                                </select>
                           </div>
                            <div class="col-md-2 col-lg-2 col-sm-2 pull-right" style="margin-top: 0px; padding-left: 0">
                                <select name="limit" class="form-control" id="limit">
                                    <option value="">View All Data</option>
                                    <option value="50">50</option> 
                                    <option value="100">100</option>   
                                    <option value="150">150</option> 
                                    <option value="200">200</option>
                                </select>
                            </div>
                        </div> 
                        <div class="body">
                            <div class="table-responsive">
                                 <form id="create_form" name="create_form" class="form-horizontal" action="" method="post">
                        <div class="box-body">
                            <div class="pull-right"></div>                         
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table no-margin table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>    
                                                <th>House Code</th>                                         
                                                <th>Message</th> 
                                                <th>Status</th>                               
                                            </tr>
                                        </thead>
                                        <tbody>                                         
                                            <?php $count =0; ?>
                                            @if(!empty($data[0]))
                                             @foreach ($data as $row) 
                                            <tr>
                                                <td>{{++$count}}</td>  
                                                <td>{{$row->h_code}}</td>
                                                <td>{{$row->message}}</td>          
                                                <td>
                                                    <form action="#">
                                                        <input type="hidden" id="_token" name="_token" value="<?= csrf_token(); ?>">
                                                        <p>
                                                          <input type="checkbox" class="status" id="status_{{$row->id}}" value="{{$row->status}}" onclick="changeStatus({{$row->id}},this.value)" @if($row->status == 1){{'checked="checked"'}}@endif   />
                                                          <label id="label_{{$row->id}}" for="status_{{$row->id}}">@if($row->status == 1){!!'<span class="text-success">Resolved</span>' !!}@else{!!'<span class="text-danger">Un-Resolved</span>'!!}@endif</label>
                                                        </p>
                                                    </form>
                                                </td>          
                                            </tr>
                                             @endforeach    
                                            @else
                                            <tr>
                                                <td colspan="4">
                                                    No Data Found
                                                </td>
                                            </tr>
                                            @endif                               
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $(document).ready(function()
            {
                var url = window.location.href;
                $('#limit').on('change', function () {
                    var limit = $(this).val();
                    var str = window.location.search;
                    str = replaceQueryParam('limit', limit, str);
                    str = replaceQueryParam('page', 1, str);
                    window.location = window.location.pathname + str;
                });
                $('#limit').val(getParameterByName('limit'));
                function replaceQueryParam(param, newval, search) {
                    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
                    var query = search.replace(regex, "$1").replace(/&$/, '');

                    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
                }

                function getParameterByName(name) {
                    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
                    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
                }
                //console.log({{$limit}});
                $('#limit').val('{{$limit}}');

                $('#select_status').on('change',function(){
                    var status_id = $(this).val();
                    var str = window.location.search;
                    str = replaceQueryParam('status_value', status_id, str);
                    window.location = window.location.pathname + str;
                });
                $('#select_status').val(getParameterByName('status_value'));
            });
        function changeStatus(id,status)
        {
            //alert(status);
            var statusData = {
                'id'     : id,
                'status' : status,
                '_token' : $('#_token').val()
            };
            // process the form
            $.ajax({
                type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url     : '{{url("contact_us/status")}}', // the url where we want to POST
                data    : statusData, // our data object
                dataType: 'text'  // what type of data do we expect back from the server
                //encode  : true
            })
                // using the done promise callback
                .done(function(data) {
                    //console.log(data);
                    if(data){
                        if(status == 1){ 
                            status = 0;
                            $('#label_'+id+' span').text('Un-Resolved');
                            $('#label_'+id+' span').attr('class','text-danger');
                        }else{
                            status = 1;
                            $('#label_'+id+' span').text('Resolved');
                             $('#label_'+id+' span').attr('class','text-success');
                        }
                        $('#status_'+id).attr('checked','checked');
                        $('#status_'+id).attr('value',status);
                    }else{
                        alert("Something went wrong! Please try again later")
                    }
                });

            // stop the form from submitting the normal way and refreshing the page
            //event.preventDefault();
            //});
        }
    </script>
@endsection

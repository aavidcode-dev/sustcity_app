@extends('layouts.app')

@section('content')
<style>
    .pagination{
        margin:0px !important;
    }
    .txt-algn{
        text-align: -webkit-center;
    padding-top: 6% !important;
    }
</style>
<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Utility</h2>
                         <div class="box-header with-border">
                            <div class="row">
                               <div class="col-lg-10">                               
                               </div>
                               <div class="col-lg-2 text-right">
                                   <a class="btn btn-primary pull-right" href="<?php echo 'utility/create'; ?>">Add Utility</a>
                               </div> 
                            </div>
                            <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                            <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                         </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <div class="col-lg-12 pad-search">
                                <form id="create_form" class="form-horizontal" action="{{ url('/utility') }}" method="get">
                                    <div class="form-group"> 
                                            <div class="col-lg-3">
                                            <div class="form-group">
                                                        <label>Utility</label>
                                                        <select class="form-control" name="utility_name"> 
                                                            <option></option>                                                
                                                              @foreach ($data2 as $row) 
                                                            <option value="{{$row->utility_name}}">{{ $row->utility_name }}</option>
                                                            @endforeach   
                                                        </select>
                                                    <p style="color:red; margin: 0 0 10px 18%;">{{ $errors->first('utility_year') }}</p>                                      
                                                </div>
                                            </div>  
                                            <div class="col-lg-3 col-sm-3 col-md-3" style="margin-left: 5%;">
                                            <div class="form-group">
                                                        <label>Utility Type</label>
                                                        <select class="form-control" name="name"> 
                                                            <option></option>                                                
                                                              @foreach ($data1 as $row) 
                                                            <option value="{{$row->name}}">{{ $row->name }}</option>
                                                            @endforeach   
                                                        </select>
                                                    <p style="color:red; margin: 0 0 10px 18%;">{{ $errors->first('name') }}</p>                                      
                                                </div>
                                            </div>
                                                                               
                                            <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                            <div class="col-lg-1"><input type="submit" class="btn btn-warning" value="search"></div>
                                            <div class="col-lg-1"><a href="{{ url('/utility') }}" class="btn btn-danger">Reset</a></div>                                           
                                    </div>
                                    <hr>
                                </form>
                            </div>
                                <div class="row">
                                    <div class="col-lg-6" style="margin-top: 0px; margin-bottom: 0px;">
                                        <div class="pull-right">{!!$data->appends($post)->render()!!}</div>
                                    </div>
                                    <div class="col-lg-2 pull-right" style="margin-top: 0px; padding-left: 0">
                                        <select name="limit" class="form-control" id="limit">
                                            <option value="">View All</option>
                                            <option value="50">50</option> 
                                            <option value="100">100</option>   
                                            <option value="150">150</option> 
                                            <option value="200">200</option>
                                        </select>
                                    </div>
                                </div>  
                                <div class="box-body">                            
                                <div class="pull-right"></div>
                                <div class="col-lg-2 pull-right" style="margin-top: 20px; padding-left: 0">                               
                                </div> 
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table no-margin table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No.</th>
                                                    <th>Utility Id</th> 
                                                    <th>Utility Logo</th>  
                                                    <th>Utility Name</th>     
                                                    <th>Utility Type</th> 
                                                    <th>Identifier</th> 
                                                    <th>Example</th> 
                                                    <th>Status</th> 
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>                                         
                                                <?php $count =0; ?>
                                                @foreach ($data as $row) 
                                                <tr>
                                                    <td class="txt-algn">{{++$count}}</td>
                                                    <td class="txt-algn">{{$row->id}}</td>
                                                    <td style="width: 142px;"><img src="{{URL('/').'/'.$row->image_url}}" style="width: 100%;"/></td> 
                                                    <td class="txt-algn">{{$row->utility_name}}</td> 
                                                    <td class="txt-algn">{{$row->utility_type->name}}</td>
                                                    <td class="txt-algn">{{$row->identifier}}</td>
                                                    <td class="txt-algn">{{$row->example}}</td>
                                                     <td>
                                                    <?php if($row->status == 1){  ?>
                                                        <span class="label label-success">Active</span>
                                                    <?php } else{ ?>
                                                        <span class="label label-danger">De-Active</span>
                                                    <?php } ?>
                                                </td>
                                                <td> 
                                                    <a class="btn btn-warning" href="<?php echo 'utility/' . $row->id ?>/edit">Edit</a>
                                                    <?php if($row->status == 1){  ?>   
                                                    <a href="#my_modal" data-toggle="modal" data-book-id="{{$row->id}}"><i class="material-icons">clear</i></a>                                                    
                                                    <?php } else{ ?>
                                                    <a href="#my_modal1" data-toggle="modal" data-book-id="{{$row->id}}"><i class="material-icons">done</i></a>                                                     
                                                    <?php } ?> 
                                                </td>
                                                </tr>
                                                @endforeach                                     
                                            </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div>
                                </div><!-- /.box-body -->                   
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<div class="modal" id="my_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'utility/disable']) !!}
            <div class="modal-body">
              <p>Are you sure want to delete this utility?</p>        
            </div>
            <input type="hidden" name="u_id" value="" />
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Submit">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
      {!! Form::close() !!}          
    </div>
  </div>
</div>
<div class="modal" id="my_modal1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'utility/enable']) !!}
            <div class="modal-body">
              <p>Are you sure want to enable this utility?</p>        
            </div>
            <input type="hidden" name="u_id" value="" />
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Submit">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
      {!! Form::close() !!}          
    </div>
  </div>
</div>
<script>
$(document).ready(function() 
{                 
    $('#my_modal').on('show.bs.modal', function(e) { 
        var bookId = $(e.relatedTarget).data('book-id');
        $(e.currentTarget).find('input[name="u_id"]').val(bookId);
    });
});
</script> 
<script>
$(document).ready(function() 
{                 
    $('#my_modal1').on('show.bs.modal', function(e) { 
        var bookId = $(e.relatedTarget).data('book-id');
        $(e.currentTarget).find('input[name="u_id"]').val(bookId);
    });
});
</script> 
<script type="text/javascript">
$(document).ready(function()
{    
    $('[name="utility_name"]').val('<?php echo (isset($post["utility_name"]) ? $post["utility_name"] : "") ?>'); 
    $('[name="name"]').val('<?php echo (isset($post["name"]) ? $post["name"] : "") ?>');   
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=upload-excel]");
    form.validate();   
    
    
      var url = window.location.href;
        $('#limit').on('change', function () {
            var limit = $(this).val();
            var str = window.location.search;
            str = replaceQueryParam('limit', limit, str);
            str = replaceQueryParam('page', 1, str);
            window.location = window.location.pathname + str;
        });

        $('#limit').val(getParameterByName('limit'));

        function replaceQueryParam(param, newval, search) {
            var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
            var query = search.replace(regex, "$1").replace(/&$/, '');

            return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
        }

        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
        $('#limit').val('{{$limit}}');
});
</script>  
@endsection

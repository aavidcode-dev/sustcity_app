@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Utility</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            {!! Form::model($data1, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'utility/' . $data1->id , 'enctype' => 'multipart/form-data']) !!}
                        <div class="box-body">                                                                                                                                                          
                                <div class="body">                            
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="utility_name" value="<?php echo $data1->utility_name; ?>" id="utility_name" placeholder="Utility Name" />                                                    
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('utility_name') }}</p>
                                            </div>
                                             <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="identifier" value="<?php echo $data1->identifier; ?>" id="identifier" placeholder="Identifier" />                                                    
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('identifier') }}</p>
                                            </div>
                                             <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="example" value="<?php echo $data1->example; ?>" id="example" placeholder="Example" />                                                    
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('example') }}</p>
                                            </div>
                                            <div class="form-group">
                                                 <label>Utility Type</label>                                                
                                               <select class="form-control required" name="utility_type_id" id="name">
                                                    <option value=""></option>
                                                    @foreach($data2 as $type)
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                    @endforeach
                                                </select>
                                                <p style="color:red;">{{ $errors->first('utility_type_id') }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Premium</label>
                                                <select class="form-control required" name="premium" id="premium">
                                                    <option value=""></option>                                                    
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                                <p style="color:red;">{{ $errors->first('premium') }}</p>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-line">
                                                <label>Logo</label>
                                                <input type="file" class="form-control" name="image_url" id="image_url" />
                                                <img src="{{URL::to('/').'/'.$data1->image_url}}" />
                                                </div> 
                                            </div> 
                                            
                                            <div class="form-group">
                                            
                                            <div class="demo-checkbox"> 
                                            
                                                 <input type="hidden" name="c_id" value="<?php echo $data1->id; ?>">
                                                
                                            </div>
                                             <p style="color:red;">{{ $errors->first('cat_id') }}</p>  
                                            </div>
                                        </div>
                                    </div>                                   
                                </div>                                                   
                            <br/><br/>
                        
                       
                            <div class="col-md-11 col-sm-11">
                                <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Update">
                            </div>
                            <div class="col-md-1 col-sm-1">
                                <a href="{{URL('/utility')}}" class="btn btn-warning">Cancel</a> 
                             </div>
                        </div><!-- /.box-body -->
                       {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

<script type="text/javascript">
$(document).ready(function()
{
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();    
    $('[name="utility_type_id"]').val('{{$data1->utility_type_id}}');
    $('[name="premium"]').val('{{$data1->premium}}');        
});
</script>

@endsection
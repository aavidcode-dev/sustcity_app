@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Utility Year
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <p style="color:#32CD32;"><?php echo Session::get('message'); ?></p><br/>
                        <h3 class="box-title">Add</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                       {!! Form::open(['name'=>'create', 'id'=>'create', 'files'=>true, 'method'=>'POST', 'url'=>'utility', 'class'=>'form-horizontal']) !!}
                        <div class="box-body">
                            <div class="col-md-2">Utility Year:</div>
                            <div class="col-md-10"> 
                                <input type="text" class="form-control required" name="utility_name" id="utility_name"><br/> 
                            </div>                            
                            <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                            <p style="color:red;margin: 0 0 10px 18%;">{{ $errors->first('utility_name') }}</p>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Add Utility">
                            </div>
                        </div><!-- /.box-body -->
                       {!! Form::close() !!}
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div>

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@endsection




@section('pageFooterSpecificPlugin')
@endsection


@section('pageFooterSpecificJS')

<script type="text/javascript" src="{{ asset('public/js/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function()
{
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=create]");
    form.validate();
});
</script>
@endsection


@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Utility</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                        </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                {!! Form::open(['name'=>'create', 'id'=>'create', 'files'=>true, 'method'=>'POST', 'url'=>'utility','enctype' => 'multipart/form-data']) !!}
                                <div class="box-body">                                                                                                                                                          
                                <div class="body">                            
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="utility_name" id="utility_name" placeholder="Utility Name" value="{{old('utility_name')}}" />
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('utility_name') }}</p>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="identifier" id="utility_name" placeholder="Identifier" value="{{old('identifier')}}" />
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('identifier') }}</p>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="example" id="utility_name" placeholder="Example" value="{{old('example')}}" />
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('example') }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Utility Type</label>
                                                <select class="form-control required" name="utility_type_id" id="name">
                                                    <option value=""></option>
                                                    @foreach($data1 as $type)
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                    @endforeach
                                                </select>
                                                <p style="color:red;">{{ $errors->first('utility_type_id') }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Premium</label>
                                                <select class="form-control required" name="premium" id="premium">
                                                    <option value=""></option>                                                    
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                                   <p style="color:red;">{{ $errors->first('premium') }}</p>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-line">
                                                <label>Logo</label>
                                                <input type="file" class="form-control" name="image_url" id="image_url" />
                                                <p style="color:red;">{{ $errors->first('image_url') }}</p> 
                                                </div> 
                                            </div>                                          
                                        </div>
                                    </div>                                   
                                </div>                                                   
                            <br/><br/>                                                      
                            <div class="col-md-11 col-sm-11">
                                <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Add Utility">
                            </div>
                             <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/utility')}}" class="btn btn-warning">Cancel</a> 
                             </div>
                        </div><!-- /.box-body -->
                       {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();   
    
   $('[name="utility_type_id"]').val('{{old("utility_type_id")}}');
   $('[name="premium"]').val('{{old("premium")}}');
   $('[name="cat_id"]').val('{{old("cat_id")}}');
});
</script>  
@endsection





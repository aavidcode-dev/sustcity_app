@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Edit Top Credits Details</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                 {!! Form::model($row, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'top-credits/' . $row->id, 'class'=>'form-horizontal']) !!}
                            <div class="box-body">                                                                                                                                                          
                                    <div class="body">                            
                                        <div class="row clearfix">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="col-md-2 col-sm-2"><label>House Code:</label></div>
                                                    <div class="col-md-10 col-sm-10">                                                                                                     
                                                        <input type="text" class="form-control required" name="h_code" value="<?= $row->h_code; ?>"> 
                                                      <p style="color:red;">{{ $errors->first('h_code') }}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                     <div class="col-md-2 col-sm-2"><label>Credits:</label></div>
                                                    <div class="col-md-10 col-sm-10">
                                                    <div class="form-line">
                                                         <input type="text" class="form-control required" name="credit" value="<?= $row->credit; ?>"> 
                                                    </div>
                                                        <p style="color:red;">{{ $errors->first('credit') }}</p>
                                                    </div>
                                                </div>
                                                 <input type="hidden" name="c_id" value="<?php echo $row->id; ?>">                                                
                                                <div class="col-md-11">
                                                     <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Update">
                                                  </div>
                                                <div class="col-md-1 col-sm-1">
                                                     <a href="{{URL('/top-credits')}}" class="btn btn-warning">Cancel</a> 
                                                </div> 
                                                
                                            </div>
                                        </div>                                   
                                    </div>                                                   
                                <br/><br/>                                                      
                                <p style="color:red;margin: 0 0 10px 18%;">{{ $errors->first('h_code') }}</p>
                            </div><!-- /.box-body -->
                       {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();     
});
</script>
@endsection


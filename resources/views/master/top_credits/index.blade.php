@extends('layouts.app')

@section('content')
<link href="{!! asset('public/css/bootstrap-datepicker3.min.css')!!}" rel="stylesheet">
<style>
     hr{
        margin-top: 40px !important; 
    margin-bottom: 0px !important; 
    }
    .pad-search{
            padding: 32px 0px 10px 5px !important;
    }
</style>
<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Top Credits Details</h2> 
                         <div class="box-header with-border">
                            <div class="row">
                                
                                <div class="col-lg-8">
                                </div>      
                                 <div class="col-lg-2 text-right">                                    
                                     <a class="btn btn-warning pull-right" href="<?php echo 'top-credits/excel_report'; ?>">Download Data</a>
                                </div>
                                 <div class="col-lg-2 text-right">   
                                    <a class="btn btn-primary pull-right" href="<?php echo 'top-credits/upload'; ?>">Upload Sheet</a>
                                </div>
                            </div>
                                                  
                             
                             
                              <?php 
                            $report = Session::get('unsuccess_message');
                           
                            ?>
                              <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                               <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message_notification'); ?></p>
                              
                            @if (isset($report))
                            <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                Success: {{isset($report['success']) ? count($report['success']) : 0}}<br />
                                Fail: {{isset($report['fail']) ? count($report['fail']) : 0}}<br />
                                @if (isset($report['fail']) && count($report['fail'] > 0))
                                <ul>
                                    @foreach($report['fail'] as $ho_code => $ho_data) 
                                    <li>
                                        {{$ho_code}}
                                        <ul>
                                            @foreach($ho_data as $key => $message)
                                            <li>{{$key}} : {{$message[0]}}</li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </div>
                            @endif
                        </div><!-- /.box-header --> 
                    
                    
                        </div>
                    
                        



                        <script src="{!! asset('public/js/bootstrap-datepicker.min.js') !!}"></script>
                        <script type="text/javascript">
                           /* $('#sandbox-container input').datepicker({
                                todayBtn: "linked",
                                autoclose: true,
                                todayHighlight: true
                            });*/
                        $(document).ready(function()
                            {   
                                $('#sandbox-container input').datepicker({
                                todayBtn: "linked",
                                format: "dd-mm-yyyy",
                                endDate: "Start date",
                                clearBtn: true,
                                autoclose: true,
                                todayHighlight: true,
                                orientation: "bottom right"
                            });
                            });
                        </script>
                        
                       <div class="col-lg-12 pad-search">
                                <form id="create_form" class="form-horizontal" action="{{ url('/top-credits') }}" method="get">
                                    <div class="form-group">                                         
                                        <div class="col-lg-3 col-sm-3 col-md-3" style="margin-left: 5%;">
                                        <div class="form-line">
                                              <label>House Code</label>
                                            <input type="text" name="h_code" class="form-control" placeholder="House Code" value="{{ Request::get('h_code')}}"/>
                                        </div>
                                        </div>   

                                        <div class="col-lg-2 col-sm-2 col-md-2" style="margin-left: 5%;">
                                        <div class="form-line" id="sandbox-container">
                                              <label>From</label>
                                            <input type="text" name="f_date" class="form-control" placeholder="Date" value="{{ Request::get('f_date')}}"/>
                                        </div>
                                        </div> 

                                        <div class="col-lg-2 col-sm-2 col-md-2" style="margin-left: 5%;">
                                        <div class="form-line" id="sandbox-container">
                                              <label>Till</label>
                                            <input type="text" name="t_date" class="form-control" placeholder="Date" value="{{ Request::get('t_date')}}"/>
                                        </div>
                                        </div> 

                                        <input type="hidden" name="_token" value="<?= csrf_token(); ?>"> 
                                        <div class="col-lg-1"><input type="submit" class="btn btn-warning" value="search"></div>
                                        <div class="col-lg-1"><a href="{{ url('/top-credits') }}" class="btn btn-danger">Reset</a></div>                                           
                                    </div> 
                                    <hr>
                                </form>
                            </div>
                            <table class="table no-margin table-bordered table-hover">
                                 <div class="col-lg-12 col-sm-12 col-md-12"> 
                                     <?php  
                                    
                                            if(count($top_count) < count($topcredits)){
                                                
                                         ?>
                                        <a href="{{URL('/top-credits/update_notification')}}" class="btn btn-warning pull-right">Push Notifications</a>
                                            <?php //Changed to Push Notifications instead of submit
                                            } ?>
                                 </div>
                            </table> 
                     
                        <div class="body"> 
                             
                            <div class="table-responsive">                                
                        <div class="box-body">
                           
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table no-margin table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>    
                                                <th>House Code</th>
                                                <th>Credit</th>                                         
                                                <th>Is Claimed</th>  
                                                <?php if(@$_GET['h_code'] || @$_GET['f_date']){ ?>
                                                    <th>Top Credit Send Date</th>     
                                                <?php } ?>
                                                <?php if(@$_GET['h_code'] || @$_GET['f_date']){ ?>
                                                    <th>Claim Date</th>     
                                                <?php } ?>
                                                    <th>Created at</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($data[0]))                                         
                                            <?php $count =0; ?>
                                            @foreach ($data as $row) 
                                            <tr>
                                                <td>{{++$count}}</td>
                                                <td>{{$row->h_code}}</td>    
                                                <td>{{$row->credit}}</td>  
                                                <td>{{$row->is_claimed > 0 ? 'No' : 'Yes'}}</td>   
                                                <?php if(@$_GET['h_code'] || @$_GET['f_date']){ ?>
                                                <td>                                                    
                                                    <?php if($row->sent_date == NULL){?>
                                                    <label style="font-weight: 100;">Not Yet sent</label>
                                                    <?php }else{ ?>
                                                        {{$row->sent_date}}
                                                    <?php }?>
                                                </td>
                                                <?php } ?>
                                                                                                
                                                <?php if(@$_GET['h_code'] || @$_GET['f_date']){ ?>
                                                <td>                                                   
                                                    <?php if($row->claim_date == NULL){?>
                                                        <label style="font-weight: 100;">Not Yet claim</label>
                                                    <?php }else{ ?>
                                                        {{$row->claim_date}}
                                                    <?php }?>
                                                </td>
                                                <?php } ?>
                                                <td>{{$row->created_at}}</td> 
                                            </tr>
                                            @endforeach 
                                            @else
                                            <tr>
                                                <td colspan="5">
                                                    No Data Found
                                                </td>
                                            </tr>
                                            @endif                                    
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div>
                        </div><!-- /.box-body -->
                 
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
   
   
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=upload-excel]");
    form.validate();   
    
    
      var url = window.location.href;
        $('#limit').on('change', function () {
            var limit = $(this).val();
            var str = window.location.search;
            str = replaceQueryParam('limit', limit, str);
            str = replaceQueryParam('page', 1, str);
            window.location = window.location.pathname + str;
        });

        $('#limit').val(getParameterByName('limit'));

        function replaceQueryParam(param, newval, search) {
            var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
            var query = search.replace(regex, "$1").replace(/&$/, '');

            return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
        }

        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
       
});
</script>  
@endsection

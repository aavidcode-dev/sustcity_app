@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            
                            <div class="col-lg-12 text-right" style="padding-bottom: 5px;">
                               <h2 class="pull-left">Top Credits</h2>     <a class="btn btn-success pull-right" href="{{ url('/top-credits/downloadXls') }}">Download Sheet</a>
                                </div>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <div class="box-body">                                                                                                                                                          
                                <div class="body">
                                    {!! Form::open(['name'=>'upload-excel', 'id'=>'upload-excel','files'=>true, 'method'=>'POST', 'url'=>'top-credits/uploadexcelSave', 'class'=>'form-horizontal']) !!}
                                    <div class="box-body">                                                                                                                                                          
                                        <div class="body">                            
                                            <div class="row clearfix">
                                                <div class="col-sm-9" style=" margin-left: 5%;">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                          <input type="file" class="form-control required" name="uploadexcel" id="uploadexcel"><br/> 
                                                        </div>                                              
                                                    </div>                                           
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="submit" class="btn btn-sm btn-warning pull-right" id="create-btn" value="Upload">
                                                    <a href="{{ url('/top-credits') }}" class="btn btn-sm btn-primary pull-right" style="margin-right: 5px;">Back</a>
                                                </div>
                                            </div>                                   
                                        </div>                                                   
                                        <br/><br/>
                                        <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                        <p style="color:red;margin: 0 0 10px 18%;">{{ $errors->first('uploadexcel') }}</p>                            
                                    </div><!-- /.box-body -->
                                   {!! Form::close() !!}
                                </div>                                                   
                                <br/><br/>                                                                                
                                </div><!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=create]");
    form.validate();    
     $('[name="utility_year"]').val('{{old("utility_year")}}');
      $('[name="utility_id"]').val('{{old("utility_id")}}');
       $('[name="cat_id"]').val('{{old("cat_id")}}');
});
</script>  
@endsection




@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Top Credits</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="    background: #FFF; padding-bottom: 29px; padding-top: 29px;">
                                {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'top-credits']) !!}
                                <div class="row clearfix">
                                    <div class="col-sm-12">

                                            <div class="col-md-2 col-sm-2"><label>House Code:</label></div>
                                            <div class="col-md-10 col-sm-10"> 
                                                 <div class="form-group">
                                              <div class="form-line">
                                                 <input type="text" class="form-control required" name="h_code" value="{{old('h_code')}}"> 
                                            </div> 
                                                 <p style="color:red;">{{ $errors->first('h_code') }}</p>
                                            </div>
                                        </div>

                                             <div class="col-md-2 col-sm-2"><label>Name:</label></div>
                                            <div class="col-md-10 col-sm-10">
                                                <div class="form-group">
                                            <div class="form-line">
                                                 <input type="text" class="form-control required" name="name" value="{{old('name')}}"> 
                                            </div>
                                                 <p style="color:red;">{{ $errors->first('name') }}</p>
                                            </div>
                                        </div>

                                             <div class="col-md-2 col-sm-2"><label>Partener:</label></div>
                                            <div class="col-md-10 col-sm-10">
                                                 <div class="form-group">
                                            <div class="form-line">
                                                 <input type="text" class="form-control required" name="partner" value="{{old('partner')}}"> 
                                            </div>
                                                  <p style="color:red;">{{ $errors->first('partner') }}</p>
                                            </div>
                                        </div>

                                            <div class="col-md-2 col-sm-2"><label>Address:</label></div>
                                            <div class="col-md-10 col-sm-10">
                                                 <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control required" name="address" value="{{old('address')}}">                                                       
                                                <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                            </div>
                                                <p style="color:red;">{{ $errors->first('address') }}</p>
                                            </div>
                                        </div>

                                              <div class="col-md-2 col-sm-2"><label>Utility:</label></div>
                                               <div class="col-md-10 col-sm-10">
                                                    <div class="form-group">
                                            <select class="form-control required" name="utility_id">
                                                <option value=""></option>
                                                @foreach($data1 as $type) 
                                                    <option value="{{$type->id}}">{{$type->utility_name}}</option>
                                                @endforeach
                                            </select>
                                              <p style="color:red;">{{ $errors->first('utility_name') }}</p>
                                               </div> 
                                        </div>

                                       <div class="col-md-2 col-sm-2"><label>Category:</label></div>
                                               <div class="col-md-10 col-sm-10">
                                                    <div class="form-group">
                                            <select class="form-control required" name="cat_id">
                                                <option value=""></option>
                                                @foreach($data2 as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                              <p style="color:red;">{{ $errors->first('name') }}</p>
                                               </div>                                                                                           
                                        </div>
                                       
                                          <div class="col-md-11 col-sm-11">
                                            <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Add House Details">
                                        </div>
                                         <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/housedetails')}}" class="btn btn-warning">Cancel</a> 
                                        </div>
                                    </div>
                                </div>   
                                   {!! Form::close() !!}                                                                                
                            <br/><br/>                                                      
                          
                        
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=create]");
    form.validate();   
     $('[name="utility_id"]').val('{{old("utility_id")}}');
   $('[name="cat_id"]').val('{{old("cat_id")}}');
   
   
});
</script>  
@endsection




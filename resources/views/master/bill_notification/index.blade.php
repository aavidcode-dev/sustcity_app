@extends('layouts.app')

@section('content')
<style>
     hr{
        margin-top: 40px !important; 
    margin-bottom: 0px !important; 
    }
    .pad-search{
            padding: 32px 0px 10px 5px !important;
    }
    [type="checkbox"]:not(:checked){
        left: 0 !important;
            opacity: .6 !important;
    }
    
    [type="checkbox"]:checked
    {
        left: 0 !important;
            opacity: 1 !important; 
    }
    input[type="checkbox"]{
        margin-left: 0px !important;
    }  
    .error{        
      color: red;
    }
    [type="checkbox"] + label{
        margin-left: 24px;
    }
    [type="checkbox"] + label:before, [type="checkbox"]:not(.filled-in) + label:after{
        border: 0px solid gray;
    }
</style>
<link href="{!! asset('public/admin-material/mac.css')!!}" rel="stylesheet" />
<script type="text/javascript" src="{!! asset('public/admin-material/mac.js')!!}"></script>

<script> 
$(document).ready(function()
{
    $('#the_table').dataTable( {
        "dom": 'iftrp<"clear">'
	//"dom": '<"top"i>rt<"bottom"flp><"clear">'
    });
    $('#the_table tbody').on( 'click', 'tr', function ()
    {
        $(this).toggleClass('selected');
    });
 
    $('#button').click( function ()
    {
        alert( table.rows('.selected').data().length +' row(s) selected' );
    });    
}); 
</script>

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>New Bill Notification Details</h2> 
                         <div class="box-header with-border">
                            <div class="row">                                
                                <div class="col-lg-10">
                                </div>                              
                                <div class="col-lg-2 text-right">
                                   
                                </div>                                
                            </div>                             
                            <?php 
                            $report = Session::get('unsuccess_message');                           
                            ?>
                              <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                               <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('message1'); ?></p>
                            @if (isset($report))
                            <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                Success: {{isset($report['success']) ? count($report['success']) : 0}}<br />
                                Fail: {{isset($report['fail']) ? count($report['fail']) : 0}}<br />
                                @if (isset($report['fail']) && count($report['fail'] > 0))
                                <ul>
                                    @foreach($report['fail'] as $ho_code => $ho_data) 
                                    <li>
                                        {{$ho_code}}
                                        <ul>
                                            @foreach($ho_data as $key => $message)
                                            <li>{{$key}} : {{$message[0]}}</li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </div>
                            @endif
                        </div><!-- /.box-header -->                                         
                        </div>                        
                       <div class="col-lg-12 pad-search">
                                <form id="create_form" class="form-horizontal" method="get">
                                    <div class="form-group">                                         
                                        <div class="col-lg-2 col-sm-2 col-md-2" style="margin-left: 3%;">
                                        <div class="form-group">
                                            <label>Utility</label>
                                            <select class="form-control" name="utility_name"> 
                                                <option></option>                                                
                                                @foreach ($data1 as $row) 
                                                    <option value="{{$row->id}}">{{ $row->utility_name }}</option>
                                                @endforeach   
                                            </select>
                                            <p style="color:red; margin: 0 0 10px 18%;">{{ $errors->first('utility_name') }}</p>                                      
                                         </div>
                                         </div>
                                         <div class="col-lg-2 col-sm-2 col-md-2" >
                                            <div class="form-line">
                                                  <label>House Code</label>
                                                <input type="text" name="h_code" class="form-control" placeholder="House Code" value="{{ Request::get('h_code')}}"/>
                                            </div>
                                        </div>
                                         <div class="col-lg-2">
                                             <div class="form-group">
                                                 <label>Categories</label>
                                                 <select class="form-control" name="cat_id"> 
                                                     <option></option>                                                
                                                     @foreach ($data2 as $row) 
                                                         <option value="{{$row->id}}">{{ $row->name }}</option>
                                                     @endforeach   
                                                 </select>
                                                 <p style="color:red; margin: 0 0 10px 18%;">{{ $errors->first('name') }}</p>                                      
                                             </div>
                                         </div>
                                        <div class="col-lg-2">
                                             <div class="form-group">
                                                 <label>Year</label>
                                                 <select class="form-control" name="year"> 
                                                     <option></option>                                                
                                                     @foreach ($data3 as $row) 
                                                         <option value="{{$row['year']}}">{{ $row['year'] }}</option>
                                                     @endforeach   
                                                 </select>
                                                 <p style="color:red; margin: 0 0 10px 18%;">{{ $errors->first('year') }}</p>                                      
                                             </div>
                                         </div>   
                                        <div class="col-md-1 col-lg-1 col-sm-1" style="width: 130px";>
                                             <div class="form-group">
                                                 <label>Month</label>
                                                 <select class="form-control" name="month"> 
                                                     <option></option>                                                
                                                     @foreach ($data4 as $row) 
                                                         <option value="{{$row['month']}}">{{$row['month']}}</option>
                                                     @endforeach   
                                                 </select>
                                                 <p style="color:red; margin: 0 0 10px 18%;">{{ $errors->first('month') }}</p>                                      
                                             </div>
                                         </div>  
                                         
                                         <div class="col-lg-1"><input type="submit" class="btn btn-warning" value="search"></div>
                                         <div class="col-lg-1"><a href="{{ url('/bill_notification') }}" class="btn btn-danger">Reset</a></div>                                           
                                    </div>
                                    <hr>
                                </form>
                            </div>
                            <div class="row">
                                <div class="col-lg-6" style="margin-top: 0px; margin-bottom: 0px;"></div>
                                <div class="col-lg-2 pull-right" style="margin-top: 0px; padding-left: 0">

                                </div>
                            </div>  
                       <?php if(isset($post["utility_name"]) || isset($post["cat_id"]) || isset($post["year"]) ||isset($post["month"]) ){ ?>                        
                    
                        <div class="body">                              
                            <div class="table-responsive">                                
                        <div class="box-body">
                            <form id="create_form" name="create_form" class="form-horizontal" action="{{ url('/bill_notification/send_notification') }}" method="post">
                            <div class="pull-right"></div>
                            <div class="col-lg-2 pull-right" style="margin-top: 20px; padding-left: 0">
                                <input type="submit" class="btn btn-success" id="noti_btn" value="Push Notifications"/> {{--submit--}}
                            </div>
                            <div class="col-lg-12">
                                <label>Notification Text</label>
                                <textarea name="notificationtext" class="form-control required" placeholder="Enter Notification Text"></textarea>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table no-margin table-bordered table-hover" id="the_table">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>    
                                                <th>House Code</th>
                                                <th>Mobile</th> 
                                                <th>Year</th>
                                                <th>Month</th>
                                                <th>Unit</th>                                              
                                                <th>
                                                    <div class="checkbox">
                                                         <input type="checkbox" id="ckbCheckAll">
                                                    </div>
                                                </th>                                                                                        
                                            </tr>
                                        </thead>
                                        <tbody>                                         
                                            <?php $count =0; ?>
                                            @foreach ($data as $row) 
                                            <?php if(@$row->House_details->mobile) { ?>
                                            <tr id="test">
                                                <td>{{++$count}}</td>
                                                <td>{{$row->h_code}}</td>    
                                                <td>{{@$row->House_details->mobile}}</td>  
                                                <td>{{$row->year}}</td>  
                                                <td>{{$row->month}}</td>    
                                                <td>{{$row->unit}}</td>                                                  
                                                <td> 
                                                    <div class="checkbox"> 
                                                        <input type="checkbox" class="checkbox checkBoxClass required" name="h_code[]" value="{{$row->h_code}}">
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            @endforeach                                              
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div>
                             </form>
                           </div><!-- /.box-body -->                 
                            </div>
                        </div>
                       <?php } ?>                        
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

<script type="text/javascript">
$(document).ready(function()
{
    $("#noti_btn").removeAttr('disabled');
    var form = $("[name=create_form]");
    form.validate(
    {
        ignore: "",
        rules:
        {
            notificationtext:
            {
                required: true
            }
        },
        messages:
        {
            notificationtext:
            {
                required:"Required"
            }
        }
    });
});
</script>    

<script type="text/javascript">
$(document).ready(function()
{    
     $("#ckbCheckAll").click(function () {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
    });
   
    
    $('[name="utility_name"]').val('<?php echo (isset($post["utility_name"]) ? $post["utility_name"] : "") ?>'); 
    $('[name="cat_id"]').val('<?php echo (isset($post["cat_id"]) ? $post["cat_id"] : "") ?>');   
    $('[name="year"]').val('<?php echo (isset($post["year"]) ? $post["year"] : "") ?>'); 
    $('[name="month"]').val('<?php echo (isset($post["month"]) ? $post["month"] : "") ?>'); 
    
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=upload-excel]");
    form.validate();   
    
    
      var url = window.location.href;
        $('#limit').on('change', function () {
            var limit = $(this).val();
            var str = window.location.search;
            str = replaceQueryParam('limit', limit, str);
            str = replaceQueryParam('page', 1, str);
            window.location = window.location.pathname + str;
        });

        $('#limit').val(getParameterByName('limit'));

        function replaceQueryParam(param, newval, search) {
            var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
            var query = search.replace(regex, "$1").replace(/&$/, '');

            return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
        }

        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
       
});
</script>  
@endsection

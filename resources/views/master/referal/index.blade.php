@extends('layouts.app')

@section('content')
<style>
    .pagination{
        margin:0px !important;
    }
        .map {
        height: 200px;
        width: 100%;
       }
</style>
<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">                           
                              <div class="col-lg-6">   
                                <h2>Referals Friends Report</h2>
                            </div>
                            <div class="col-lg-4 text-right">    
                            </div>
                             <div class="col-lg-2 text-right">                                    
                                     <a class="btn btn-warning pull-right" href="<?php echo 'referal/excel_report'; ?>">Download Data</a>
                                </div>    
                         <div class="box-header with-border">
                        <div class="row">
                            <div class="col-lg-10">
                               
                            </div>                            
                        </div>
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">                                                               
                                <div class="box-body">                            
                                <div class="pull-right"></div>
                                <div class="col-lg-2 pull-right" style="margin-top: 20px; padding-left: 0">                               
                                </div>
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table no-margin table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No.</th>
                                                    <th>Referee Mobile</th>
                                                    <th>Referred Mobile</th>
                                                    <th>Ref Code</th>
                                                </tr>
                                            </thead>
                                            <tbody>                                         
                                                <?php $count =0;  ?>                                                
                                                @foreach ($data as $row) 
                                                <tr>
                                                    <td>{{++$count}}</td>
                                                    <td>{{$row->referee_mobile}}</td> 
                                                    <td>{{$row->referred_mobile}}</td>
                                                    <td>{{$row->ref_code}}</td>
                                                </tr>
                                                @endforeach                                     
                                            </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div>
                                </div><!-- /.box-body -->                   
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{ 
});
</script>  

@endsection

@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Change Password</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                  {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'updatepassword']) !!}
                        <div class="box-body">                                                                                                                                                            
                                <div class="body">                            
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="password" class="form-control required" name="password" id="password" placeholder="New Password"/>
                                                </div>
                                            </div>                                                                                         
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group"> 
                                                <div class="form-line">  
                                                    <input type="password" class="form-control required" name="password_confirmation" id="c_password" placeholder="Confirm Password"/>
                                                </div>
                                            </div>                                                                                       
                                        </div>
                                    </div>                                   
                                </div>                                                   
                            <br/><br/>
                            <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                            <p style="color:red;">{{ $errors->first('password') }}</p>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Update">
                            </div>
                        </div><!-- /.box-body -->
                       {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=create]");
    form.validate();
});
</script>

@endsection
    
@extends('layouts.app')

@section('content')
   <section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Edit Utility Electricity Consumption</h2>
                         <div class="box-header with-border">                      
                            <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                            <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                        </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                {!! Form::model($row, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'electricconsumption/' . $row->id, 'class'=>'form-horizontal']) !!}
                            <div class="box-body">
                                <div class="col-md-2">House Code:</div> 
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label><?= $row->h_code; ?></label>
                                            <input type="hidden"  name="u_id" value="<?= $row->id; ?>">
                                            <input type="hidden"  name="h_code" value="<?= $row->h_code; ?>">
                                        </div>                                                          
                                </div>
                                </div>
                          
                                <div class="col-md-2">House Rank:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control required" name="rank" value="<?= $row->rank; ?>">                                  
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('rank') }}</p>
                                </div>
                                </div>
                                
                                 <div class="col-md-2">Current Reading:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="curr_reading" value="<?= $row->curr_reading; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('curr_reading') }}</p>
                                </div>
                                </div>
                                 
                                 <div class="col-md-2">Pre Reading:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="pre_reading" value="<?= $row->curr_reading; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('pre_reading') }}</p>
                                </div>
                                </div>
                                 
                                 <div class="col-md-2">Unit:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="unit" value="<?= $row->unit; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('unit') }}</p>
                                </div>
                                </div>
                                 
                                 
                                  <div class="col-md-2">EP Amount:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="ep_amount" value="<?= $row->ep_amount; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('ep_amount') }}</p>
                                </div>
                                </div>
                                
                                <div class="col-md-2">ED Amount:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control required" name="ed_amount" value="<?= $row->ed_amount; ?>">                                    
                                       </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('ed_amount') }}</p>
                                </div>
                                </div>
                                
                                          <div class="col-md-2">MH Amount:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="mh_amount" value="<?= $row->mh_amount; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('mh_amount') }}</p>
                                </div>
                                </div>
                                 
                                          
                                <div class="col-md-2">Fix Amount:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="fix_amount" value="<?= $row->fix_amount; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('fix_amount') }}</p>
                                </div>
                                </div>
                                  
                                 <div class="col-md-2">Other Amount:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="other_amount" value="<?= $row->other_amount; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('other_amount') }}</p>
                                </div>
                                </div>
                                <div class="col-md-2">Total Amount:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="total_amt" value="<?= $row->total_amt; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('total_amt') }}</p>
                                </div>
                                </div>
                                
                                <div class="col-md-2">SP Units:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="sp_units" value="<?= $row->sp_units; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('sp_units') }}</p>
                                </div>
                                </div>
                                
                                <div class="col-md-2">SP Cost:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="sp_cost" value="<?= $row->sp_cost; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('sp_cost') }}</p>
                                </div>
                                </div>
                                
                                
                                
                                 <div class="col-md-2">Average Cost/unit:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="avg_cost_per_unit" value="<?= $row->avg_cost_per_unit; ?>">                                    
                                                </div>
                                          <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('avg_cost_per_unit') }}</p>
                                </div>
                                </div>
                                 
                                 
                                     <div class="col-md-2">Utility Month:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                            <?php  $date = strtotime('-1 years'); 
                                            $d_year =  date('Y', $date);
                                            ?>
                                             <!--<select name="utility_year" class="form-control"><option value="">Year</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option></select>-->                                                                                          
                                            <select class="form-control" name="month"> 
                                                <option><?= $row->month; ?></option>  
                                                @for ($i = 1; $i <= 12; $i++)
                                                <option value="{{$i}}">{{ $i }}</option>
                                                @endfor 
                                            </select>
                                    </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('month') }}</p>
                               </div>   
                                 <div class="col-md-2">Utility Year:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                            <?php  $date = strtotime('-1 years'); 
                                            $d_year =  date('Y', $date);
                                            ?>
                                             <!--<select name="utility_year" class="form-control"><option value="">Year</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option></select>-->                                                                                          
                                            <select class="form-control" name="year"> 
                                                <option><?= $row->year; ?></option>  
                                                @for ($i = $d_year; $i > 2000; $i--)
                                                <option value="{{$i}}">{{ $i }}</option>
                                                @endfor 
                                            </select>
                                    </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('year') }}</p>
                               </div>                                                          
                               
                               
                                         <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                    </div>
                                    <div class="col-md-11">
                                           <input type="submit" class="btn btn-sm btn-success pull-right" id="edit-btn" value="Update">
                                    </div>
                                     <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/electricconsumption')}}" class="btn btn-warning">Cancel</a> 
                                     </div>
                                   </div>
                              
                                
                            </div><!-- /.box-body -->
                        {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();
    
     $('[name="utility_year"]').val('{{$row->utility_year}}');    
     $('[name="utility_id"]').val('{{$row->utility_id}}');  
     $('[name="cat_id"]').val('{{$row->cat_id}}');   
     
     $('[name="year"]').val("{{old('year')}}");
    $('[name="month"]').val("{{old('month')}}");
});
</script>
@endsection

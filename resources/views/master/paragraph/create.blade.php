@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->
<link href="{!! asset('public/summernote-0.8.2/dist/summernote.css')!!}" rel="stylesheet">
<script src="{!! asset('public/summernote-0.8.2/dist/summernote.js') !!}"></script>
<style>
    .note-editor.note-frame {
        height: 211px !important;
    }
</style>
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>{{strtoupper($type)}}</h2>
            </div>

            <!-- CKEditor -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            
                              <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>   
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body" style="padding-bottom: 43px;">
                            {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'paragraph/'.$type]) !!}
                                <div id="paragraphs">{{isset($paragraph->description) ? $paragraph->description : ''}}</div>
                                <p style="color:red;">{{ $errors->first('description') }}</p>
                                <p style="color:red;">{{ $errors->first('name') }}</p>
                                <input type="hidden" name="_token" value="<?= csrf_token(); ?>">   
                                <input type="hidden" name="description" id="description">   
                                <input type="hidden" name="name" value="sust_admin">   
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Add">
                                </div>
                            {!! Form::close() !!}
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# CKEditor -->
            <!-- TinyMCE -->
            
            <!-- #END# TinyMCE -->
        </div>
    </section>
<script>
$(document).ready(function()
{
    $('#paragraphs').summernote({
  height: 150,   //set editable area's height
  codemirror: { // codemirror options
    theme: 'monokai'
  }
});
        //$('#paragraphs').summernote();
    
        $(document).on('click', '#create-btn', function (e) {
        e.preventDefault();
        var markupStroverview = $('#paragraphs').summernote('code');
      
        $('#description').val(markupStroverview);
      
        var form = $("[name=create]");
        form.submit();
    });
}); 
</script>
@endsection



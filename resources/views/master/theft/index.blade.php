@extends('layouts.app')

@section('content')
<style>
    .pagination{
        margin:0px !important;
    }
        .map {
        height: 200px;
        width: 100%;
       }
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjwmNTf25ITmZCpsKrQOh-KPyKOPyQ-5g"></script>
<script src="{!! asset('public/admin-material/markerwithlabel.js')!!}"></script>

<script>
var map;
function create_map(lat,long,elem) {

    var myLatlng, mapOptions, marker;
    var myLatlng = new google.maps.LatLng(lat, long);
    var div_id = document.getElementById(elem);
    mapOptions = {
        zoom: 15,
        center: myLatlng,
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    map = new google.maps.Map(div_id, mapOptions);
    
    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'LOCATION'
    });
}
  </script>
                                                            
<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <div class="col-lg-6">   
                               <h2>Theft Report</h2>
                            </div>
                            <div class="col-lg-4 text-right">    
                            </div>
                             <div class="col-lg-2 text-right">                                    
                                     <a class="btn btn-warning pull-right" href="<?php echo 'theft/excel_report'; ?>">Download Data</a>
                                </div>                                                        
                         <div class="box-header with-border">
                        <div class="row">
                            <div class="col-lg-10">
                               
                            </div>                            
                        </div>
                             <p style="color:#32CD32; margin: 0 0 10px 15px;" class="pull-right"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">                                                               
                                <div class="box-body">                            
                                <div class="pull-right"></div>
                                <div class="col-lg-2 pull-right" style="margin-top: 20px; padding-left: 0">                               
                                </div>
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table no-margin table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No.</th>    
                                                    <th>House Code</th>  
                                                    <th>Location</th> 
                                                    <th>Discription</th> 
                                                    <th>Map</th> 
                                                    <th>Confirm</th> 
                                                </tr>
                                            </thead>
                                            <tbody>                                         
                                                <?php $count =0; $i=0; ?>
                                                <script> var i =0;</script>
                                                @foreach ($data as $row) 
                                                <tr>
                                                    <td>{{++$count}}</td>
                                                    <td>{{$row->h_code}}</td>
                                                    <td>{{$row->location}}</td>
                                                    <td>{{$row->description}}</td>
                                                     <td style="width:400px;">                                                        
                                                         <?php if(!empty($row->latitude) && $row->latitude != ''){?>
                                                         <div id="map_id{{$row->id}}" class="create_map{{$row->id}}" style="height:150px;width:300px;"></div>                                                                              
                                                        <script>
                                                            create_map({{$row->latitude}},{{$row->longitude}},"map_id{{$row->id}}");
                                                        </script>                                                        
                                                         <?php }else{ ?>                                                             
                                                            <p>No Map Available</p>
                                                       <?php   } ?>
                                                    </td>
                                                    <td>    
                                                        <?php if($row->is_validated == 0){ ?>
                                                            <a href="#my_modal" class="btn btn-primary" data-toggle="modal" data-book-id="{{$row->id}}">Confirm</a>
                                                        <?php }else{ ?>
                                                            <label>Confirmed</label>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                               <?php $i++; ?>
                                                @endforeach                                     
                                            </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div>
                                </div><!-- /.box-body -->                   
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

<div class="modal" id="my_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Theft Report</h4>
      </div>
        {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'theft']) !!}
            <div class="modal-body">
              <p>Are you sure want confirmed theft report?</p>        
            </div>
            <input type="hidden" name="theftid" value="" />
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Submit">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
      {!! Form::close() !!}          
    </div>
  </div>
</div>
<script>
$(document).ready(function()
{
$('#my_modal').on('show.bs.modal', function(e) {
    var bookId = $(e.relatedTarget).data('book-id');
    $(e.currentTarget).find('input[name="theftid"]').val(bookId);
});
});
</script> 
@endsection
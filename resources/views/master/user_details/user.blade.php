@extends('layouts.app')

@section('content')
<style>
    .dropdown-menu {
        top:0% !important;
    } 
</style>
<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>User</h2>
                         <div class="box-header with-border"> 
                           <div class="row">                                
                                <div class="col-lg-10">
                                </div>                              
                                <div class="col-lg-2 text-right">
                                    <a class="btn btn-warning pull-right" href="<?php echo 'user-details/excel_report'; ?>">Download Data</a>
                                </div>                                
                            </div>
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                        </div><!-- /.box-header --> 
                        </div>                                                                           
                       <div class="row">
                        
                            <div class="col-lg-2 pull-right" style="margin-top: 00px; padding-left: 0">
                                <select name="limit" class="form-control" id="limit">
                                    <option value="">View All</option>
                                    <option value="50">50</option> 
                                    <option value="100">100</option>   
                                    <option value="150">150</option> 
                                    <option value="200">200</option>
                                </select>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row">
                        
                            </div>
                            <div class="table-responsive">
                                 <form id="create_form" name="create_form" class="form-horizontal" action="" method="post">
                        <div class="box-body">
                            <div class="pull-right"></div>
                            <div class="col-lg-2 pull-right" style="margin-top: 20px; padding-left: 0">

                            </div>
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table no-margin table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Mobile</th>
                                                <th>Verify Status</th>
                                                <th>Full Status</th>
                                        </thead>
                                        <tbody>                                         
                                            <?php $count =0; ?>
                                            @foreach ($data as $row) 
                                            <tr>
                                                <td>{{++$count}}</td>                                              
                                                <td>{{$row->name}}</td>   
                                                <td>{{$row->email}}</td> 
                                                <td>{{$row->mobile}}</td>
                                                <td>{{$row->verify_status > 0 ? 'Yes' : 'No'}}</td> 
                                                <td>{{$row->full_status > 0 ? 'Yes' : 'No'}}</td>  
                                            </tr>
                                            @endforeach                                     
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div>
                        </div><!-- /.box-body -->
                        </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

<script type="text/javascript">
$(document).ready(function()
{    
    $('[name="year"]').val('<?php echo (isset($post["year"]) ? $post["year"] : "") ?>');
    $('[name="city"]').val('<?php echo (isset($post["city"]) ? $post["city"] : "") ?>');
    
    $("#upload-btn").removeAttr('disabled');
    var form = $("[name=upload-excel]");
    form.validate();     
    
    $("#search-btn").removeAttr('disabled');
    var form = $("[name=search]");
    form.validate();
    
     var url = window.location.href;
        $('#limit').on('change', function () {
            var limit = $(this).val();
            var str = window.location.search;
            str = replaceQueryParam('limit', limit, str);
            str = replaceQueryParam('page', 1, str);
            window.location = window.location.pathname + str;
        });

        $('#limit').val(getParameterByName('limit'));

        function replaceQueryParam(param, newval, search) {
            var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
            var query = search.replace(regex, "$1").replace(/&$/, '');

            return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
        }

        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
        $('#limit').val('{{$limit}}');
});
</script>  
@endsection

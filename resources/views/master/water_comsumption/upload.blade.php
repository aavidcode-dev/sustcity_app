@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Utility</h2>
                             <div class="col-lg-12 text-right"> 
                                        <a class="btn btn-success pull-right" href="{{ url('/water-comsumption/downloadXls') }}">Download Sheet</a>
                                </div>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <div class="box-body">                                                                                                                                                          
                                <div class="body">
                                    {!! Form::open(['name'=>'upload-excel', 'id'=>'upload-excel','files'=>true, 'method'=>'POST', 'url'=>'water-comsumption/uploadexcelSave']) !!}
                        <div class="box-body">                                                                                                                                                          
                                <div class="body">                            
                                    <div class="row clearfix">
                                        <div class="col-sm-9" style=" margin-left: 5%;">
                                            <div class="form-group">
                                                <select class="form-control required" name="utility_id">
                                                    <option value=""></option>
                                                    @foreach($data as $type)
                                                        <option value="{{$type->id}}">{{$type->utility_name}}</option>
                                                    @endforeach
                                                </select>
                                                <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('utility_id') }}</p>
                                            </div>                                        
                                        </div>
                                        <div class="col-sm-9" style=" margin-left: 5%;">
                                            <div class="form-group">
                                                <div class="form-line">
                                                  <input type="file" class="form-control required" name="uploadexcel" id="uploadexcel"><br/> 
                                                </div>                                              
                                            </div>                                           
                                        </div> 
                                        
                                        <div class="col-md-12">
                                            <input type="submit" class="btn btn-sm btn-warning pull-right" id="upload-btn" value="Upload">
                                            <a href="{{ url('/water-comsumption') }}" class="btn btn-sm btn-primary pull-right" style="margin-right: 5px;">Back</a>
                                        </div>
                                    </div>                                   
                                </div>                                                   
                            <br/><br/>
                            <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                            <p style="color:red;margin: 0 0 10px 18%;">{{ $errors->first('uploadexcel') }}</p>
                            
                        </div><!-- /.box-body -->
                       {!! Form::close() !!}
                                </div>                                                   
                            <br/><br/>                                                      
                          
                        </div><!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
   $("#upload-btn").removeAttr('disabled');
    var form = $("[name=upload-excel]");
    form.validate();
       
});
</script>  
@endsection




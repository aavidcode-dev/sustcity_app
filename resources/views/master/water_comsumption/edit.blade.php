@extends('layouts.app')

@section('content')
   <section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Edit Water Consumptions</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                {!! Form::model($row, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'water-comsumption/' . $row->id, 'class'=>'form-horizontal']) !!}
                            <div class="box-body">
                                <div class="col-md-2">House Code:</div> 
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label><?= $row->h_code; ?></label>
                                            <input type="hidden" name="h_code" value="<?= $row->h_code; ?>">                                                </div>
                                        </div>
                                     <p style="color:red;">{{ $errors->first('h_code') }}</p>                                                          
                                </div>
                                <div class="col-md-2">BP:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="bp" value="<?= $row->bp; ?>">
                                                </div>
                                </div>
                                   
                                </div>
                                <div class="col-md-2">House Rank:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control required" name="rank" value="<?= $row->rank; ?>">
                                                </div>
                                        <p style="color:red;">{{ $errors->first('rank') }}</p>
                                </div>
                                   
                                </div>
                                 <div class="col-md-2">Utility Year:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <?php  $date = strtotime('-1 years'); 
                                        $d_year =  date('Y', $date);
                                        ?>
                                             <select name="utility_year" class="form-control">
                                                <option></option> 
                                                @for ($i = $d_year; $i > 2000; $i--)
                                                <option value="{{$i}}">{{ $i }}</option>
                                                @endfor 
                                             </select>
                                    </div>
                                </div>
                                <div class="col-md-2">Utility:</div>
                                <div class="col-md-4"> 
                                      <div class="form-group">
                                                <select class="form-control required" name="utility_id">
                                                    <option value=""></option>
                                                    @foreach($data1 as $type)
                                                        <option value="{{$type->id}}">{{$type->utility_name}}</option>
                                                    @endforeach
                                                </select>
                                </div>
                                </div>
                                <div class="col-md-2">Category:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">                                               
                                        <select class="form-control required" name="cat_id">
                                            <option value=""></option>
                                            @foreach($data2 as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>                                                 
                                </div>  
                                     </div>
                                <div class="col-md-2">January:</div>
                                <div class="col-md-4"> 
                                      <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="january" value="<?= $row->january; ?>">  
                                                </div>
                                </div>
                                </div>
                                <div class="col-md-2">February:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="february" value="<?= $row->february; ?>">
                                                </div>
                                </div>  
                                     </div>
                                  <div class="col-md-2">March:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="march" value="<?= $row->march; ?>">
                                                </div>
                                </div>
                                     </div>
                                <div class="col-md-2">April:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="april" value="<?= $row->april; ?>">    
                                                </div>
                                </div> 
                                     </div>
                                 <div class="col-md-2">May:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="may" value="<?= $row->may; ?>"> 
                                                </div>
                                </div>
                                     </div>
                                <div class="col-md-2">June:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="june" value="<?= $row->june; ?>">
                                                </div>
                                </div> 
                                     </div>
                                 <div class="col-md-2">July:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="july" value="<?= $row->july; ?>"> 
                                                </div>
                                </div>
                                </div>   
                                <div class="col-md-2">August:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="august" value="<?= $row->august; ?>">                                 
                                </div>
                                     </div></div>
                                 <div class="col-md-2">September:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="september" value="<?= $row->september; ?>">                         
                                </div>
                                     </div></div>
                                <div class="col-md-2">October:</div>
                                <div class="col-md-4"> 
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="october" value="<?= $row->october; ?>">    
                                                </div>
                                    </div>
                                </div> 
                                 <div class="col-md-2">November:</div>
                                <div class="col-md-4"> 
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="november" value="<?= $row->november; ?>">
                                                </div>
                                    </div>
                                </div>
                                <div class="col-md-2">December:</div>
                                <div class="col-md-4"> 
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="december" value="<?= $row->december; ?>">                                  
                                       <input type="hidden" name="e_id" value="<?php echo $row->id; ?>">
                                    <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                </div> 
                                    </div></div>
                               
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-sm btn-success pull-right" id="edit-btn" value="Update">
                                </div>
                            </div><!-- /.box-body -->
                        {!! Form::close() !!}
                            </div>
                        </div>
                    </div>                  
                </div>
                <!-- #END# Task Info -->
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();
      $('[name="utility_year"]').val('{{$row->utility_year}}');    
        $('[name="cat_id"]').val('{{$row->cat_id}}');    
          $('[name="utility_id"]').val('{{$row->utility_id}}');    
     
});
</script>
@endsection

@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Utility</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="    background: #FFF; padding-bottom: 29px; padding-top: 29px;">
                                                                                                                                                                              
                               
                                    {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'water-comsumption']) !!}
                                   
                                <div class="col-md-2">House Code:</div> 
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control required" name="h_code" value="{{old('h_code')}}">      
                                        </div>
                                    </div>
                                     <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('h_code') }}</p>
                                </div>
                                <div class="col-md-2">BP:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control required" name="bp" value="{{old('bp')}}">
                                        </div>
                                    </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('bp') }}</p>
                                </div>
                                <div class="col-md-2">House Rank:</div> 
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control required" name="rank" value="{{old('rank')}}">      
                                        </div>
                                    </div>
                                     <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('rank') }}</p>
                                </div>
                                <div class="col-md-2">Utility Year:</div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                         <?php  $date = strtotime('-1 years'); 
                                    $d_year =  date('Y', $date);
                                    ?>
                                         <!--<select name="utility_year" class="form-control"><option value="">Year</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option></select>-->
                                         <select class="form-control" name="utility_year"> 
                                                <option></option> 
                                                @for ($i = $d_year; $i > 2000; $i--)
                                                <option value="{{$i}}">{{ $i }}</option>
                                                @endfor 
                                        </select>
                                    </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('utility_year') }}</p>
                                </div>
                                <div class="col-md-2">Utility:</div>
                                <div class="col-md-4"> 
                                      <div class="form-group">
                                                <select class="form-control required" name="utility_id">
                                                    <option value=""></option>
                                                    @foreach($data1 as $type)
                                                        <option value="{{$type->id}}">{{$type->utility_name}}</option>
                                                    @endforeach
                                                </select>
                                </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('utility_id') }}</p>
                                </div>
                                <div class="col-md-2">Category:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                               
                                                <select class="form-control required" name="cat_id">
                                                    <option value=""></option>
                                                    @foreach($data2 as $type)
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                    @endforeach
                                                </select>
                                                
                                </div> 
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('cat_id') }}</p>
                                     </div>
                                <div class="col-md-2">January:</div>
                                <div class="col-md-4"> 
                                      <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="january" value="{{old('january')}}">  
                                                </div>
                                </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('january') }}</p>
                                </div>
                                <div class="col-md-2">February:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="february" value="{{old('february')}}">
                                                </div>
                                </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('february') }}</p>
                                     </div>
                                  <div class="col-md-2">March:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="march" value="{{old('march')}}">
                                                </div>
                                </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('march') }}</p>
                                     </div>
                                <div class="col-md-2">April:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="april" value="{{old('april')}}">    
                                                </div>
                                </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('april') }}</p>
                                     </div>
                                 <div class="col-md-2">May:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="may" value="{{old('may')}}"> 
                                                </div>
                                </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('may') }}</p>
                                     </div>
                                <div class="col-md-2">June:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="june" value="{{old('june')}}">
                                                </div>
                                </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('june') }}</p>
                                     </div>
                                 <div class="col-md-2">July:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="july" value="{{old('july')}}"> 
                                                </div>
                                </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('july') }}</p>
                                </div>   
                                <div class="col-md-2">August:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="august" value="{{old('august')}}">                                 
                                </div>
                                     </div>
                                  <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('august') }}</p>
                                </div>
                                 <div class="col-md-2">September:</div>
                                <div class="col-md-4"> 
                                     <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="september" value="{{old('september')}}">                         
                                </div>
                                     </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('september') }}</p>
                                </div>
                                <div class="col-md-2">October:</div>
                                <div class="col-md-4"> 
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="october" value="{{old('october')}}">    
                                                </div>
                                    </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('october') }}</p>
                                </div> 
                                 <div class="col-md-2">November:</div>
                                <div class="col-md-4"> 
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="november" value="{{old('november')}}">
                                                </div>
                                    </div>
                                      <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('november') }}</p>
                                </div>
                                <div class="col-md-2">December:</div>
                                <div class="col-md-4"> 
                                    <div class="form-group">
                                                <div class="form-line">
                                    <input type="text" class="form-control" name="december" value="{{old('december')}}">                                  
                                       
                                    <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                </div> 
                                    </div>
                                  <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('december') }}</p>
                                </div>
                               
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Add">
                                </div>
                         
                                       {!! Form::close() !!}
                                                                              
                            <br/><br/>                                                      
                        </div>
                       
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=create]");
    form.validate();    
     $('[name="utility_year"]').val('{{old("utility_year")}}');
      $('[name="utility_id"]').val('{{old("utility_id")}}');
       $('[name="cat_id"]').val('{{old("cat_id")}}');
});
</script>  
@endsection




@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                          
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>About Us</h2> 
                         <div class="box-header with-border">
                      <br/>
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                           
                                    <div class="box-body">                                                                                                                                                          
                                            <div class="body">  
                                                  @foreach ($data as $row) 
                                                   {!! Form::model($row, ['name'=>'edit', 'files' => true , 'method'=>'PUT', 'url'=> 'about_us/'.$row->id]) !!}
                                               
                                                <div class="row clearfix">  
                                                    <div class="col-sm-12">                                                        
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <textarea class="form-control required" name="description" id="description" placeholder="Description" value="{{ old('description') ? old('description') : $row->description }}">{{ old('description') ? old('description') : $row->description }}</textarea>                                                               
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('description') }}</p>
                                                        </div>   
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <img src="{{ url("/resources/assets/images/$row->id/$row->logo") }}">
                                                                <input type="file" class="form-control required" name="logo" id="logo" />                                                           
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('logo') }}</p>                                                               
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="hidden" name="a_id" value="{{ $row->id }}">
                                                <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Update">
                                                </div>
                                                    {!! Form::close() !!}
                                                    @endforeach    
                                            </div>                                                
                                       
                                    </div><!-- /.box-body -->
                               
                            </div>
                        </div>
                    </div>                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

<script type="text/javascript">
$(document).ready(function()
{    
    
});
</script>  
@endsection



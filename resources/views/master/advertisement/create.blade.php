@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Advertisement</h2>  
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'advertisement']) !!}
                                    <div class="box-body">                                                                                                                                                          
                                            <div class="body">                            
                                                <div class="row clearfix">  
                                                    <div class="col-sm-12"> 
                                                        <div class="form-group">  
                                                            <div class="form-line"> 
                                                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter title" value="{{old('title')}}" />                                                                                                                    
                                                            </div>
                                                               <p style="color:red;">{{ $errors->first('title') }}</p>
                                                        </div>  
                                                   
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" class="form-control" name="link" id="link" placeholder="Enter link" value="{{old('link')}}" />                                                               
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('link') }}</p>
                                                        </div> 
                                                       
                                                 
                                                    </div>
                                                </div>                                   
                                            </div>                                                
                                        <br/><br/>
                                        <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                        <p style="color:red;margin: 0 0 10px 18%;">{{ $errors->first('name') }}</p>
                                        <div class="col-md-11 col-sm-11">
                                            <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Add Advertisement">
                                        </div>
                                       <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/advertisement')}}" class="btn btn-warning">Cancel</a> 
                                     </div>
                                    </div><!-- /.box-body -->
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

<script type="text/javascript">
$(document).ready(function()
{    
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=create]");
    form.validate();   
    
    $('[name="year"]').val('{{old("year")}}');
   $('[name="month"]').val('{{old("month")}}');
   $('[name="max"]').val('{{old("max")}}');
   $('[name="min"]').val('{{old("min")}}');
});
</script>  
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
<script>
// just for the demos, avoids form submit

$( "#create" ).validate({
  rules: {
    title: {
      required: true,
    },
    link: {
      required: true,
      url: true
    }
  }
});
</script>
@endsection



@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Edit Advertisement</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            {!! Form::model($data, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'advertisement/' . $data->id]) !!}
                            <div class="box-body">                                                                                                                                                           
                                <div class="body">                            
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="title" value="<?php echo $data->title; ?>" id="title" placeholder="Enter title" />                                                    
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('title') }}</p>
                                            </div>
                                             <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="link" value="<?php echo $data->link; ?>" id="link" placeholder="Enter link" />                                                    
                                                </div>
                                                 <p style="color:red;">{{ $errors->first('link') }}</p>
                                            </div>                                            
                                        </div>
                                    </div>                                   
                                </div>                                                   
                            <br/><br/>
                               
                            <input type="hidden" name="a_id" value="<?php echo $data->id; ?>">
                            <div class="col-md-11 col-sm-11">
                                <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Update">
                            </div>
                             <div class="col-md-1 col-sm-1">
                                <a href="{{URL('/advertisement')}}" class="btn btn-warning">Cancel</a> 
                             </div>
                        </div><!-- /.box-body -->
                       {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

<script type="text/javascript">
$(document).ready(function()
{
        
});
</script>

@endsection
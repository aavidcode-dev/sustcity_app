@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Graph Details</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                        </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                 {!! Form::model($row, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'graph_details/'.$row->id]) !!}
                                    <div class="box-body">                                                                                                                                                          
                                            <div class="body">                            
                                                <div class="row clearfix">
                                                    <div class="col-sm-12">                                                        
                                                        <div class="form-group">
                                                            <label>Categoty</label>
                                                            <select class="form-control required" name="cat_id" id="cat_id">
                                                                <option value=""></option>
                                                                @foreach($category as $type)
                                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <p style="color:red;">{{ $errors->first('cat_id') }}</p>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label>Utility</label>
                                                            <select class="form-control required" name="utility_type_id" id="name">
                                                                <option value=""></option>
                                                                @foreach($utility as $type)
                                                                    <option value="{{$type->id}}">{{$type->utility_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <p style="color:red;">{{ $errors->first('utility_type_id') }}</p>
                                                        </div>  
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                 <label>Count</label>
                                                            <input type="text" class="form-control" name="count" value="<?php echo $row->count; ?>" id="example" placeholder="count" />                                                    
                                                            </div>
                                                            <p style="color:red;">{{ $errors->first('count') }}</p>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <label>Median</label>
                                                            <input type="text" class="form-control" name="median" value="<?php echo $row->median; ?>" id="example" placeholder="median" />                                                    
                                                            </div>
                                                            <p style="color:red;">{{ $errors->first('median') }}</p>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <label>Top</label>
                                                            <input type="text" class="form-control" name="top" value="<?php echo $row->top; ?>" id="example" placeholder="top" />                                                    
                                                            </div>
                                                            <p style="color:red;">{{ $errors->first('top') }}</p>
                                                        </div>
                                                         <div class="form-group">
                                                            <div class="form-line">
                                                        <label>Date</label>
                                                        <div class="input-group date " id="dmy">
                                                            <input type="text" name="dmy" value="{{$row->dmy}}" class="form-control ret input--textfield" id="firstd" autocomplete="off">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                        </div>
                                                             <input type="hidden" class="form-control" name="g_id" value="<?php echo $row->id; ?>" />                                                    
                                                        </div>
                                                    </div>
                                                </div>                                   
                                            </div>                                                   
                                        <br/><br/>                                                      
                                        <div class="col-md-11 col-sm-11">
                                               <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Update">
                                        </div>
                                        <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/graph_details')}}" class="btn btn-warning">Cancel</a>  
                                         </div>
                                    </div><!-- /.box-body -->
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>


<script type="text/javascript">
    $(document).ready(function()
    {
        $("#edit-btn").removeAttr('disabled');
        var form = $("[name=edit]");
        form.validate();
        $('[name="cat_id"]').val('{{$row->cat_id}}');  
        $('[name="utility_type_id"]').val('{{$row->utility_type_id}}');  
        $('[name="dmy"]').val('{{$row->dmy}}'); 
    });
</script>

<script type="text/javascript">
$(function()
{
    var startDate;
    var d = new Date();
    var open = false;

    $('#dmy').datetimepicker({
        allowInputToggle: true ,
        defaultDate: d,
//        autoclose: true,
        format: 'YYYY-MM-DD',
        minDate:{{$row->dmy}}
    }); 
});
</script>
@endsection
@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>FAQ</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                        </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                 {!! Form::model($row, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'faq/'.$row->id]) !!}
                                    <div class="box-body">                                                                                                                                                          
                                            <div class="body">                            
                                                <div class="row clearfix">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?= $row->title; ?>" />
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('title') }}</p>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <textarea class="form-control" name="description" id="description" placeholder="Description"><?= $row->description; ?></textarea>
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('description') }}</p>
                                                        </div> 
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <select class="form-control" name="status">
                                                                    <option value="1" @if($row->status==1) selected="selected"  @endif >Active</option>
                                                                    <option value="0" @if($row->status==0) selected="selected"  @endif>Deactive</option>
                                                                </select>
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('description') }}</p>
                                                             <input type="hidden" name="f_id" value="<?php echo $row->id; ?>">
                                                        </div> 
                                                    </div>
                                                </div>                                   
                                            </div>                                                   
                                        <br/><br/>                                                      
                                        <div class="col-md-11 col-sm-11">
                                               <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Updata">
                                        </div>
                                        <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/faq')}}" class="btn btn-warning">Cancel</a> 
                                         </div>
                                    </div><!-- /.box-body -->
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
@endsection


@section('pageFooterSpecificPlugin')
@endsection


@section('pageFooterSpecificJS')

<script type="text/javascript" src="{{ asset('public/js/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function()
{
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();
     $('[name="status"]').val('{{$row->status}}');
});
</script>
@endsection
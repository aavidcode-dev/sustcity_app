@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>FAQ</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                  {!! Form::open(['name'=>'create', 'id'=>'create', 'files'=>true, 'method'=>'POST', 'url'=>'faq']) !!}
                                    <div class="box-body">                                                                                                                                                          
                                            <div class="body">                            
                                                <div class="row clearfix">
                                                    <div class="col-sm-12">
                                                        <div class="form-group"> 
                                                            <div class="form-line"> 
                                                                <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="{{old('title')}}" />
                                                            </div>
                                                            <p style="color:red;">{{ $errors->first('title') }}</p>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <textarea class="form-control" name="description" id="description" placeholder="Description">{{old('description')}}</textarea>
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('description') }}</p>
                                                        </div>                                          
                                                    </div>
                                                </div>                                   
                                            </div>                                                   
                                        <br/><br/>                                                      
                                        <div class="col-md-11 col-sm-11">
                                               <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Add Faq">
                                        </div>
                                         <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/faq')}}" class="btn btn-warning">Cancel</a> 
                                        </div>
                                    </div><!-- /.box-body -->
                                   {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();   
    
   $('[name="utility_type_id"]').val('{{old("utility_type_id")}}');
   $('[name="premium"]').val('{{old("premium")}}');
   $('[name="cat_id"]').val('{{old("cat_id")}}');
});
</script>  
@endsection





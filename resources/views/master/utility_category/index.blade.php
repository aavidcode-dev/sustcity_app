@extends('layouts.app')

@section('content')
<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>House Category</h2>
                         <div class="box-header with-border">
                        <div class="row">
                            <div class="col-lg-6">
                               
                            </div> 
                            <div class="col-lg-2 text-right">        
                                 <a class="btn btn-warning pull-right" href="<?php echo 'utility_category/excel_report'; ?>">Download Data</a>
                                </div>
                            <div class="col-lg-2 text-right">
                                  <a class="btn btn-primary pull-right" href="<?php echo 'utility_category/create'; ?>">Add Category</a>
                            </div>
                            <div class="col-lg-2 text-right">   
                                    <a class="btn btn-primary pull-right" href="<?php echo 'utility_category/upload'; ?>">Upload Sheet</a>
                                </div>
                        </div>
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                 <form id="create_form" name="create_form" class="form-horizontal" action="" method="post">
                        <div class="box-body">
                            <div class="pull-right"></div>
                            <div class="col-lg-2 pull-right" style="margin-top: 20px; padding-left: 0">
                               
                            </div>
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table no-margin table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Category Id</th>
                                                <th>Category Name</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                         
                                            <?php $count =0; ?>
                                            @foreach ($data as $row) 
                                            <tr>
                                                <td>{{++$count}}</td>
                                                <td>{{$row->id}}</td> 
                                                <td>{{$row->name}}</td> 
                                                <td>
                                                    <?php if($row->status == 1){  ?>
                                                        <span class="label label-success">Active</span>
                                                    <?php } else{ ?>
                                                        <span class="label label-danger">De-Active</span>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <a class="btn btn-warning" href="<?php echo 'utility_category/' . $row->id ?>/edit">Edit</a>                                                                                                          
                                                    <?php if($row->status == 1){  ?>   
                                                    <a href="#my_modal" data-toggle="modal" data-book-id="{{$row->id}}"><i class="material-icons">clear</i></a>                                                    
                                                    <?php } else{ ?>
                                                    <a href="#my_modal1" data-toggle="modal" data-book-id="{{$row->id}}"><i class="material-icons">done</i></a>                                                     
                                                    <?php } ?>                                                  
                                                </td>
                                            </tr>
                                            @endforeach                                     
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

<div class="modal" id="my_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'utility_category/disable']) !!}
            <div class="modal-body">
              <p>Are you sure want to delete this utility category?</p>        
            </div>
            <input type="hidden" name="u_id" value="" />
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Submit">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
      {!! Form::close() !!}          
    </div>
  </div>
</div>
<div class="modal" id="my_modal1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'utility_category/enable']) !!}
            <div class="modal-body">
              <p>Are you sure want to enable this utility category?</p>        
            </div>
            <input type="hidden" name="u_id" value="" />
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Submit">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
      {!! Form::close() !!}          
    </div>
  </div>
</div>
<script>
$(document).ready(function() 
{                 
    $('#my_modal').on('show.bs.modal', function(e) { 
        var bookId = $(e.relatedTarget).data('book-id');
        $(e.currentTarget).find('input[name="u_id"]').val(bookId);
    });
});
</script> 
<script>
$(document).ready(function() 
{                 
    $('#my_modal1').on('show.bs.modal', function(e) { 
        var bookId = $(e.relatedTarget).data('book-id');
        $(e.currentTarget).find('input[name="u_id"]').val(bookId);
    });
});
</script> 
@endsection

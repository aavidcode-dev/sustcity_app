@extends('layouts.app')

@section('content')
<style>
     hr{
        margin-top: 40px !important; 
    margin-bottom: 0px !important; 
    }
    .pad-search{
            padding: 32px 0px 10px 5px !important;
    }
</style>
<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>House Details</h2> 
                         <div class="box-header with-border">
                            <div class="row">
                                
                                <div class="col-lg-6">
                                </div>
                               <div class="col-lg-2 text-right">
                                    <a class="btn btn-primary pull-right" href="<?php echo 'housedetails/create'; ?>">Add Home Details</a>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <a class="btn btn-warning pull-right" href="<?php echo 'housedetails/excel_report'; ?>">Download Data</a>
                                </div>
                                 <div class="col-lg-2 text-right">   
                                    <a class="btn btn-success pull-right" href="<?php echo 'housedetails/upload'; ?>">Upload Data</a>
                                </div>
                            </div>
                                                  
                             
                             
                              <?php 
                            $report = Session::get('unsuccess_message');
                           
                            ?>
                              <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                            @if (isset($report))
                            <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                Success: {{isset($report['success']) ? count($report['success']) : 0}}<br />
                                Fail: {{isset($report['fail']) ? count($report['fail']) : 0}}<br />
                                @if (isset($report['fail']) && count($report['fail'] > 0))
                                <ul>
                                    @foreach($report['fail'] as $ho_code => $ho_data) 
                                    <li>
                                        {{$ho_code}}
                                        <ul>
                                            @foreach($ho_data as $key => $message)
                                            <li>{{$key}} : {{$message[0]}}</li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </div>
                            @endif
                        </div><!-- /.box-header --> 
                    
                    
                        </div>
                        
                       <div class="col-lg-12 pad-search">
                                <form id="create_form" class="form-horizontal" action="{{ url('/housedetails') }}" method="get">
                                    <div class="form-group"> 
                                        
                                            <div class="col-lg-3 col-sm-3 col-md-3" style="margin-left: 5%;">
                                            <div class="form-line">
                                                  <label>House Code</label>
                                                <input type="text" name="h_code" class="form-control" placeholder="House Code" />
                                            </div>
                                            </div>
                                                                               
                                            <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                            <div class="col-lg-1"><input type="submit" class="btn btn-warning" value="search"></div>
                                            <div class="col-lg-1"><a href="{{ url('/housedetails') }}" class="btn btn-danger">Reset</a></div>
                                           
                                    </div>
                                    <hr>
                                </form>
                            </div>
                         
                        <div class="row">
                                   <div class="col-lg-6" style="margin-top: 0px; margin-bottom: 0px;">
                        <div class="pull-right">{!!$data->appends($post)->render()!!}</div>
                                   </div>
                            <div class="col-lg-2 pull-right" style="margin-top: 0px; padding-left: 0">
                                <select name="limit" class="form-control" id="limit">
                                    <option value="">View All</option>
                                    <option value="50">50</option> 
                                    <option value="100">100</option>   
                                    <option value="150">150</option> 
                                    <option value="200">200</option>
                                </select>
                            </div>
                        </div>  
                        <div class="body">
                              
                            <div class="table-responsive">
                                 <form id="create_form" name="create_form" class="form-horizontal" action="" method="post">
                        <div class="box-body">
                            <div class="pull-right"></div>
                            <div class="col-lg-2 pull-right" style="margin-top: 20px; padding-left: 0">
                               
                            </div>
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table no-margin table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>    
                                                <th>House Code</th>
                                                <th>Name</th>                                         
                                                <th>Partner</th>                                                                                        
                                                <th>Address</th>
                                                <th></th>                                                                                        
                                            </tr>
                                        </thead>
                                        <tbody>                                         
                                            <?php $count =0; ?>
                                            @foreach ($data as $row) 
                                            <tr>
                                                <td>{{++$count}}</td>
                                                <td>{{$row->h_code}}</td>    
                                                <td>{{$row->name}}</td>  
                                                <td>{{$row->partner}}</td>                                                 
                                                <td>{{$row->address}}</td>  
                                                <td>
                                                    <a class="" href="<?php echo 'housedetails/' . $row->id ?>/edit">Edit</a>
                                                </td>
                                            </tr>
                                            @endforeach                                     
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $('[name="h_code"]').val('<?php echo (isset($post["h_code"]) ? $post["h_code"] : "") ?>'); 
    $('[name="name"]').val('<?php echo (isset($post["name"]) ? $post["name"] : "") ?>');   
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=upload-excel]");
    form.validate();   
    
    
      var url = window.location.href;
        $('#limit').on('change', function () {
            var limit = $(this).val();
            var str = window.location.search;
            str = replaceQueryParam('limit', limit, str);
            str = replaceQueryParam('page', 1, str);
            window.location = window.location.pathname + str;
        });

        $('#limit').val(getParameterByName('limit'));

        function replaceQueryParam(param, newval, search) {
            var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
            var query = search.replace(regex, "$1").replace(/&$/, '');

            return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
        }

        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
        $('#limit').val('{{$limit}}');
});
</script>  
@endsection

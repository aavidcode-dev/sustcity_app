@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Edit Utility House Details</h2>
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                 {!! Form::model($row, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'housedetails/' . $row->id, 'class'=>'form-horizontal']) !!}
                            <div class="box-body">                                                                                                                                                          
                                    <div class="body">                            
                                        <div class="row clearfix">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="col-md-2 col-sm-2"><label>House Code:</label></div>
                                                    <div class="col-md-10 col-sm-10">                                                                                                     
                                                        <input type="text" class="form-control " name="h_code" value="<?= $row->h_code; ?>" readonly="readonly"> 
                                                      <p style="color:red;">{{ $errors->first('h_code') }}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                     <div class="col-md-2 col-sm-2"><label>Mobile:</label></div>
                                                    <div class="col-md-10 col-sm-10">                                                    
                                                         <input type="text" class="form-control" name="mobile" value="<?= $row->mobile; ?>" readonly="readonly">                                                                                                                
                                                    </div>
                                                     <p style="color:red;">{{ $errors->first('mobile') }}</p>
                                                </div>
                                                <div class="form-group">
                                                     <div class="col-md-2 col-sm-2"><label>Name:</label></div>
                                                    <div class="col-md-10 col-sm-10">
                                                    <div class="form-line">
                                                         <input type="text" class="form-control " name="name" value="<?= $row->name; ?>"> 
                                                    </div>
                                                          <p style="color:red;">{{ $errors->first('name') }}</p> 
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                     <div class="col-md-2 col-sm-2"><label>City:</label></div>
                                                    <div class="col-md-10 col-sm-10">
                                                    <div class="form-line">
                                                         <input type="text" class="form-control " name="city" value="<?= $row->city; ?>"> 
                                                    </div>
                                                          <p style="color:red;">{{ $errors->first('city') }}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                     <div class="col-md-2 col-sm-2"><label>Partner:</label></div>
                                                    <div class="col-md-10 col-sm-10">
                                                    <div class="form-line">
                                                         <input type="text" class="form-control " name="partner" value="<?= $row->partner; ?>"> 
                                                    </div>
                                                          <p style="color:red;">{{ $errors->first('partner') }}</p>
                                                    </div>
                                                </div>
                                               
                                                <div class="form-group">
                                                    <div class="col-md-2 col-sm-2"><label>Address:</label></div>
                                                    <div class="col-md-10 col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control " name="address" value="<?= $row->address; ?>">  
                                                        <input type="hidden" name="u_id" value="<?php echo $row->id; ?>">
                                                        <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                                    </div>
                                                             <p style="color:red;">{{ $errors->first('address') }}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-2 col-sm-2"><label>Utility:</label></div>
                                                    <div class="col-md-10 col-sm-10">
                                                        <select class="form-control " name="utility_id">
                                                            <option value=""></option>
                                                            @foreach($data1 as $type)
                                                                <option value="{{$type->id}}">{{$type->utility_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <p style="color:red;">{{ $errors->first('utility_name') }}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                               <div class="col-md-2 col-sm-2"><label>Category:</label></div>
                                                       <div class="col-md-10 col-sm-10">
                                                    <select class="form-control " name="cat_id">
                                                        <option value=""></option>
                                                        @foreach($data2 as $type)
                                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                                        @endforeach
                                                    </select>
                                                      <p style="color:red;">{{ $errors->first('name') }}</p>
                                                       </div>                                                                                           
                                                </div>
                                                
                                                  <div class="col-md-11">
                                                     <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Update">
                                                  </div>
                                                   <div class="col-md-1 col-sm-1">
                                                        <a href="{{URL('/housedetails')}}" class="btn btn-warning">Cancel</a> 
                                                   </div>
                                                  
                                            </div>
                                        </div>                                   
                                    </div>                                                   
                                <br/><br/>                                                      
                                <p style="color:red;margin: 0 0 10px 18%;">{{ $errors->first('h_code') }}</p>
                            </div><!-- /.box-body -->
                       {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();
    $('[name="utility_id"]').val('{{$row->utility_id}}'); 
    $('[name="cat_id"]').val('{{$row->cat_id}}');       
});
</script>
@endsection


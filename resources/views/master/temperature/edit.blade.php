@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Temperature</h2> 
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                {!! Form::model($row, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'temperature/' . $row->id]) !!}
                                    <div class="box-body">                                                                                                                                                          
                                            <div class="body">                            
                                                <div class="row clearfix"> 
                                                    <div class="col-sm-12"> 
                                                        <div class="form-group">  
                                                            <div class="form-line"> 
                                                                <select class="form-control" name="month"><option value="">Month</option><option value="Jan">Jan</option><option value="Feb">Feb</option><option value="Mar">Mar</option><option value="Apr">Apr</option><option value="May">May</option><option value="Jun">Jun</option><option value="Jul">Jul</option><option value="Aug">Aug</option><option value="Sept">Sept</option><option value="Oct">Oct</option><option value="Nov">Nov</option><option value="Dec">Dec</option></select>
                                                            </div>
                                                            <p style="color:red;">{{ $errors->first('month') }}</p>
                                                        </div>  
                                                        <div class="form-group">
                                                            <div class="form-line">       
                                                                <select name="year" class="form-control"><option value="">Year</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option></select>
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('year') }}</p>
                                                        </div>  
                                                        <div class="form-group">
                                                            <div class="form-line"> 
                                                                <input type="text" class="form-control" name="max" value="<?= $row->max; ?>" placeholder="Max" />
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('max') }}</p>
                                                        </div> 
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" class="form-control" name="min" value="<?= $row->min; ?>" placeholder="Min" />
                                                                 <input type="hidden" name="t_id" value="<?php echo $row->id; ?>">
                                                                <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('min') }}</p>
                                                        </div> 
                                                    </div>
                                                </div>                                   
                                            </div>                                                
                                        <br/><br/>
                                        <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                        <p style="color:red;margin: 0 0 10px 18%;">{{ $errors->first('name') }}</p>
                                        <div class="col-md-11">
                                            <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Update">
                                        </div>
                                          <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/temperature')}}" class="btn btn-warning">Cancel</a> 
                                         </div>
                                    </div><!-- /.box-body -->
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();   
    
    $('[name="month"]').val('{{$row->month}}');
    $('[name="year"]').val('{{$row->year}}');
    $('[name="max"]').val('{{$row->max}}');
    $('[name="min"]').val('{{$row->min}}');
});
</script> 
@endsection







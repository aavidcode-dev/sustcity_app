@extends('layouts.app')

@section('content')
<style>
    .dropdown-menu {
        top:0% !important;
    } 
</style>
<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>Temperature</h2>
                         <div class="box-header with-border">
                        <div class="row">
                             <div class="col-lg-8">
                               
                            </div>
                            
                            <div class="col-lg-2 text-right">
                                  <a class="btn btn-primary pull-right" href="<?php echo 'temperature/create'; ?>">Add Temperature</a>
                            </div> 
                              <div class="col-lg-2 text-right">   
                                    <a class="btn btn-primary pull-right" href="<?php echo 'temperature/upload'; ?>">Upload Sheet</a> 
                                </div>
                        </div>
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                     
                              
                        {!! Form::open(['name'=>'search', 'id'=>'search', 'method'=>'GET', 'url'=>'temperature']) !!}                                                                                                                                                                                                                                    
                            <div class="box-body">                         
                                <div class="col-sm-3" style=" margin-left: 3%;     margin-top: 6%;">
                                    <div class="form-group">
                                        <label>City</label>
                                        <select class="form-control" name="city">
                                            <option value=""></option>
                                            @foreach($city as $type)
                                                <option value="{{$type->id}}">{{$type->city}}</option>
                                            @endforeach 
                                        </select> 
                                        <p style="color:red;     margin: 0 0 10px 18%;">{{ $errors->first('city') }}</p>
                                         <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                    </div>                                        
                                </div>
                                <div class="col-sm-3" style=" margin-left: 3%;     margin-top: 6%;">
                                    <div class="form-group">
                                        <label>Year</label>
                                            <select class="form-control" name="year"> 
                                                <option></option>                                                
                                                @for ($i = date('Y'); $i > 2000; $i--)
                                                <option value="{{$i}}">{{ $i }}</option>
                                                @endfor 
                                            </select>
                                        <p style="color:red; margin: 0 0 10px 18%;">{{ $errors->first('utility_year') }}</p>                                      
                                    </div>                                        
                                </div>
                                <div class="col-md-2" style="margin-top: 6%;">
                                    <input type="submit" class="btn btn-sm btn-warning pull-right" id="search-btn" value="Search"> 
                                </div>
                                 <div class="col-lg-1" style=" margin-top: 6%;"><a href="{{ url('/temperature') }}" class="btn btn-danger">Reset</a></div> 
                                <br/><br/> 
                                                                                      
                            </div>
                       {!! Form::close() !!}   
                       <div class="row">
                        
                            <div class="col-lg-2 pull-right" style="margin-top: 00px; padding-left: 0">
                                <select name="limit" class="form-control" id="limit">
                                    <option value="">View All</option>
                                    <option value="50">50</option> 
                                    <option value="100">100</option>   
                                    <option value="150">150</option> 
                                    <option value="200">200</option>
                                </select>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row">
                            <div class="col-md-3 pull-right">{!!$data->appends($post)->render()!!}</div>
                            </div>
                            <div class="table-responsive">
                                 <form id="create_form" name="create_form" class="form-horizontal" action="" method="post">
                        <div class="box-body">
                            <div class="pull-right"></div>
                            <div class="col-lg-2 pull-right" style="margin-top: 20px; padding-left: 0">

                            </div>
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table no-margin table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Year</th>                                         
                                                <th>Month</th>
                                                <th>Max</th>
                                                <th>Min</th>
                                                <th>City</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>                                         
                                            <?php $count =0; ?>
                                            @foreach ($data as $row) 
                                            <tr>
                                                <td>{{++$count}}</td>                                              
                                                <td>{{$row->year}}</td>  
                                                <td>{{$row->month}}</td>  
                                                <td>{{$row->max}}</td>   
                                                <td>{{$row->min}}</td> 
                                                <td>{{$row->city->city}}</td> 
                                                <td>
                                                    <a class="" href="<?php echo 'temperature/' . $row->id ?>/edit">Edit</a>
                                                </td>
                                            </tr>
                                            @endforeach                                     
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div>
                        </div><!-- /.box-body -->
                        </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>



<script type="text/javascript">
$(document).ready(function()
{    
    $('[name="year"]').val('<?php echo (isset($post["year"]) ? $post["year"] : "") ?>');
    $('[name="city"]').val('<?php echo (isset($post["city"]) ? $post["city"] : "") ?>');
    
    $("#upload-btn").removeAttr('disabled');
    var form = $("[name=upload-excel]");
    form.validate();     
    
    $("#search-btn").removeAttr('disabled');
    var form = $("[name=search]");
    form.validate();
    
     var url = window.location.href;
        $('#limit').on('change', function () {
            var limit = $(this).val();
            var str = window.location.search;
            str = replaceQueryParam('limit', limit, str);
            str = replaceQueryParam('page', 1, str);
            window.location = window.location.pathname + str;
        });

        $('#limit').val(getParameterByName('limit'));

        function replaceQueryParam(param, newval, search) {
            var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
            var query = search.replace(regex, "$1").replace(/&$/, '');

            return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
        }

        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
        $('#limit').val('{{$limit}}');
});
</script>  
@endsection

@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Temperature</h2> 
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                {!! Form::open(['name'=>'create', 'id'=>'create', 'method'=>'POST', 'url'=>'temperature']) !!}
                                    <div class="box-body">                                                                                                                                                          
                                            <div class="body">                            
                                                <div class="row clearfix">  
                                                    <div class="col-sm-12"> 
                                                        <div class="form-group">  
                                                            <div class="form-line"> 
                                                                <select class="form-control required" name="month"><option value="">Month</option><option value="Jan">Jan</option><option value="Feb">Feb</option><option value="Mar">Mar</option><option value="Apr">Apr</option><option value="May">May</option><option value="Jun">Jun</option><option value="Jul">Jul</option><option value="Aug">Aug</option><option value="Sept">Sept</option><option value="Oct">Oct</option><option value="Nov">Nov</option><option value="Dec">Dec</option></select>                                                              
                                                            </div>
                                                               <p style="color:red;">{{ $errors->first('month') }}</p>
                                                        </div>  
                                                        <div class="form-group">
                                                            <div class="form-line">      
                                                                <select name="year" class="form-control required">
                                                                    <option></option> 
                                                                    @for ($i = date('Y'); $i > 2000; $i--)
                                                                    <option value="{{$i}}">{{ $i }}</option>
                                                                    @endfor 
                                                                </select>                                                               
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('year') }}</p>
                                                        </div>  
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" class="form-control required" name="max" id="name" placeholder="Max" value="{{old('max')}}" />                                                               
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('max') }}</p>
                                                        </div> 
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" class="form-control required" name="min" id="name" placeholder="Min" value="{{old('min')}}"/>                                                                
                                                            </div>
                                                            <p style="color:red;">{{ $errors->first('min') }}</p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>City</label>
                                                           <select class="form-control required" name="city_id" id="city_id">
                                                               <option value=""></option>
                                                               @foreach($city as $type)
                                                                   <option value="{{$type->id}}">{{$type->city}}</option>
                                                               @endforeach
                                                           </select> 
                                                             <p style="color:red;">{{ $errors->first('city') }}</p>
                                                       </div>
                                                    </div>
                                                </div>                                   
                                            </div>                                                
                                        <br/><br/>
                                        <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                        <p style="color:red;margin: 0 0 10px 18%;">{{ $errors->first('name') }}</p>
                                        <div class="col-md-11 col-sm-11">
                                            <input type="submit" class="btn btn-sm btn-success pull-right" id="create-btn" value="Add Temperature">
                                        </div>
                                         <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/temperature')}}" class="btn btn-warning">Cancel</a> 
                                        </div>
                                    </div><!-- /.box-body -->
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>

<script type="text/javascript">
$(document).ready(function()
{    
    $("#create-btn").removeAttr('disabled');
    var form = $("[name=create]");
    form.validate();   
    
    $('[name="year"]').val('{{old("year")}}');
   $('[name="month"]').val('{{old("month")}}');
   $('[name="max"]').val('{{old("max")}}');
   $('[name="min"]').val('{{old("min")}}');
});
</script>  
@endsection



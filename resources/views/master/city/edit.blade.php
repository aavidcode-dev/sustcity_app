@extends('layouts.app')

@section('content')
<!-- Library - Jquery Validator -->

<section class="content">
        <div class="container-fluid">                                         
            <div class="row clearfix">
                <!-- Task Info --> 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="header">
                            <h2>City</h2> 
                         <div class="box-header with-border">
                      
                         <p style="color:#32CD32; margin: 0 0 10px 15px;"><?php echo Session::get('message'); ?></p>
                         <p style="color:red; margin: 0 0 10px 15px;"><?php echo Session::get('unsuccess_message'); ?></p>                         
                    </div><!-- /.box-header --> 
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                {!! Form::model($row, ['name'=>'edit', 'method'=>'PUT', 'url'=> 'city/' . $row->id]) !!}
                                    <div class="box-body">                                                                                                                                                          
                                            <div class="body">                            
                                                <div class="row clearfix"> 
                                                    <div class="col-sm-12">                                                                                                                  
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" class="form-control" name="city" value="<?= $row->city; ?>" placeholder="City Name" />
                                                                 <input type="hidden" name="c_id" value="<?php echo $row->id; ?>">
                                                                <input type="hidden" name="_token" value="<?= csrf_token(); ?>">
                                                            </div>
                                                             <p style="color:red;">{{ $errors->first('city') }}</p>
                                                        </div> 
                                                    </div>
                                                </div>                                   
                                            </div>                                                
                                        <br/><br/>
                                        <input type="hidden" name="_token" value="<?= csrf_token(); ?>">                                       
                                        <div class="col-md-11 col-sm-11"> 
                                           <input type="submit" class="btn btn-sm btn-success pull-right" id="edit-btn" value="Update">
                                        </div>
                                        <div class="col-md-1 col-sm-1">
                                            <a href="{{URL('/city')}}" class="btn btn-warning">Cancel</a> 
                                         </div>
                                    </div><!-- /.box-body -->
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </section>
<script type="text/javascript">
$(document).ready(function()
{    
    $("#edit-btn").removeAttr('disabled');
    var form = $("[name=edit]");
    form.validate();   
    
    $('[name="city"]').val('{{$row->city}}');

});
</script> 
@endsection







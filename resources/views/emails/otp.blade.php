@extends('emails.layout')

@section('email-specific-css')
<style type="text/css">
    .prodtbl{ 
        border-collapse: solid #dcdcdc;
    }
    .prodtbl heading{
        font-size: 12px;
        text-align: center; 
    }
    .prodtbl td{
        width:80px;
        font-size: 10px;
        text-align: center;
    }
</style>
@stop

@section('content')
<label>Dear Customer,</label><br/><br/>

Your otp is: <br/><br/><br/>


<a href="{{URL::to('/')}}">Click here</a> to login
@stop

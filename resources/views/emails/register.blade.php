@extends('emails.layout')

@section('email-specific-css')
<style type="text/css">
    .prodtbl{ 
        border-collapse: solid #dcdcdc;
    }
    .prodtbl heading{
        font-size: 12px;
        text-align: center; 
    }
    .prodtbl td{
        width:80px;
        font-size: 10px;
        text-align: center;
    }
</style>
@stop

@section('content')
Thank you for registration.
@stop
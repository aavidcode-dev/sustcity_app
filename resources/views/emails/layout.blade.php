<?php date_default_timezone_set('Asia/Calcutta'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Email</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href='https://fonts.googleapis.com/css?family=Noto+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css' />
        @yield('email-specific-link')
        <style type="text/css">
            /* General */


            a { color: #345498; text-decoration: none; }

            .bg-eee { text-align: center }

            #container { width: 700px; background-color: #f8f8f8;
            }
            #content { padding: 28px 47px 20px 47px; overflow: hidden; }

            #inner-content {
                background-color: #fff;
                margin:50px;
            }
            /* General */

            /* Footer */

            #footer { width: 600px; margin-top:30px;}
            #footer a { color:#fff; }
            #timestamp { color: #EEE; }
            /* Footer */

            /* Header */
            #header { min-height: 76px; width: 600px; }
            #headerDivider { float: left; }
            #headerDivider img { float: left; margin: 27px 24px auto 20px; }
            #logo {  }
            #logo img { margin: 40px auto auto 40px; }
            #tagLine { float: left; }
            #tagLine img { margin-top: 32px; }

            #footer-links a {padding:6px;}
            /* Header */
        </style>
        @yield('email-specific-css')
    </head>
    <body style="padding:0; margin:0;">
        <div  style="width: 700px; background: #f8f8f8; margin:0 auto;
              font-family: Arial,Tahoma,Verdana,sans-serif;
              font-size:16px;
              -webkit-text-size-adjust: 100% !important;
              -ms-text-size-adjust: 100% !important;
              -webkit-font-smoothing: antialiased !important; padding:50px;">
            <div style="background: #fff;height:auto;">
                <div id="header" class="bg-eee" style="text-align: center; min-height: 76px; width: 100%;">
                    <div id="logo">
                        <a href="{{ URL::to('/')}}" target="_blank"><img src="{{asset('resources/assets/images/lobaanya_logo_small.png')}}" alt="Lobaanya" width="230" style="margin-top: 40px;"/></a>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div id="content" style="margin-top:20px; padding: 28px 47px; overflow: hidden; width:600px; min-height:
                     200px;">
                    @yield('content')
                </div> 
                <div style="clear: both;"></div>
                <div class="footer" style="font-size:12px; padding: 20px 40px; background-color: #171719; color: #666666;">
                    <div style="width:60%; display: inline-block; vertical-align: middle;">
                        <div>
                            <strong style="font-size:14px;">Address: </strong> <br />
                            
                        </div>    
                        <div style="margin-top:5px;"><strong style="font-size:13px;">Phone: </strong>+91 9999999999</div>
                        <div style="margin-top:5px;"><strong style="font-size:13px;">Website: </strong>www.sustcity.com</div>
                    </div>
                    <div style="width:39%; display: inline-block; vertical-align: middle; text-align: right;">
                        <ul style="list-style-type: none;">
                            <li style="display: inline-block"><a href="https://facebook.com/lobaanya"><img src="{!! asset('resources/assets/images/social/facebook_circle_color.png') !!}" alt="Facebook" /></a></li>
                            <li style="display: inline-block"><a href="https://twitter.com/lobaanya"><img src="{!! asset('resources/assets/images/social/twitter_circle_color.png') !!}" alt="Twitter" /></a></li>
                            <li style="display: inline-block"><a href="https://instagram.com/lobaanya/"><img src="{!! asset('resources/assets/images/social/instagram_circle_color.png') !!}" alt="Instagram" /></a></li>
                            <li style="display: inline-block"><a href="https://plus.google.com/108249059578053418314"><img src="{!! asset('resources/assets/images/social/google_circle_color.png') !!}" alt="Goolge Plus" /></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

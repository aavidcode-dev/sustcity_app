<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Faq extends Model {
 
    protected $table = 'faqs';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['title','description','status'];  
    protected $rules = [ 
        'title' => 'required',
        'description' => 'required'
    ]; 
    protected $customMessages = [
        'title.required' => 'Required', 
        'description.required' => 'Required' 
    ]; 
    
    public static function rules($id = 0) {
        return [                      
            'title' => 'required',
        'description' => 'required'              
        ];
    }
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

}

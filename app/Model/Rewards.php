<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Rewards extends Model {
 
    protected $table = 'rewards';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['name','value','code'];  
    protected $rules = [ 
        'name' => 'required',
        'value' => 'required|numeric',
        'code' => 'required'
    ]; 
    protected $customMessages = [
        'name.required' => 'Required', 
        'value.required' => 'Required',
        'code.required' => 'Required'
    ]; 
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
    
    

}

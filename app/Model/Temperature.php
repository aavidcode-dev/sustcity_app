<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Temperature extends Model {
 
    protected $table = 'temperature';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['month','year','min','max','city_id'];  
    protected $rules = [ 
        'month' => 'required',
        'year' => 'required',
        'min' => 'required|numeric',
        'max' => 'required|numeric',
        
    ]; 
    protected $customMessages = [
        'month.required' => 'Required',
        'year.required' => 'Required',
        'min.required' => 'Required',
        'max.required' => 'Required'
    ]; 
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
    
    public function city() {
        return $this->belongsTo('\App\Model\City', 'city_id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Knowmore extends Model {

    protected $table = 'know_more';
    protected $hidden = [];
    protected $fillable = ['h_code','mobile','created_at'];  
    public $timestamps = true;
    protected $customMessages = [
       
    ];
    
    public static function rules($id = 0) {
        return [
           
        ];
    }

    public function getRules() {
        return $this->rules;
    }
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
}

?>
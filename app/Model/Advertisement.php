<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Advertisement extends Model {
 
    protected $table = 'advertisements';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['title','link','image_utl','status'];  
    protected $rules = [ 
        'title' => 'required',
        'link' => 'required'
      
    ]; 
    protected $customMessages = [
        'title.required' => 'Required', 
        'link.required' => 'Required'
        
    ]; 
    
    public static function rules($id = 0) {
        return [                      
            'title' => 'required',
            'link' => 'required'
            
        ];
    }
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

}

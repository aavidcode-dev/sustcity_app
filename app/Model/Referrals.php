<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Referrals extends Model {
 
    protected $table = 'referrals';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['referee_mobile','referred_mobile','ref_code'];  
    protected $rules = [ 
        'referee_mobile' => 'required',
        'referred_mobile' => 'required'
    ]; 
    protected $customMessages = [
        'referee_mobile.required' => 'Required', 
        'referee_mobile.required' => 'Required' 
    ]; 
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
    
    

}

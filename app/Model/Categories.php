<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Categories extends Model { 
  
    protected $table = 'categories';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['name','status'];  
    protected $rules = [ 
        'name' => 'required'  
    ]; 
    protected $customMessages = [
      'name.required' => 'Required'
    ]; 
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

}

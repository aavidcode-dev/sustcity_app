<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Theft extends Model {

    protected $table = 'theft_reports';
    protected $hidden = [];
    protected $fillable = ['h_code','location','description','latitude','longitude','timezone'];  
    public $timestamps = true;
    protected $customMessages = [
        'h_code.required' => 'Required',
        'location.required' => 'Required',
        'description' => 'Required'
    ];
    
//    protected $rules = [
//            'utility_name' => 'required|unique:utility,utility_name',
//            'utility_type_id' => 'required',
//            'premium' => 'required',
//            'cat_id' => 'required',
//        ];

    public static function rules($id = 0) {
        return [
            'h_code' => 'required',
            'location' => 'required',
            'description' => 'required'
        ];
    }

    public function getRules() {
        return $this->rules;
    }
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
}

?>
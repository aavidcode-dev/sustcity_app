<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class ElectricConsumption extends Model {
 
    protected $table = 'utility_electricity_consumption';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['rank','h_code','curr_reading','pre_reading','unit','ep_amount','ed_amount','mh_amount','fix_amount','other_amount','total_amt','sp_units','sp_cost','avg_cost_per_unit','month','year','created_at'];
    protected $rules = [ 
       
    ];
    protected $customMessages = [
     
    ];
    
     public static function rules($id = 0) {
        
        return [                      
            //'h_code' => 'required|unique:utility_electricity_consumption,h_code'.($id ? ",$id" : ''),
            'curr_reading'=>'required|numeric',  
            'pre_reading'=>'required|numeric',  
            'unit'=>'required|numeric',
            'ep_amount'=>'required|numeric',  
            'ed_amount'=>'required|numeric',  
            'mh_amount'=>'required|numeric',  
            'fix_amount'=>'required|numeric',  
            'total_amt'=>'required|numeric',  
            'avg_cost_per_unit'=>'required|numeric', 
            'rank'=>'required|numeric', 
            'month'=>'required',    
            'year'=>'required',                  
        ];
    }
    
     public function getRules() {
        return $this->rules;
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

    public function utility() {
        return $this->belongsTo('\App\Model\Utility', 'utility_id');
    }
    
     public function House_details() {
        return $this->belongsTo('\App\Model\UtilityHouseDetails', 'h_code','h_code');
    }
}

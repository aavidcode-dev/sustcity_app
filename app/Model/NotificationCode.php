<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NotificationCode extends Model {

    protected $table = 'notification_code';
    protected $hidden = [];
    protected $fillable = ['id','notification_code','image_url'];  
    public $timestamps = true;
    protected $customMessages = [
        'notification_code.required' => 'Required',
        'image_url.required' => 'Required'
    ];
    
    public static function rules($id = 0) {
        return [
            'notification_code' => 'required',
            'image_url' => 'required'
        ];
    }

    public function getRules() {
        return $this->rules;
    }
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

    public function top_credits_details() {
        return $this->belongsTo('\App\Model\Topcredits', 'top_credits_id');
    }
}

?>
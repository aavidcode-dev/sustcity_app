<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class City extends Model {
 
    protected $table = 'city';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['city'];  
    protected $rules = [ 
        'city' => 'required|unique:city,city'
    ]; 
    protected $customMessages = [
        'city.required' => 'Required'
    ]; 
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $table = 'notification';
    protected $hidden = [];
    protected $fillable = ['h_code','type','read_out','house_id'];  
    public $timestamps = true;
    protected $customMessages = [
        'h_code.required' => 'Required',
        'type.required' => 'Required',
        'read_out' => 'Required'
    ];
    
    public static function rules($id = 0) {
        return [
            'h_code' => 'required',
            'type' => 'required',
            'read_out' => 'required'
        ];
    }

    public function getRules() {
        return $this->rules;
    }
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

    public function top_credits_details() {
        return $this->belongsTo('\App\Model\Topcredits', 'top_credits_id');
    }
    
    public function notification_code() {
        return $this->belongsTo('\App\Model\NotificationCode', 'type');
    }
    
     public function house_id() {
        return $this->belongsTo('\App\Model\UtilityHouseDetails', 'house_id');
    }
    
    public function notification_for_app($player_id=false,$data=false)
    {
        $content['head'] = 'Sustcity';
      
        $notification_code_id =  $data['notification_code_id'];
        if($notification_code_id == '1'){
             $content['body'] = 'SR Credits';
        }else if($notification_code_id == '2'){
             $content['body'] = 'SR Meter';
        }else if($notification_code_id == '3'){
             $content['body'] = 'Refferals'; 
        }else if($notification_code_id == '4'){
             $content['body'] = 'Theft report has been verified';
        }else{
             $content['body'] = $data['notificationtext'];
        }
        $player_id = ($player_id) ? $player_id : '';
        $response = $this->sendMessage($content,$player_id,$data);
        $return["allresponses"] = $response;
        $return = json_encode( $return); 
        return $return;
    }
    
    public function sendMessage($content=false,$player_id=false,$data=false){
        $contented = array(
            "en" => $content['body']
            );
        
        $fields = array(
            'app_id' => "8e8b84d0-8bf2-4918-bb4e-75322c13bd19",
            'include_player_ids' => $player_id,
            'data' => $data,
            'headings' => array("en"=> $content['head'] ),
            'contents' => $contented
        );
        
        $fields = json_encode($fields);
        
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    }
    
    
    
}

?>
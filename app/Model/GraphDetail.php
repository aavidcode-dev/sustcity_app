<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class GraphDetail extends Model {
 
    protected $table = 'utility_graph_details';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['cat_id','utility_type_id','month','year','dmy','count','median','top'];
   
     protected $customMessages = [
      'cat_id.required' => 'Required',     
      'utility_type_id.required' => 'Required',
      'dmy.required' => 'Required',
      'count.required'=>'Required',    
      'median.required' => 'Required',
      'top.required' => 'Required'
    ];
    
     public static function rules($id = 0) {
        return [            
            'cat_id'=>'required',  
            'utility_type_id'=>'required',  
            'dmy'=>'required',             
            'count'=>'required',
            'median'=>'required',
            'top'=>'required'
        ];
    }
    
     public function getRules() {         
        return $this->rules;
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
    
      public function cat_details() {
        return $this->belongsTo('\App\Model\Categories', 'cat_id');
    }
    
    public function utility_details() {   
        return $this->belongsTo('\App\Model\Utility', 'utility_type_id');
    }
}

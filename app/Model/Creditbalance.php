<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Creditbalance extends Model {
 
    protected $table = 'credit_balance_transactions'; 
    public $timestamps = true; 
    protected $hidden = []; 
    protected $fillable = ['h_code','type','value','spending_type'];
    protected $rules = [ 
       
    ];
    protected $customMessages = [
     
    ];
    
     public static function rules($id = 0) {
        
        return [                      
            'h_code'=>'required',  
            'type'=>'required',  
            'value'=>'required',
            'spending_type'=>'required'                
        ];
    }
    
     public function getRules() {
        return $this->rules;
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    } 
}

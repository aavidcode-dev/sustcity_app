<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Subscriptions extends Model {
 
    protected $table = 'subscriptions'; 
    public $timestamps = true; 
    protected $hidden = [];
    protected $fillable = ['name','amount','period', 'description'];
    protected $rules = [ 
     
    ];
        
    protected $customMessages = [
      'name.required' => 'Required',     
      'amount.required' => 'Required',
      'period.required' => 'Required',
      'description.required'=>'Required'      
    ];
    
     public static function rules($id = 0) {
        return [            
            'name'=>'required',  
            'amount'=>'required',  
            'period'=>'required',             
            'description'=>'required',  
        ];
    }
    
     public function getRules() {         
        return $this->rules;
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
    
   
}

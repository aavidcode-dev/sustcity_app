<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Aboutus extends Model {
 
    protected $table = 'about_us';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['logo','description','logo_url'];  
    protected $rules = [ 
        'logo' => 'required',
        'description' => 'required'
      
    ]; 
    protected $customMessages = [
        'logo.required' => 'Required', 
        'description.required' => 'Required'
        
    ]; 
    
    public static function rules($id = 0) {
        return [                
            'logo' => ($id ? "" : 'required'),           
            'description' => 'required'
            
        ];
    }
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

}

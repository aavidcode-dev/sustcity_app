<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * Class CategoryAttribute
 */
class Contactus extends Model {
 
    protected $table = 'contact_us';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['h_code','message'];
    protected $rules = [ 
       
    ];
    protected $customMessages = [
     
    ];
    
     public static function rules($id = 0) {
        
        return [                                
            'h_code'=>'required',    
            'message'=>'required',                  
        ];
    }
    
     public function getRules() {
        return $this->rules;
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
}

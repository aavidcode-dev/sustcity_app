<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Subscribeuser extends Model {
 
    protected $table = 'subscribed_users'; 
    public $timestamps = true; 
    protected $hidden = [];
    protected $fillable = ['subscription_id','h_code'];
    protected $rules = [ 
     
    ];
        
    protected $customMessages = [
      'subscription_id.required' => 'Required',     
      'h_code.required' => 'Required'    
    ];
    
     public static function rules($id = 0) {
        return [            
            'subscription_id'=>'required',  
            'h_code'=>'required'
        ];
    }
    
     public function getRules() {         
        return $this->rules;
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
    
   
}

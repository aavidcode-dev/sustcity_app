<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Topcredits extends Model {

    protected $table = 'top_credits';
    protected $hidden = [];
    protected $fillable = ['h_code','credit','is_claimed','is_sent','dmy'];  
    public $timestamps = true;
    protected $customMessages = [
        'h_code.required' => 'Required',
        'credit.required' => 'Required'
       
    ];
   

    public static function rules($id = 0) {
        return [
            'h_code' => 'required',
            'credit' => 'required'
          
        ];
    }

    public function getRules() {
        return $this->rules;
    }
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
    
    public function notifications()
    {
        return $this->hasMany('\App\Models\Notification' ,'top_credits_id','created_at','updated_at');
    }
    
    public function topcreditid() {
        return $this->belongsTo('App\Model\Notification','top_credits_id');
    }   
    
    
    
    
}

?>
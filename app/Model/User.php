<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class User extends Model {

    protected $table = 'users';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['name', 'email', 'password', 'mobile', 'verify_code', 'h_code'];
    
    protected $customMessages = [
        
    ];

    public function getCustomMessages() {
        return $this->customMessages;
    }
    
     public function user_details() {
        return $this->belongsTo('\App\Model\UtilityHouseDetails', 'mobile');
    }

}

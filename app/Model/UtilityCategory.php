<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class UtilityCategory extends Model {
 
    protected $table = 'utility_category';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['utility_id','cat_id'];  
    
   
}

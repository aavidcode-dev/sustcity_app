<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class UtilityType extends Model {
 
    protected $table = 'utility_types';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['name','status'];
    protected $rules = [ 
        'name' => 'required|unique:utility_types'  
    ];
    protected $customMessages = [
      'name.required' => 'Required'
    ]; 
    
     public function getRules() {
        return $this->rules;
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

}

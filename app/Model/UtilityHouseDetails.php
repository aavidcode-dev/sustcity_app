<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class UtilityHouseDetails extends Model {
 
    protected $table = 'utility_house_details';
    public $timestamps = true; 
    protected $hidden = [];
    protected $fillable = ['h_code','partner','mobile', 'name', 'address','cat_id','utility_id','city','created_at']; 
        
    protected $customMessages = [
      'h_code.required' => 'Required',     
      'name.required' => 'Required',
      'address.required' => 'Required',
      'partner.required'=>'Required'   
      
    ];
    
     public static function rules($id = 0) {
        return [
            'h_code' => 'required|unique:utility_house_details,h_code'.($id ? ",$id" : ''), 
            'name'=>'required',  
            'address'=>'required',  
            'partner'=>'required',             
            'cat_id'=>'required|numeric'  
        ];
    }
    
     public function getRules() {         
        return $this->rules;
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }
    
     public function utility() {
        return $this->belongsTo('\App\Model\Utility', 'utility_id');
    }
    
     public function electric_consumption() {
        return $this->hasMany('\App\Model\ElectricConsumption', 'h_code','h_code');
    }
    
     public function categories() {
        return $this->belongsTo('\App\Model\Categories', 'cat_id');
    }
    
    public function user() {  
        return $this->belongsTo('\App\Model\User', 'user_id');
    }
}

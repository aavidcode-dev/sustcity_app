<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Watercomsumption extends Model {

    protected $table = 'utility_water_consp';
    protected $hidden = [];
    public $timestamps = true; 
     protected $fillable = ['bp','rank', 'h_code','utility_id','cat_id', 'january', 'february', 'march','april','may','june','july','august','september','october','november','december','utility_year'];
    protected $rules = [  
      
    ];
    protected $customMessages = [  
          'utility_name.required' => 'Required',
        'premium.required' => 'Required',
        'utility_type_id' => 'Required',
        'rank' => 'Required'
    ];
    
     public static function rules($id = 0) {        
        return [                      
            'h_code' => 'required|unique:utility_water_consp,h_code'.($id ? ",$id" : ''),
            'bp'=>'required',
            'utility_id'=>'required',
            'cat_id'=>'required',
            'january'=>'required|numeric',
            'february'=>'required|numeric',
            'march'=>'required|numeric',
            'april'=>'required|numeric',
            'may'=>'required|numeric',
            'june'=>'required|numeric',
            'july'=>'required|numeric',
            'august'=>'required|numeric',
            'september'=>'required|numeric',
            'october'=>'required|numeric',
            'november'=>'required|numeric',
            'december'=>'required|numeric',
            'utility_year'=>'required',
             'rank'=>'required'
        ];
    }
    
    public function getRules() 
    {
        return $this->rules;
    }
    
    public function getCustomMessages() { 
        return $this->customMessages;
    }     
        public function utility() {
        return $this->belongsTo('\App\Model\Utility', 'utility_id');
    }
}

?>
<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Utility extends Model {

    protected $table = 'utility';
    protected $hidden = [];
    protected $fillable = ['utility_name','premium','utility_type_id','image_url','example','identifier','status'];  
    public $timestamps = true;
    protected $customMessages = [
        'utility_name.required' => 'Required',
        'premium.required' => 'Required',
        'utility_type_id' => 'Required',
        'identifier' => 'Required',
        'example' => 'Required'
        
    ];
    
    public function u_category() {
        return $this->hasMany('\App\Model\UtilityCategory', 'utility_id');
    }

    public static function rules($id = 0) {
        return [
            'utility_name' => 'required|unique:utility,utility_name' . ($id ? ",$id" : ''),
            'utility_type_id' => 'required',
            'premium' => 'required',
            'identifier' => 'required',
            'example' => 'required'            
        ];
    }

    public function getRules() {
        return $this->rules;
    }
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

    public function utility_type() {
        return $this->belongsTo('\App\Model\UtilityType', 'utility_type_id');
    }

    public function utilityhousedetails()
    {
        return $this->hasOne('\App\Model\UtilityHouseDetails', 'utility_id');
    }

}

?>
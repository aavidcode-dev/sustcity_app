<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class ElectricDetails extends Model {
 
    protected $table = 'utility_electric_details';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['h_code','partner','unit','ep','ed','mh','fix','total','year','month'];  
    protected $rules = [ 
        'h_code' => 'required',
        'partner' => 'required'
    ]; 
    protected $customMessages = [
        'h_code.required' => 'Required', 
        'partner.required' => 'Required' 
    ]; 
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

}

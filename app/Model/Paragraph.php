<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryAttribute
 */
class Paragraph extends Model {
 
    protected $table = 'paragraphs';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['name','description'];  
    protected $rules = [ 
        'name' => 'required',
        'description' => 'required'
    ]; 
    protected $customMessages = [
        'city.required' => 'Required',
        'description.required' => 'Required'
    ]; 
    
     public function getRules() {
        return $this->rules; 
    } 
    
    public function getCustomMessages() {
        return $this->customMessages;
    }

}

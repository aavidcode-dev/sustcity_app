<?php

namespace App\Http\Controllers;
 
use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\ElectricConsumption;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Model\UtilityType;
use App\Model\Categories;
use App\Model\Utility; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
         $count = [
            'housedetails' => \App\Model\UtilityHouseDetails::count(),
            'utility' => \App\Model\Utility::count(),
            'user' => \App\Model\User::count()
        ];
        return view('welcome', compact('count'));
    }
    
     public function changePassword() {
        return view('master.change_password');
    }

    public function updatePassword(Request $request) {
        $post = $request->all();
        $user = \Auth::user();

        $rules = array(
            'password' => 'required',
            'c_password' => 'required'
        );
        $validator = \Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {            
            $password = bcrypt($post['password']);
            \App\Model\User::find($user->id);
            $user->password = $password; 
            $user->update();

            Session::flash('message', 'Password has been changed successfully');
            return redirect('change-password');
        }
    }
    
    public function forgotPassword(Request $request) {
        $post = $request->all();
        
        $email = $post['email'];
        $user = User::where('email', $email)->first();

        if ($user != null) {
            $password = str_random(6);
            $customer = User::find($user->id);
            $customer->password = bcrypt($password);
            $customer->update();

            if ($customer) {

                $userData = [
                    'email' => $post['email'],
                    'password' => $password
                ];

                \Mail::send('master.emails.forgotpassword', ['userData' => $userData], function($message) use ($user) {
                    $message->to($user->email)->subject("Sustcity | Forgot Password");
                });
 
                 Session::flash('message', 'Password has been changed successfully');
                 return redirect('login'); 
            }
        } else {
            Session::flash('message', 'Invalid Email Provided');
            return redirect('login'); 
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\AuthenticateUser;
use Hash;
use Auth;
use App\Model\User;
use App\Model\ElectricConsumption;
use App\Model\Utility;
use App\Model\Watercomsumption;
use App\Model\UtilityHouseDetails;
use Illuminate\Http\Response;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
     * @return void
     */
    public function __construct() {
        //$this->auth = $auth;
        //$this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }
    
    
    public function postLogin(Request $request) 
    {
        $post = $request->all();

        $validator = \Validator::make($post, ['mobile' => 'required', 'password' => 'required']);

        if ($validator->fails()) {
            return response()->json([
                        'success' => false,
                        'error' => $validator->messages()
                            ], 200); 
        }

        $mobile = $post['mobile'];
        $password = $post['password'];

        $user = User::where('mobile', $mobile)->first();
         

        if (!empty($user) && count($user) > 0) {
            if (Hash::check($password, $user->password)) {
                if ($user->verify_status == '1') {
                    $housedetails = UtilityHouseDetails::where('mobile', $user->mobile)->first();
                    
                    $user->player_id = $post['playerId'];                
                    $user->save();
                                
                    if (!empty($housedetails)) {
                        return response()->json([
                            'status' => 200,
                            'data' => [
                                    'success' => true,
                                    'error' => '',
                                    'verify_mobile' => true,
                                    'verify_h_code' => true,
                                    'status' => 'Dashboard Screen',
                                    'utility_details' => $housedetails->utility,
                                    'house_details' =>  $housedetails,
                                    'email' =>  $user->email 
                                ]
                        ]);
                    } else {
                        return response()->json([
                            'status' => 400,
                            'data' => [
                                    'success' => false,                                    
                                    'verify_status' => true,
                                    'utility_status' => false,
                                    'error' => 'Kindly complete registration. House Details not entered.'
                                ]
                        ]);
                    }
                } else {
                    return response()->json([
                            'status' => 400,
                            'data' => [
                                'success' => false,
                                'error' => 'Kindly complete registration. OTP Verification pending.',
                                'verify_status' => false                                                                
                                ]
                    ]);
                }
            } else {
                return response()->json([
                     'status' => 400,
                     'data' => [
                            'success' => false,
                            'error' => 'Password did not match. Please try again.',
                            'valid_password' => false                           
                         ]
                ]);
            }
        } else {
            return response()->json([ 
                     'status' => 400,
                     'data' => [
                        'success' => false,
                        'error' => 'Mobile number did not match. Please use the number entered during registration.',
                        'valid_mobile' => false,
                  ]
            ]);
        }
    }

    public function postRegister(Request $request) 
    {
        $post = $request->all();
        $mobile = $post['mobile'];        
        if($post['ref_code']){            
             $ref_details = UtilityHouseDetails::where('ref_code', $post['ref_code'])->first();
        }else{           
             $ref_details = '';
        }
        
        $user_details = User::where('mobile', $post['mobile'])->where('full_status', '0')->first();
        
        $valid_user = User::where('mobile', $post['mobile'])->first();
        
        $r_code = $post['ref_code'];
        
        if(!empty($ref_details))
        {
            if(!empty($valid_user))
            {    
                if(!empty($user_details))
                {            
                    $verify_status = $user_details->verify_status;
                    $utility_status = $user_details->utility_status;

                    if($verify_status == '0')
                    {
                        $rand = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                        $user_details->verify_code = $rand;
                        $user_details->password = bcrypt($post['password']);
                        $user_details->email = $post['email'];
                        $user_details->save();

                        $message = "Your mobile verification code is " .$rand;
                        $sms = new \App\Helpers\Sms(); 
                        $resp = $sms->send($mobile, $message); 

                        return response()->json([
                               'status' => 200,
                               'data' => [
                                   'success' => true,
                                   'verify_code' => $rand,
                                   'verify_status' => '0',                           
                                   'utility_status' => '0',
                                   'full_status' => '0',
                                   'error' => ''
                                   ]
                        ]); 
                    }else{
                        if($utility_status == '0')
                        {
                            
                           return response()->json([
                                'status' => 200,
                                'data' => [
                                    'success' => true,
                                    'verify_status' => '1',                            
                                    'utility_status' => '0',
                                    'full_status' => '0',
                                    'error' => ''
                                    ]
                         ]);  
                        }else{
                            return response()->json([
                                'status' => 400,
                                'data' => [
                                    'success' => false,
                                    'verify_status' => '1',
                                    'utility_status' => '1',
                                    'full_status' => '0',                           
                                    'error' => ''
                                    ]
                         ]); 
                        }
                          return response()->json([
                                'status' => 400,
                                'data' => [
                                    'success' => false,
                                    'verify_status' => '1',
                                    'full_status' => '0',
                                    'utility_status' => '0',
                                    'error' => ''
                                    ]
                         ]);                  
                    }                   
                }else{
                    return response()->json([
                                'status' => 404,
                                'data' => [
                                    'success' => false,
                                    'verify_status' => '1',
                                    'full_status' => '1',
                                    'utility_status' => '1',
                                    'error' => 'Dashboard'
                                    ]
                         ]);                        
                   }
                }else{            
                    $rand = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                    $new_user = new User();
                    $new_user->mobile = $post['mobile'];
                    $new_user->verify_code = $rand;
                    $new_user->password = bcrypt($post['password']);
                    $new_user->email = $post['email'];
                    $new_user->save();

                    $message = "Your mobile verification code is " .$rand;
                    $sms = new \App\Helpers\Sms(); 
                    $resp = $sms->send($mobile, $message);


                    return response()->json([
                           'status' => 200,
                           'data' => [
                               'success' => true,
                               'verify_code' => $rand,
                               'verify_status' => '0',                           
                               'utility_status' => '0',
                               'full_status' => '0',
                               'error' => ''
                               ]
                    ]);  
                }
            
        }
        
        else{
            
        if($r_code == '' && empty($r_code))          
        {
        if(!empty($valid_user))
        {    
        if(!empty($user_details))
        {            
            $verify_status = $user_details->verify_status;
            $utility_status = $user_details->utility_status;
            
            if($verify_status == '0')
            {
                $rand = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                $user_details->verify_code = $rand;
                $user_details->password = bcrypt($post['password']);
                $user_details->email = $post['email'];
                $user_details->save();
                                
                $message = "Your mobile verification code is " .$rand;
                $sms = new \App\Helpers\Sms(); 
                $resp = $sms->send($mobile, $message); 
               
                return response()->json([
                       'status' => 200,
                       'data' => [
                           'success' => true,
                           'verify_code' => $rand,
                           'verify_status' => '0',                           
                           'utility_status' => '0',
                           'full_status' => '0',
                           'error' => ''
                           ]
                ]); 
            }else{
                if($utility_status == '0')
                {                   
                   $valid_user->password = bcrypt($post['password']);
                   $valid_user->email = $post['email'];
                   $valid_user->save();
                                        
                   return response()->json([
                        'status' => 200,
                        'data' => [
                            'success' => true,
                            'verify_status' => '1',                            
                            'utility_status' => '0',
                            'full_status' => '0',
                            'error' => ''
                            ]
                 ]);  
                }else{
                    return response()->json([
                        'status' => 400,
                        'data' => [
                            'success' => false,
                            'verify_status' => '1',
                            'utility_status' => '1',
                            'full_status' => '0',                           
                            'error' => ''
                            ]
                 ]); 
                }
                  return response()->json([
                        'status' => 400,
                        'data' => [
                            'success' => false,
                            'verify_status' => '1',
                            'full_status' => '0',
                            'utility_status' => '0',
                            'error' => ''
                            ]
                 ]);                 
            }                   
        }else{
            
            return response()->json([
                        'status' => 404,
                        'data' => [
                            'success' => false,
                            'verify_status' => '1',
                            'full_status' => '1',
                            'utility_status' => '1',
                            'error' => 'Dashboard'
                            ]
                 ]);                        
           }
        }else{            
                $rand = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                $new_user = new User();
                $new_user->mobile = $post['mobile'];
                $new_user->verify_code = $rand;
                $new_user->email = $post['email'];
                $new_user->password = bcrypt($post['password']);
                $new_user->save();
                                
                $message = "Your mobile verification code is " .$rand;
                $sms = new \App\Helpers\Sms(); 
                $resp = $sms->send($mobile, $message);
            
            
                return response()->json([
                       'status' => 200,
                       'data' => [
                           'success' => true,
                           'verify_code' => $rand,
                           'verify_status' => '0',                           
                           'utility_status' => '0',
                           'full_status' => '0',
                           'error' => ''
                           ]
                ]);  
            } 
              }else{
                return response()->json([
                                'status' => 400,
                                'data' => [
                                    'success' => false,
                                    'error' => 'Please enter correct refer code'
                                ]
                        ]); 
            }
        }       
      }
    
    public function resendVerificationCode($mobile, Request $request)
    {       
        $rand = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
        
        $post['verify_code'] = $rand;       
           
        $profile = User::where('mobile',$mobile)->first();
        
        if(!empty($profile) && count($profile) > 0)
        {
            $profile->fill($post);
            $profile->update();
            $message = "Your mobile verification code is " .$rand;
           
            $sms = new \App\Helpers\Sms(); 
            $resp = $sms->send($mobile, $message); 

            if($profile){
                return response()->json([
                                'status' => 200,
                                'data' => [
                                    'success' => true,
                                    'verify_code' => $rand,
                                    'error' => false
                    ]]);
            }
        }else{
                 return response()->json([
                            'status' => 400,
                            'data' => [
                                'success' => false,
                                'verify_code' => '',
                                'error' => 'cannot find any this record'
                ]]);
        }               
      
     
    }
    public function verify(Request $request) {
        $post = $request->all();
        $validator = \Validator::make($post, array(
                    'mobile' => 'required',
                    'verify_code' => 'required'
        ));

        if ($validator->fails()) {
            return response()->json([
                        'status' => 400,
                        'data' => [
                        'success' => false,
                        'error' => $validator->messages()
                        ]
            ]);
        }

        $mobile = $post['mobile'];
        $verify_code = $post['verify_code'];

        $user = User::where('mobile', $mobile)->first();

        if ($user && $user->verify_code == $verify_code) {
            $m_code = $user->mobile;
            $user->verify_status = true;
            $user->save();

            return response()->json([
                        'status'=> 200,
                        'data' => [
                            'success' => true,
                            'error' => '',
                            'verify_status' => true
                       ]
            ]);
        } else {
            return response()->json([
                        'status' => 400,
                        'data' => [
                            'success' => false,
                            'verify_status' => false,
                            'error' => 'Invalid verification entered'
                        ]
            ]);
        }
    }  
}

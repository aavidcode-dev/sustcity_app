<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller; 

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\AuthenticateUser;
use Auth;
use App\Model\User;
use App\Model\Aboutus;
use App\Model\Faq;
use App\Model\Contactus;
use App\Model\ElectricDetails;
use App\Model\UtilityHouseDetails;
use App\Model\UtilityType;
use App\Model\Utility;
use App\Model\ElectricConsumption;
use App\Model\Subscriptions;
use App\Model\Subscribeuser;
use App\Model\Creditbalance;
use App\Model\Advertisement;
use App\Model\Theft;
use App\Model\Rewards;
use App\Model\Referrals; 
use App\Model\Credit_balance; 
use App\Model\Notification; 
use App\Model\Topcredits; 
use App\Model\Knowmore; 
use App\Model\NotificationCode; 

class ApiController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
     * @return void
     */
    public function __construct(ElectricDetails $model) {
        $this->middleware('guest', ['except' => 'getLogout']);
    }
    
     public function send_email($email)
     { 
         //$message = "$email has sent enquiry.";
         $to_email = "suresh@aavidcode.com";
          \Mail::send('emails.sample', compact('email'), function($message) use ($to_email) {
                    $message->to($to_email)->subject("Waabisaabi Enquiry");
                });        
     }
     
     public function forget_password(Request $request)
     { 
        $post = $request->all();
        $email = $post['email'];
        $user = User::where('email', $email)->first();

        if ($user != null) {
            $password = str_random(6);
            $customer = User::find($user->id);
            $customer->password = bcrypt($password);
            $customer->update();

            if ($customer) {

                $userData = [
                    'email' => $post['email'],
                    'password' => $password
                ];

                \Mail::send('emails.forgotpassword', ['userData' => $userData], function($message) use ($user) {
                    $message->to($user->email)->subject("Sustcity | Forgot Password");
                });

                return response()->json(['success' => true, 'message' => 'Password has been sent to your email']);
            }
        } else {
            return response()->json(['success' => false, 'message' => 'Invalid email provided']);
        }     
     }
     
    public function getUtilities() {
        $utilities = Utility::all();
        return response()->json([
                    'success' => true,
                    'error' => false, 
                    'data' => $utilities 
        ]);
    }

    public function getSubscriptions() {
        $subscriptions = Subscriptions::all();
        return response()->json([
                    'status' => 200,
                    'data' => [
                        'success' => true,
                        'error' => '',
                        'data' => $subscriptions     
                    ]
        ]);
    }

    public function subscribUser(Request $request) {
        $post = $request->all();

        $user = new Subscribeuser;
        $user->fill($post);
        $user->save();

        $user_mobile = $post['mobile'];
        $message = "Thank you for subscriptions";

        $sms = new \App\Helpers\Sms();
        $resp = $sms->send($user_mobile, $message);

        return response()->json([
                    'status' => 200,
                    'data' => [
                        'success' => true,
                        'error' => '',
                        'data' => 'successfully insert'
                    ]
        ]);
    }
   
    public function sendsms($hcode)
    {
        $housedetails = UtilityHouseDetails::where('h_code', $hcode)->first();
                
        if($housedetails){
            $mobile = $housedetails->mobile;
            $message = "Thank you for expressing interest. We will contact you soon,";
            $sms = new \App\Helpers\Sms(); 
            $resp = $sms->send($mobile, $message); 

            $post['h_code'] = $housedetails->h_code;
            $post['mobile'] = $housedetails->mobile;

            $user = new Knowmore;
            $user->fill($post);
            $user->save();

            return response()->json([
                   'status' => 200,
                   'data' => [
                       'success' => true,                                   
                       'error' => ''
                       ]
            ]); 
        }        
    }
    
    
    public function verifyDashboard(Request $request) 
    {  
        $post = $request->all();
        $player_id = !empty($post['playerId']) ? $post['playerId']: null;
        $housedetails = UtilityHouseDetails::where('h_code', $post['h_code'])->where('utility_id', $post['utility_id'])->first();       
        
        $user = User::where('mobile', $post['mobile'])->first();
       
        $house_data = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first();
        
        $mobile_verification = UtilityHouseDetails::where('mobile', $post['mobile'])->first();
        
        $ref_details = UtilityHouseDetails::where('ref_code', $post['ref_code'])->first();
         
        $credit_balance_transactions = Credit_balance::where('h_code', $post['h_code'])->first();
        
        $r_code = $post['ref_code'];
        
        $mobile_c = UtilityHouseDetails::where('h_code', $post['h_code'])->first();        
        
        if(@$mobile_c->mobile != NULL)
        {
              return response()->json([
                            'status' => 400,
                            'data' => [
                                'success' => true,
                                'error' => 'enter correct  h code'                                                      
                             ]
                ]);
        }
        else{
        if($ref_details)  
        { 
            if(empty($house_data))  
            {
            if ($housedetails && count($user) > 0) 
            {
                $housedetails->mobile = $post['mobile']; 
                $housedetails->user_id = $user->id; 
                $housedetails->ref_code = substr(number_format(time() * rand(), 0, '', ''), 0, 5);                
                $housedetails->save();
                
                $post1['referee_mobile'] = $ref_details->mobile;
                $post1['referred_mobile'] = $post['mobile'];
                $post1['ref_code'] = $ref_details->ref_code;

                $refer = new Referrals;
                $refer->fill($post1); 
                $refer->save(); 
                
                $rewards = Rewards::where('code', 'C3')->first();
                if(!empty($rewards) && count($rewards) > 0)
                {
                    $rewards_value = $rewards->value;
                }
                
                $ref_details->total_credits = $ref_details->total_credits > 1 ? $ref_details->total_credits + @$rewards_value : @$rewards_value;   
                $ref_details->save(); 
                
                $user1 = User::where('mobile', $post['mobile'])->first();              
                $user1->utility_status = 1;
                $user1->full_status = 1;
                $user1->player_id = $player_id;
                $user1->name = $mobile_c->name;                
                $user1->save();         
                
                $h_coddee = $ref_details->h_code;
                
                $notifications = new Notification;
                $notifications->h_code = $ref_details->h_code;
                $notifications->house_id = $housedetails->id;
                $notifications->type = 3;
                $notifications->dmy = date('Y-m-d');
                $notifications->save();
                
                
                
                $to_notify_user =  UtilityHouseDetails::with('user')->where('h_code', $h_coddee)->get();
                $last_record = Notification::where('h_code', $h_coddee)->orderBy('id', 'desc')->first();               
                $topcredits_ids = $last_record->type; 

                $get_notification_detail = NotificationCode::where('id', $topcredits_ids)->first(); 
                $get_notification_code = $get_notification_detail['notification_code'];
                $get_notification_id = $get_notification_detail['id'];

                $player_id = array($to_notify_user[0]['user']->player_id);

                //$notify_data['credit'] = $get_credit_notify;
                $notify_data['notification_code'] = $get_notification_code;
                $notify_data['id'] = $get_notification_id;
                $notify_data['notification_code_id'] = $get_notification_id;
                
                $ins_notification = new Notification();
                $notify_return = $ins_notification->notification_for_app($player_id,$notify_data);
                
                $credit_data = new Credit_balance(); 
                $credit_data->h_code = $ref_details->h_code;
                $credit_data->type = 'Credit'; 
                $credit_data->value = @$rewards_value;
                $credit_data->spending_type = 'C3';
                $credit_data->save();              
                                
                return response()->json([
                            'status' => 200,
                            'data' => [
                                'success' => true,
                                'error' => false,
                                'utility_details' => $housedetails->utility,
                                'house_details' => $housedetails,
                                'email' => $user->email
                            ]
                ]);
            } else { 
                return response()->json([
                            'status' => 400,
                            'data' => [
                                'success' => false,
                                'error' => 'Invalid House code entered'
                            ]
                ]);
            }
            }else {
                return response()->json([
                            'status' => 400,
                            'data' => [
                                'success' => false,
                                'error' => 'Invalid House code entered'
                            ] 
                ]);
            }
        }else{
            if(empty($mobile_verification))
            {            
            if(empty($house_data))
            {
              if($r_code == '' && empty($r_code))          
              {                                    
                if ($housedetails && count($user) > 0) {
                $housedetails->mobile = $post['mobile'];
                $housedetails->user_id = $user->id;
                $housedetails->ref_code = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                $housedetails->save();
                
                $user1 = User::where('mobile', $post['mobile'])->first();
              
                $user1->utility_status = 1;
                $user1->full_status = 1;
                $user1->player_id = $player_id; 
                $user1->name = $mobile_c->name;
                $user1->save(); 
                
                return response()->json([
                            'status' => 200,
                            'data' => [
                                'success' => true,
                                'error' => false,
                                'utility_details' => $housedetails->utility,
                                'house_details' => $housedetails,
                                'email' => $user->email
                            ]
                ]);
                } else { 
                return response()->json([
                            'status' => 400,
                            'data' => [
                                'success' => false,
                                'error' => 'Invalid House code entered'
                            ]
                ]);
            }
               }else{
                   return response()->json([
                            'status' => 400,
                            'data' => [
                                'success' => false,
                                'error' => 'Please enter correct ref code'
                            ]
                ]);                                                                        
              }
            
            }else {
                return response()->json([
                            'status' => 400,
                            'data' => [
                                'success' => false,
                                'error' => 'Invalid House code entered'
                            ]
                ]);
            }
          }
          else{
               return response()->json([
                            'status' => 400,
                            'data' => [
                                'success' => false,
                                'error' => 'Invalid House code entered'
                            ]
                ]);              
          }
        }
       }
    }

    public function energyConsumedashboard($h_code, Request $request) {
        $post = $request->all();

        $electricConsumption = ElectricConsumption::where('h_code', $h_code)->orderBy('dmy','DESC')->first();
        
        $houseData = UtilityHouseDetails::where('h_code', $h_code)->first();
        $utlity_count = $houseData->utility_id;
        
        $house_count = UtilityHouseDetails::where('utility_id', $utlity_count)->count();
        
        if (!empty($electricConsumption)) {
            return response()->json([
                        'status' => 200,
                        'data' => [
                            'success' => true,
                            'ec_details' => $electricConsumption,
                            'house_count' => $house_count,
                            'error' => '' 
            ]]);
        } else {  
            return response()->json([
                        'status' => 400,
                        'data' => [
                            'success' => false,
                            'ec_details' => '',
                            'house_count' => '',
                            'error' => 'Electricconsume data not available'
            ]]);
        }
    }
    
    public function getGraphDetails($h_code, $limit=4) {
        $data = [];
        $ec_details = ElectricConsumption::where('h_code', $h_code)->orderBy('dmy', 'desc')->take($limit)->get();
        foreach ($ec_details as $ec) {
            $dt = \DateTime::createFromFormat('!m', $ec->month);
            $month = $dt->format('M');
            $dt = \DateTime::createFromFormat('Y', $ec->year);
            $year = $dt->format('y');
            $key = $month . '-' . $year;
            $data['labels'][] = $key;
            $data['you'][] = $ec->unit;

            $graph = \App\Model\GraphDetail::where('month', $ec->month)->where('year', $ec->year)->first();
            if ($graph) {
                $data['median'][] = $graph->median;
                $data['top'][] = $graph->top;
            }
        }
        
        $newArr = array();
        foreach($data as $key => $val) {
            $newArr[$key] = array_reverse($val);
        }

        return response()->json([
                    'status' => 200,
                    'data' => [
                        'success' => true,
                        'graph_details' => $newArr,
                        'error' => ''
        ]]);
    }

    public function getAccountdetails($h_code) {
        $utilityhouseDetails = UtilityHouseDetails::where('h_code', $h_code)->first();

        if ($utilityhouseDetails) {
            return response()->json([
                        'status' => 200,
                        'data' => [
                            'success' => true,
                            'account_details' => $utilityhouseDetails,
                            'error' => ''
            ]]);
        }
    }

    public function updateAccountdetails(Request $request) {
        $post = $request->all();
       
        $accountdetails = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first();
        if (!$accountdetails) {
            return response()->json([
                        'status' => 400,
                        'data' => [
                            'success' => true,
                            'error' => 'House code not exists!'
            ]]);
        }

        $accountdetails->name = $post['name'];  
        $accountdetails->is_paper_claimed = $post['is_paper_claimed'];
        $accountdetails->address = $post['address'];
        $accountdetails->save();
        
        $user_name = User::where('mobile', $post['mobile'])->first();
        $user_name->name = $post['name'];
        $user_name->save();
        
        $is_claimed = $post['is_paper_claimed'];  
       
        
        $check_already_1 = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first();
        $i_p_c = $check_already_1->is_paper_claimed;
        
        if($is_claimed == 1){     
           
                $claimed_y = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first(); 
                $claimed_y->is_claimed_yes = $accountdetails->is_claimed_yes + 1;  
                $claimed_y->is_paper_claimed = '1';
                $claimed_y->save();
                      
        }else{
          
                $claimed_n = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first(); 
                $claimed_n->is_claimed_no = $accountdetails->is_claimed_no + 1;  
                $claimed_n->is_paper_claimed = '0';
                $claimed_n->save();            
         }
        
       if ($accountdetails) {
            return response()->json([
                        'status' => 200,
                        'data' => [
                            'success' => true,
                            'account_details' => $accountdetails,
                            'error' => ''
            ]]);
        }
    }
    
       
    
    public function updatepaperBill(Request $request) 
    {
        $post = $request->all(); 
        $h_code = $post['h_code']; 
        $mobile = $post['mobile']; 
        
        $h_data = UtilityHouseDetails::where('h_code', $h_code)->where('mobile', $mobile)->first();
        $h_id = $h_data->id;
        
        $rewards = Rewards::where('code', 'C1')->first();
        if(!empty($rewards) && count($rewards) > 0)
        {
            $rewards_value = $rewards->value;
        }
        
        $result = UtilityHouseDetails::where('id', $h_id)->first();
        $result->is_paper_bill = ($result->is_paper_bill > 0 ) ? '0' : '1' ;
        $result->total_credits = ($result->total_credits > 0) ? $result->total_credits + @$rewards_value : @$rewards_value;                
        $result->save(); 
               
        if ($result) {
            return response()->json([
                        'status' => 200,
                        'data' => [
                            'success' => true,
                            'error' => ''
            ]]);
        }
    }
    
    public function getNotification($h_code) {
        $notification = Notification::where('h_code', $h_code)->with('notification_code')->with('house_id')->where('read_out','1')->orderBy('id', 'desc')->get();         
        $notification_c = Notification::where('h_code', $h_code)->where('read_out','1')->get(); 
        
        if ($notification) {
            return response()->json([
                'status' => 200,
                'data' => [
                    'success' => true,                   
                    'notification' => $notification,                    
                    'notification_count' => count(@$notification_c),                   
                    'error' => ''
            ]]); 
        }
    }
    
    
    
    public function theftAmount() {
        $referrals = Rewards::where('code', 'C4')->first();         
        
        if ($referrals) {
            return response()->json([
                'status' => 200,
                'data' => [
                    'success' => true,                   
                    'theft_amount' => $referrals->value,                 
                    'error' => ''
            ]]); 
        }
    }
    
    
    
     public function getClaimdetails($h_code , $notification_id) {
        if((!empty($h_code) && $h_code!='') && (!empty($notification_id) && $notification_id!=''))
        { 
            $electricConsumption = ElectricConsumption::where('h_code', $h_code)->orderBy('dmy', 'desc')->select(['rank', 'top_credits', 'is_top_credit_claimed'])->first();         
            if($notification_id!='dashboard')
            {
                // for single notification
                $notification = Notification::with('top_credits_details')->where('h_code', $h_code)->where('id', $notification_id)->with('notification_code')->get();
            }
            else
            {
                // for all notification
                $notification = Notification::with('top_credits_details')->with('notification_code')->where('type','1')->where('read_out','1')->where('h_code', $h_code)->get();
            }
            
            //$credits = Topcredits::where('h_code', $h_code)->where('is_claimed','1')->orderBy('dmy','desc')->first();  

                //if($notification_id)  
                //{   
//                    $notification = Notification::where('id', $notification_id)->first(); 
//                    if($notification->read_out == 1) 
//                    {
//                      $notification->read_out = 0;
//                    }else{
//                        $notification->read_out = '';
//                    }
//                    $notification->save(); 
                //}
            if($electricConsumption) 
            {
                return response()->json([
                    'status' => 200,
                    'data' => [
                        'success' => true,
                        'claim_details' => $electricConsumption,                    
                        'credits' => $notification,
                        'error' => ''
                ]]); 
            }
            else
            {
                 return response()->json([
                    'status' => 400,
                    'data' => [
                        'success' => true,                    
                        'error' => true
                ]]); 
            }
        }
        else
        {
             return response()->json([
                'status' => 400,
                'data' => [
                    'success' => true,                    
                    'error' => true
            ]]); 
        }
    }
    
    public function paperBill($h_code,$mobile) {     
        $h_data = UtilityHouseDetails::where('h_code', $h_code)->where('mobile', $mobile)->first();
        $h_id = $h_data->id;
                
        $data = Topcredits::where('h_code', $h_code)->where('is_claimed','0')->get();
        
        $electricConsumption = ElectricConsumption::where('h_code', $h_code)->orderBy('dmy', 'desc')->first();
        
        if($electricConsumption->is_top_credit_claimed == 0){
            $is_claimed = 0;
        }else{
            $is_claimed = 1;
        } 
        $result = UtilityHouseDetails::where('id', $h_id)->first();
          
        if ($result) {
            return response()->json([
                        'status' => 200,
                        'data' => [
                            'success' => true,
                            'status' => $result->is_paper_bill, 
                            'claimed' => $is_claimed,
                            'error' => ''
            ]]);
        }
    }
    
    
    public function updateReferal(Request $request) {
        $post = $request->all();
        
        $h_code = $post['h_code'];
        $notification_id = $post['notification_id']; 
         
        $notification_read = Notification::where('id', $notification_id)->first();
        $notification_read->read_out = 0;
        $notification_read->save();
        
        $theft_amt = Rewards::where('code', 'C4')->first();
        
        if ($notification_read) {
            return response()->json([
                        'status' => 200,
                        'data' => [
                            'success' => true,       
                            'theft_amount' => $theft_amt->value,
                            'error' => ''
            ]]);
        }
    }
    
        
         
        
    public function updateClaimdetails(Request $request) {
        $post = $request->all();
        @$h_code = @$post['h_code'];
        @$notification_id = @$post['notification_id'];               
        
        if(count(@$notification_id)> 0 && !empty(@$notification_id) && @$notification_id!='')
        {                  
            $creditbalance = new Credit_balance;
            $creditbalance->h_code = $h_code;
            $creditbalance->type = 'Credit';
            $creditbalance->value = $post['value'];  
            $creditbalance->spending_type = 'C2';
            $creditbalance->save(); 

            $top_credits = Topcredits::where('h_code', $h_code)->where('is_claimed','1')->get();
           
            if(count($top_credits) == 1)
            { 
                $electricConsumption = ElectricConsumption::where('h_code', $h_code)->orderBy('dmy', 'desc')->first();
                $electricConsumption->is_top_credit_claimed = 0;
                $electricConsumption->save();
            }
            
            $topcredits1 = Notification::where('id', $notification_id)->first();
            $topcredits1->read_out = 0;
            $topcredits1->save();
            
            $topcredits = Notification::where('id', $notification_id)->first();
            $u_i = $topcredits->top_credits_id;

            $update_notification = Topcredits::where('id',$u_i)->first();
            $update_notification->is_claimed = 0;
            $update_notification->claim_date = date('Y-m-d');
            $update_notification->save(); 
            

            $topcredits = UtilityHouseDetails::where('h_code', $h_code)->first();
            $topcredits->total_credits = $topcredits->total_credits > 0 ? $topcredits->total_credits + $post['value'] : $post['value'];
            $topcredits->save();        
        }
        else
        {            
            $creditbalance = new Credit_balance;
            $creditbalance->h_code = $h_code;
            $creditbalance->type = 'Credit';
            $creditbalance->value = $post['value'];  
            $creditbalance->spending_type = 'C2';
            $creditbalance->save();

            $electricConsumption = ElectricConsumption::where('h_code', $h_code)->orderBy('dmy', 'desc')->first();
            $electricConsumption->is_top_credit_claimed = 0;
            $electricConsumption->save();

            Notification::where('h_code',$h_code)->update(['read_out' => 0]);

            Topcredits::where('h_code',$h_code)->where('is_claimed','1')->update(['is_claimed' => 0,'claim_date' => date('Y-m-d')]);
                                          
            $topcredits = UtilityHouseDetails::where('h_code', $h_code)->first();
            $topcredits->total_credits = $topcredits->total_credits > 0 ? $topcredits->total_credits + $post['value'] : $post['value'];
            $topcredits->save();
        }
        
        if ($creditbalance) {
            return response()->json([
                        'status' => 200,
                        'data' => [
                            'success' => true,
                            'error' => ''
            ]]);
        }
    }
    

    
    public function aboutUs() {    
         $about_us = Aboutus::all();
         if ($about_us) {
            return response()->json([
                        'status' => 200,
                        'data' => [
                            'success' => true,
                            'about_us' => $about_us,
                            'error' => ''
            ]]);
        }
    }
    
     public function Faq() {    
         $faq = Faq::all();
         if ($faq) {
            return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true,
                            'faq_details' => $faq,
                            'error' => ''
            ]]);
        }
    }
    
      public function Contactus(Request $request) {    
        $post = $request->all();        
         
        $contact_us = new Contactus; 
        $contact_us->fill($post);   
        $contact_us->save();
        
        $mobile = $post['mobile'];
      //  $message = "Thank you for contact us";
        $message = "Thank you for writing to us. We shall get back to you";

        $sms = new \App\Helpers\Sms(); 
        $resp = $sms->send($mobile, $message); 
                        
         if ($contact_us) {
            return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true,                           
                            'error' => ''
            ]]);
        }
    }
    
    public function Add($h_code,$mobile) {    
         $add = Advertisement::all();
         
         $h_data = UtilityHouseDetails::where('h_code', $h_code)->where('mobile', $mobile)->first();
         
         if ($add) {
            return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true,
                            'add_details' => $add,
                            'total_credits' => $h_data->total_credits,
                            'error' => ''
            ]]);
        }
    }
    
    public function spendMoney(Request $request)
    {
            $post = $request->all();    

            $h_data = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first();           
            $spend_money = $post['spend_amount'];

            @$h_data->total_credits = @$h_data->total_credits - $spend_money;
            $h_data->save(); 
            
            $creditbalance = new Credit_balance;
            $creditbalance->h_code = $post['h_code'];
            $creditbalance->type = 'Discount Claim'; 
            $creditbalance->value = $post['spend_amount'];  
            $creditbalance->spending_type = 'D1';
            $creditbalance->save();
            
            if($h_data)
            {
               return response()->json([
                        'status' => 200,
                        'data' => [  
                            'success' => true,
                            'message' => 'Your transaction successfully',                           
                            'error' => ''
                ]]);
            }        
    }              
    
    
     public function Updatetheftreports(Request $request) 
     {            
        $post = $request->all();
        $h_code = $post['h_code']; 
        $mobile = $post['mobile'];

        $theft = new Theft();
        $theft->h_code = $post['h_code'];
        $theft->location = $post['location'];
        $theft->description = $post['description'];
        $theft->latitude = $post['latitude'];
        $theft->longitude = $post['longitude'];
        $theft->timezone = $post['timezone'];
        $theft->save();

        if($theft){
           // $message = "Thank you for reporting a theft";
            $message = "Thank you for reporting a theft. You will be rewarded and notified once it is physically validated.";

            $sms = new \App\Helpers\Sms(); 
            $resp = $sms->send($mobile, $message); 
//                
//                \Mail::send('emails.admin_theft_report', compact('email'), function($message) use ($to_email) {
//                    $message->to($to_email)->subject("Theft Report");
//                });      

            return response()->json([
                    'status' => 200,
                    'data' => [ 
                        'success' => true,                          
                        'error' => ''
        ]]);
        }else{
              return response()->json([
                    'status' => 400,
                    'data' => [ 
                        'success' => false,                          
                        'error' => 'Cannot insert data please try again'
        ]]);
        } 
    }
    
    public function getReferalcode($h_code,$mobile) {       
         $housedetails = UtilityHouseDetails::select('ref_code')->where('h_code', $h_code)->where('mobile', $mobile)->first();
         $rewards = Rewards::where('code', 'C3')->first();
         if($housedetails){
                 return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true, 
                            'ref_code' => $housedetails,
                            'rewards_amt' => $rewards,
                            'error' => ''
            ]]);
            }else{
                 return response()->json([
                        'status' => 400,
                        'data' => [
                            'success' => false, 
                            'ref_code' => '',
                            'error' => ''
            ]]);
            }
    }
    
     public function updateClaimed_y(Request $request) {         
        $post = $request->all();      
        
        $housedetails = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first();         
        $id = $housedetails->id; 
        
        $claimed_y = UtilityHouseDetails::where('id', $id)->first(); 
        $claimed_y->is_claimed_yes = $housedetails->is_claimed_yes + 1;  
        $claimed_y->save();
        
        $housedetails_data = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first();
        if($housedetails_data){
                 return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true, 
                            'error' => ''
            ]]);
            }else{
                 return response()->json([
                        'status' => 400,
                        'data' => [
                            'success' => false, 
                            'ref_code' => '',
                            'error' => ''
            ]]);
            }         
     }
     
     public function updateClaimed_n(Request $request) { 
        $post = $request->all();
        $housedetails = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first();
         
        $id = $housedetails->id; 
        
        $post['is_claimer_no'] = $housedetails->is_claimed_no + 1;  

        $claimed_n = UtilityHouseDetails::where('id', $id)->first();
        $claimed_n->is_claimed_yes = $housedetails->is_claimed_yes + 1;
        $claimed_n->save();
         
         $housedetails_data = UtilityHouseDetails::where('h_code', $post['h_code'])->where('mobile', $post['mobile'])->first();
          if($housedetails_data){
                 return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true,                           
                            'error' => ''
            ]]);
            }else{
                 return response()->json([
                        'status' => 400,
                        'data' => [
                            'success' => false, 
                            'error' => ''
            ]]);
            }         
     } 
     
    public function Name($h_code,$mobile) {    
       $name = UtilityHouseDetails::where('h_code', $h_code)->where('mobile', $mobile)->first();
         
         if ($name) {
            return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true,
                            'name' => $name->name,
                            'is_paper_claimed' => $name->is_paper_claimed,
                            'error' => ''
            ]]);
        }
    }
     
    public function is_paper_claimed($h_code,$mobile) {    
       $is_paper_claimed = UtilityHouseDetails::where('h_code', $h_code)->where('mobile', $mobile)->first();
         
         if ($is_paper_claimed) {
            return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true,                           
                            'is_paper_claimed' => $is_paper_claimed->is_paper_claimed,
                            'is_paper_bill' => $is_paper_claimed->is_paper_bill,
                            'error' => ''
            ]]);
        }
    }
    
    public function discontinue_paper_bill_amount() { 
        $is_claim_amount = Rewards::where('code','C1')->first();
        
        if($is_claim_amount){
             return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true,                           
                            'amount' => $is_claim_amount->value,
                            'error' => ''
            ]]);
        }
        
    }
    
    public function nameUpdate($h_code,$name){ 
        $nameUpdate = UtilityHouseDetails::where('h_code', $h_code)->first();
        $nameUpdate->name = $name;
        $nameUpdate->save();
        
        if($nameUpdate){
             return response()->json([
                        'status' => 200,
                        'data' => [ 
                            'success' => true,                                                       
                            'error' => ''
            ]]);
        }        
    }
    
    
}

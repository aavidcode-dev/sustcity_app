<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel as Excel;                                              
use App\Model\Temperature;                     
use App\Model\City;  

class TemperatureController extends Controller { 

    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    private $model; 
 
    public function __construct(Temperature $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
    public function index() {    
        
        $limit = Input::get('limit');  
         $post = Input::all();
        $data = Temperature::query();
        if (isset($post['city']) && $post['city'] != "") {
            $data->where('city_id', 'like', '%'.$post['city'].'%');
        }    
        if (isset($post['year']) && $post['year'] != "") {
            $data->where('year', 'like', '%'.$post['year'].'%');
        }    
        $data = $data->orderBy('created_at', 'desc')->paginate($limit);       
        $city = City::all(); 
        $data->setPath('temperature'); 
        return view('master.temperature.index', compact('data','city','limit','post'));    
       
    } 
    
    public function create() {
        $data = Temperature::all();   
          $city = City::all();
        return view('master.temperature.create', compact('data','city'));  
    }
    
    public function store(Request $request) {
        $post = $request->all(); 
       
        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                                     ->withInput($request->input());
           
        } else {
             
            $temperature = Temperature::create($post);           
            
        if ($temperature) {
            Session::flash('message', 'Temperature has been Save Successfully');
            return redirect('temperature');
        }
        }
    }
    
    public function edit($id) {     
        $row = Temperature::find($id);         
        return view('master.temperature.edit', compact('row')); 
    }

    public function update(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                             ->withInput($request->input());
            
            
        } else { 
            $temperature = Temperature::find($post['t_id']); 
            $temperature->fill($post);
            $temperature->update();
        
            
            if ($temperature) {
                Session::flash('message', 'Temperature has been Update Successfully');
                return redirect('temperature');
            }
        }
    }
    
//     public function search() {                     
//        $post = Input::all();
//        $data = Temperature::query();
//        if (isset($post['city']) && $post['city'] != "") {
//            $data->where('city_id', 'like', '%'.$post['city'].'%');
//        }                        
//        $limit = ''; 
//        $data = $data->paginate($limit);
//        $city = City::all(); 
//        $data->setPath('temperature');   
//        return view('master.temperature.index', compact('data','city','limit', 'post')); 
//    }
    
     public function uploadcity(Request $request) {
         $post = $request->all();
        if (Input::hasFile('uploadexcel')) {
            
            $path = Input::file('uploadexcel')->getRealPath();
            $data = Excel::load($path, function($reader) {
                        
                    })->get();

            if (!empty($data) && $data->count()) {
                $results = $data->toArray();
                foreach ($results as $entry) {
                    $city_id = $post['city'];
                    if (isset($entry['month']) && isset($entry['year']) && isset($city_id)) { 
                       
                        $eleConsp = Temperature::where('city_id', $city_id)->where('month', $entry['month'])->where('year', $entry['year'])->first();
                        $data = [                         
                            'month' => $entry['month'],  
                            'year' => $entry['year'],
                            'max' => $entry['max'],                            
                            'min' => $entry['min'], 
                            'city_id' => $post['city']
                        ];
                        if ($eleConsp == null) {
                            $insert[] = $data;
                        } else {
                            $eleConsp->fill($data);
                            $eleConsp->save();
                        }
                    }
                }

                if (!empty($insert)) {
                    $result = Temperature::insert($insert);

                    if ($result) {
                        Session::flash('message', 'Excel Upload Successfully');
                        return redirect('temperature');
                    }
                } else {
                    Session::flash('unsuccess_message', 'Data is already uplaoded');
                    return redirect('temperature');
                }
            }
        }
        return back();
    }
    
    public function downloadXls() {
        $columns[] = ['month' => '', 'year' => '', 'max' => '', 'min' => ''];

        \Excel::create('Temperature', function($excel) use($columns) {
            $excel->sheet('Sheet1', function($sheet) use($columns) {
                $sheet->fromArray($columns);
            });
        })->export('xls');
    }
    
       public function upload() {        
        $city = City::all();          
        return view('master.temperature.upload', compact('city'));
    }
}

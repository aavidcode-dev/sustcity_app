<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\MassAssignmentException; 
use App\Model\Paragraph;   
use App\Model\Utility;   
use App\Model\Faq;
use Illuminate\Support\Facades\File;

class FaqController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(Faq $model) {
        $this->middleware('auth');
        $this->model = $model;
    }
    
     public function index() {     
        $data = Faq::orderBy('id','desc')->get();
        return view('master.faq.index',compact('data'));
    }
    
     public function create() {       
        return view('master.faq.create');
    }
    
     public function store(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, Faq::rules(), $this->model->getCustomMessages());
 
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                            ->withInput($request->input());
        } else {

            $faq = Faq::create($post);
            if ($faq) { 
                Session::flash('message', 'Faq has been Save Successfully');
                return redirect('faq');
            }
        }
    }
    
      public function edit($id) {
        $row = Faq::find($id);       
        return view('master.faq.edit', compact('row'));
    }
    
     public function update(Request $request) {
        $post = $request->all();
       
        $v = \Validator::make($post,  Faq::rules(), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                             ->withInput($request->input());                        
        } else { 
            $faq = Faq::find($post['f_id']); 
            $faq->fill($post);
            $faq->update();
                    
            if ($faq) {
                Session::flash('message', 'Faq has been Update Successfully');
                return redirect('faq');
            }
        }
    }

    

}

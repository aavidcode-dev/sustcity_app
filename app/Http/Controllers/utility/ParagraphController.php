<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\MassAssignmentException;
use App\Model\Paragraph;
use Illuminate\Support\Facades\File;

class ParagraphController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(Paragraph $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function store($type, Request $request) {
        $post = $request->all();
        $paragraph = Paragraph::where('name', $type)->first();

        if (!isset($post["_token"])) {
            return view('master.paragraph.create', compact('type', 'paragraph'));
        } else {
            if ($paragraph) {
                $paragraph->description = $post['description'];
                $paragraph->save();
            } else {
                Paragraph::create([
                    'name' => $type,
                    'description' => $post['description']
                ]);
            }
            return redirect('paragraph/' . $type);
        }
    }

}

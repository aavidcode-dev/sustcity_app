<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Model\User;

class UserController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(User $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index() {
        $limit = Input::get('limit');         
        $data = User::orderBy('id', 'desc')->paginate($limit);
        $data->setPath('user-details');
        return view('master.user_details.user', compact('data', 'limit', 'post'));
    }
    
     
     public function excel_report() {         
        $users = User::select('name', 'email', 'mobile','verify_code','verify_status','utility_status','full_status','created_at')->get();
        Excel::create('User details', function($excel) use($users) {    
            $excel->sheet('Sheet 1', function($sheet) use($users) {   
                $sheet->fromArray($users);
            }); 
        })->export('xls');        
    }
}

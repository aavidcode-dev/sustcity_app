<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\MassAssignmentException; 
use App\Model\Paragraph;   
use App\Model\Utility;   
use App\Model\Advertisement;
use Illuminate\Support\Facades\File;

class AdvertisementController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(Advertisement $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
     public function index() {     
        $data = Advertisement::orderBy('id','desc')->get();
        return view('master.advertisement.index',compact('data'));
    }
    
     public function create() {       
        return view('master.advertisement.create');
    }
    
     public function store(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, Advertisement::rules(), $this->model->getCustomMessages());
 
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                            ->withInput($request->input());
        } else {

            $faq = Advertisement::create($post);
            if ($faq) { 
                Session::flash('message', 'Advertisement has been Save Successfully');
                return redirect('advertisement');
            }
        }
    }
    
      public function edit($id) {
        $data = Advertisement::find($id);       
        return view('master.advertisement.edit', compact('data'));
    }
    
    public function update(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post,  Advertisement::rules(), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                             ->withInput($request->input());                        
        } else { 
            $advertisement = Advertisement::find($post['a_id']); 
            $advertisement->fill($post);
            $advertisement->update();
                    
            if ($advertisement) {
                Session::flash('message', 'Advertisement has been Update Successfully');
                return redirect('advertisement');
            }
        }
    } 
  }

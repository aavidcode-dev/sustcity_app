<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\Contactus;
use Maatwebsite\Excel\Facades\Excel as Excel;

class ContactusController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(Contactus $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index() {  
        $limit = Input::get('limit');
        $limit = (isset($limit) && $limit != '' && $limit > 1) ? $limit: 15;
        $status_id = Input::get('status_value');
         $post = Input::all();
        $data = Contactus::query();
        if($status_id != null){
            $data = $data->where('status','=',Input::get('status_value'));
        }
        $data = $data->orderBy('id','desc')->paginate($limit);
        return view('master.contact_us.index', compact('data','limit', 'post'));
    }
    
    
    public function excel_report() {        
        $users = Contactus::select('id', 'h_code', 'message','created_at','updated_at')->get();
        Excel::create('Contact us details', function($excel) use($users) {  
            $excel->sheet('Sheet 1', function($sheet) use($users) { 
                $sheet->fromArray($users);
            });
        })->export('xls');        
    }

    public function changeStatus(Request $request)
    {
        $data = $request->all();
        $contact_us_id = $data['id'];
        $contact_us_status = ($data['status'] == 1) ? 0 : 1;
        $contact_us = Contactus::find($contact_us_id);
        $contact_us->status = $contact_us_status;
        $result = $contact_us->save();
        if($result){
            $update_status = 'true';
        }else{
            $update_status = 'false';
        }        
        return $update_status;
    }
}

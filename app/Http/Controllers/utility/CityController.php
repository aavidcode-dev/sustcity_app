<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
 
use App\Model\City; 


class CityController extends Controller { 

    /**
     * Create a new controller instance.
     *
     * @return voiduse Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
     */ 
    private $model; 
 
    public function __construct(City $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
    public function index() {                      
        $data = City::all();              
        return view('master.city.index', compact('data'));
    }
    
    public function create() {
        $data = City::all();        
        return view('master.city.create', compact('data'));
    }
    
    public function store(Request $request) {
        $post = $request->all(); 
       
        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                                     ->withInput($request->input());
           
        } else {
             
            $city = City::create($post);           
            
        if ($city) {
            Session::flash('message', 'City has been Save Successfully');
            return redirect('city');
        }
        }
    }
    
    public function edit($id) {     
        $row = City::find($id);         
        return view('master.city.edit', compact('row')); 
    }

    public function update(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                             ->withInput($request->input());
            
            
        } else { 
            $city = City::find($post['c_id']); 
            $city->fill($post);
            $city->update();
        
            
            if ($city) {
                Session::flash('message', 'City has been Update Successfully');
                return redirect('city');
            }
        }
    }           
}

<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel as Excel;
use Illuminate\Database\Eloquent\MassAssignmentException;
use App\Model\Utility;
use App\Model\UtilityType;
use App\Model\Categories;
use App\Model\UtilityCategory;
use App\Model\Theft;
use App\Model\Notification;
use App\Model\Rewards;
use App\Model\Credit_balance; 
use App\Model\NotificationCode; 
use App\Model\UtilityHouseDetails;
use Illuminate\Support\Facades\File;

class TheftController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(Theft $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index() 
    {        
        $data = Theft::orderBy('id','desc')->get();                       
        return view('master.theft.index', compact('data'));     
    } 
    
    public function store(Request $request)
    {
        $post = $request->all();        

        $theft = Theft::where('id',$post['theftid'])->first(); 
        $theft->is_validated = 1; 
        
        $h_code = $theft->h_code;
        
        $rewards = Rewards::where('code', 'C4')->first();
        if(!empty($rewards) && count($rewards) > 0)
        {
            @$rewards_value = $rewards->value;
        }
        
        $housedetails = UtilityHouseDetails::where('h_code',$h_code)->first(); 
        $housedetails->total_credits = $housedetails->total_credits + @$rewards_value;
        $housedetails->save();  
        
        $credit_data = new Credit_balance();
        $credit_data->h_code = $h_code; 
        $credit_data->type = 'credit';
        $credit_data->value = @$rewards_value;
        $credit_data->spending_type = 'C4';
        $credit_data->save();
        
        $notifications = new Notification;
        $notifications->top_credits_id = 0;
        $notifications->h_code = $h_code;
        $notifications->type = 4;
        $notifications->dmy = date('Y-m-d');
        $notifications->save();
        
        $to_notify_user =  UtilityHouseDetails::with('user')->where('h_code', $h_code)->get();
       
            
        $last_record = Notification::where('h_code', $h_code)->orderBy('id', 'desc')->first();               
        $topcredits_ids = $last_record->type; 
        
        $get_notification_detail = NotificationCode::where('id', $topcredits_ids)->first(); 
        $get_notification_code = $get_notification_detail['notification_code'];
        $get_notification_id = $get_notification_detail['id'];

        $player_id = array($to_notify_user[0]['user']->player_id);

        //$notify_data['credit'] = $get_credit_notify;
        $notify_data['notification_code'] = $get_notification_code;
        $notify_data['id'] = $get_notification_id;
        $notify_data['notification_code_id'] = $get_notification_id;
        
        $ins_notification = new Notification();
        $notify_return = $ins_notification->notification_for_app($player_id,$notify_data);
                                    
        $theft->update(); 
                
        if ($theft) {
            Session::flash('message', 'Theft notification has been Save Successfully');
            return redirect('theft');
        }     
    }
    
     public function excel_report() {        
        $users = Theft::select('id', 'h_code', 'location','description','latitude','longitude','timezone','is_validated','created_at','updated_at')->get();
        Excel::create('Theft Report details', function($excel) use($users) {  
            $excel->sheet('Sheet 1', function($sheet) use($users) { 
                $sheet->fromArray($users);
            });
        })->export('xls');        
    }
}

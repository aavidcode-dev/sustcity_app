<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\Categories; 
use App\Model\UtilityCategory; 

use Maatwebsite\Excel\Facades\Excel as Excel;


class UtilityCategoryController extends Controller {  

    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    private $model;
 
    public function __construct(Categories $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index() {
        $data = Categories::orderBy('id','desc')->get();             
        return view('master.utility_category.index', compact('data')); 
    }
     
     public function create() {
        return view('master.utility_category.create'); 
    }
    
    public function store(Request $request) {
        $post = $request->all(); 
        
        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());
        
        if ($v->fails()) { 
            return redirect()->back()->withErrors($v->errors()); 
        } else {
            
            $utility = Categories::create($post);

            if ($utility) {
                Session::flash('message', 'Categories has been Save Successfully');
                return redirect('utility_category'); 
            }
        }
    }
    
     public function edit($id) {
        $row = Categories::find($id);
        return view('master.utility_category.edit')->with('row', $row);
    }

    public function update(Request $request) { 
        $post = $request->all();

        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $category = Categories::find($post['c_id']);
            $category->fill($post);
            $category->update(); 

            if ($category) {
                Session::flash('message', 'Categories has been Update Successfully');
                return redirect('utility_category'); 
            }
        }
    }
    
     public function uploadexcelSave(Request $request) {
         $post = $request->all();
        
        if (Input::hasFile('uploadexcel')) {
            
            $path = Input::file('uploadexcel')->getRealPath();
            $data = Excel::load($path, function($reader) {
                        
                    })->get();

            if (!empty($data) && $data->count()) {
                $results = $data->toArray();
                foreach ($results as $entry) {
                    if (isset($entry['name'])) {
                        $category = $entry['name'];  
                        $eleConsp = Categories::where('name', $category)->first();
                        $data = [                             
                            'name' => $category
                        ];
                        if ($eleConsp == null) {
                            $insert[] = $data;
                        } else {
                            $eleConsp->fill($data);
                            $eleConsp->save();
                        }
                    }
                }

                if (!empty($insert)) {
                    $result = Categories::insert($insert);

                    if ($result) {
                        Session::flash('message', 'Excel Upload Successfully');
                        return redirect('utility_category');
                    }
                } else {
                    Session::flash('unsuccess_message', 'Data is already uplaoded');
                    return redirect('utility_category');
                }
            }
        }
        return back();
    }
    
     public function downloadXls() {
        $columns[] = ['name' => ''];

        \Excel::create('Category', function($excel) use($columns) {
            $excel->sheet('Sheet1', function($sheet) use($columns) {
                $sheet->fromArray($columns);
            }); 
        })->export('xls');
    } 
    
    public function excel_report() {        
        $users = Categories::select('id', 'name','created_at','updated_at')->get();
        Excel::create('Category details', function($excel) use($users) {  
            $excel->sheet('Sheet 1', function($sheet) use($users) { 
                $sheet->fromArray($users);
            });
        })->export('xls');        
    }
     public function upload() {               
        return view('master.utility_category.upload');
    }
    
    public function disable(Request $request)
    {
        $post = $request->all();  
        $post['status'] = '0';
        $cat = Categories::find($post['u_id']); 
        $cat->fill($post);
        $cat->update();
       
        if ($cat) {
            Session::flash('message', 'Utlity Category has been disable Successfully');
            return redirect('utility_category');
        } 
    }
    
    public function enable(Request $request)
    {
        $post = $request->all();  
        $post['status'] = '1';
        $cat = Categories::find($post['u_id']); 
        $cat->fill($post);
        $cat->update();
       
        if ($cat) {
            Session::flash('message', 'Utlity Category has been enable Successfully');
            return redirect('utility_category');
        }
    }
}

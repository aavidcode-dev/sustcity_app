<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\Utility;
use App\Model\UtilityCategory;
use App\Model\Categories;
use App\Model\ElectricConsumption;
use App\Model\UtilityHouseDetails;
use App\Model\Notification;
use App\Model\User;
use App\Model\NotificationCode;

use Maatwebsite\Excel\Facades\Excel as Excel;

class NewbillNotificationController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(UtilityHouseDetails $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index(Request $request) { 
        $limit = Input::get('limit');
        $post = Input::all();
        
        $utility_name = $request->utility_name;
        $cat_id = $request->cat_id;
        $year = $request->year;
        $month = $request->month;        
       // $limit = Input::get('limit');
        $h_code = $request->h_code;
        $order_at = 'created_at';
        $order_by = 'desc';

        $q = ElectricConsumption::whereNotNull('h_code');

        if ($utility_name)
        {
            $q->whereHas('House_details', function($query) use($utility_name)
            { 
                $query->where('utility_id',$utility_name)->whereNotNull('mobile');
            });           
        }

        if(isset($h_code) && $h_code != '')
        {
           $q->where('h_code',$h_code); 
        }
        
        if ($cat_id)
        {
            $q->whereHas('House_details', function($query) use($cat_id)
            {
                $query->where('cat_id',$cat_id)->whereNotNull('mobile');
            });           
        }
        
        if ($year)
        {
            $q->where('year',$year);
        }

        if ($month)
        {
            $q->where('month',$month);
        }

        $data = $q->orderBy($order_at, $order_by)->get();      
        $data1 = Utility::all();
        $data2 = Categories::all();
        $data3 = ElectricConsumption::select('year')->distinct()->orderBy('year','asc')->get()->toArray();
        $data4 = ElectricConsumption::select('month')->distinct()->orderBy('month','asc')->get()->toArray();
        
        // dd($data3);
        //$data = $data->orderBy('created_at', 'desc')->paginate(10);
        //$data->setPath('bill_notification');
        return view('master.bill_notification.index', compact('data','data1','data2','data3','data4','limit', 'post'));
    } 
    
    public function send_notification(Request $request)
    {
        $post = $request->all();     
        $num_elements = 0;
       
        $sqlData = array();

        while($num_elements < count($post['h_code'])){
            $sqlData[] = array(
                'type'=>'5',
                'dmy'=>date("Y-m-d"),
                'text'=>$post['notificationtext'],
                'h_code'=> $post['h_code'][$num_elements],
                'created_at' => date('Y-m-d H:i:s')
            );
            $num_elements++;
    }
            $to_notify_user =  UtilityHouseDetails::with('user')->where('h_code', $post['h_code'])->get();
            $notification = DB::table('notification')->insert($sqlData);
            
            $last_record = Notification::where('h_code', $post['h_code'])->orderBy('id', 'desc')->first();            
            $topcredits_ids = $last_record->type;
            
            $get_notification_detail = NotificationCode::where('id', $topcredits_ids)->first(); 
//            print_r($get_notification_detail); exit;
            $get_notification_code = $get_notification_detail['notification_code'];
            $get_notification_id = $get_notification_detail['id'];
          
            $player_id = array($to_notify_user[0]['user']->player_id);

           // $notify_data['credit'] = $get_credit_notify;
            $notify_data['notification_code'] = $get_notification_code;   
            $notify_data['id'] = $get_notification_id; 
            $notify_data['notification_code_id'] = $get_notification_id;
            $notify_data['notificationtext'] = $post['notificationtext'];
            
            
            $ins_notification = new Notification();
            $notify_return = $ins_notification->notification_for_app($player_id,$notify_data);
        
      
            if ($notification) {
                Session::flash('message', 'New bill Notification has been sent successfully');
                return redirect('bill_notification'); 
            }       
    }

    public function create() {
        $data1 = Utility::all();
        $data2 = Categories::all();
        return view('master.utility_house_details.create', compact('data1', 'data2'));
    }

    public function store(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, UtilityHouseDetails::rules(), $this->model->getCustomMessages());

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                            ->withInput($request->input());
        } else {
            $hdetails = UtilityHouseDetails::create($post);

            if ($hdetails) {
                Session::flash('message', 'House Details has been save successfully');
                return redirect('housedetails');
            }
        }
    }

    

    public function edit($id) {
        $row = UtilityHouseDetails::find($id);
        $data1 = Utility::all();
        $data2 = Categories::all();
        return view('master.utility_house_details.edit', compact('data1', 'data2', 'row'));
    }

    public function update($id, Request $request) {      
        $post = $request->all();
        $v = \Validator::make($post, UtilityHouseDetails::rules($id), $this->model->getCustomMessages());
        if ($v->fails()) {
             return redirect()->back()->withErrors($v->errors())
                                     ->withInput($request->input());  
        } else {
            $category = UtilityHouseDetails::find($post['u_id']);
            $category->fill($post);
            $category->update();

            if ($category) {
                Session::flash('message', 'House Details has been Update Successfully');
                return redirect('housedetails');
            }
        }
    }

    public function downloadXls() {
        $columns[] = ['h_code' => '','partner' => '', 'name' => '', 'address' => '', 'city' => '' , 'mobile' => '','category_id' => '' ,'utility_id' => ''];
        \Excel::create('House_details', function($excel) use($columns) {
            $excel->sheet('Sheet1', function($sheet) use($columns) {
                $sheet->fromArray($columns);
            });
        })->export('xls');
    }
    
      public function excel_report() {        
        $users = UtilityHouseDetails::select('h_code', 'partner', 'name','address','city','mobile','utility_id','cat_id','user_id','ref_code','is_paper_bill','is_paper_claimed','is_claimed_yes','is_claimed_no','total_credits','created_at','updated_at')->get();
        Excel::create('House details', function($excel) use($users) {
            $excel->sheet('Sheet 1', function($sheet) use($users) {
                $sheet->fromArray($users);
            });
        })->export('xls');        
    }
    
    public function upload() {
        return view('master.utility_house_details.upload');    
    }
    
    public function uploadexcelSave() {
        if (Input::hasFile('uploadexcel')) {
            $path = Input::file('uploadexcel')->getRealPath();
            $data = Excel::load($path, function($reader) {
                        
                    })->get();
            if (!empty($data) && $data->count()) {
                $results = $data->toArray();
                foreach ($results as $entry) {
                    if (isset($entry['h_code'])) {
                        $h_code = $entry['h_code'];
                        $h_details = UtilityHouseDetails::where('h_code', $h_code)->first();
                        $data = [
                            'h_code' => $h_code,
                            'partner' => $entry['partner'],                            
                            'name' => $entry['name'],
                            'address' => $entry['address'],                         
                            'mobile' => $entry['mobile']
                        ];
                        
                         $v = \Validator::make($data, UtilityHouseDetails::rules());

                        if ($v->fails()) {
                            $report['fail'][$h_code] = $v->errors()->toArray();
                        } else {
                            if ($housedetails == null) {
                                $insert[] = $data;
                            } else {
                                $housedetails->fill($data);
                                $housedetails->save();
                            }
                            $report['success'][] = $h_code;
                        }
                        
                        if ($h_details == null) {
                            $utility = Utility::where('id', $entry['utility_id'])->first();
                            if ($utility) {
                                $data['utility_id'] = $utility->id;
                            }

                            $category = Categories::where('name', $entry['category_id'])->first();
                            if (!$category) {
                                $category = Categories::create([
                                            'name' => $entry['category_id']
                                ]);
                            }
                            $data['cat_id'] = $category->id;
                            $insert[] = $data;
                        } else {
                            $h_details->fill($data);
                            $h_details->save();
                        }
                    }
                }
                
                if (!empty($insert)) {
                    $result = UtilityHouseDetails::insert($insert);

                    if ($result) {
                        Session::flash('message', 'Excel Uplaod Successfully',$report);
                        return redirect('housedetails');
                    }
                } else {
                    Session::flash('unsuccess_message', $report);
                    return redirect('housedetails');
                }
            }
        }
        return back();
    }
    
    
}

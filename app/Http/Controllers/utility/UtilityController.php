<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel as Excel;
use Illuminate\Database\Eloquent\MassAssignmentException;
use App\Model\Utility;
use App\Model\UtilityType;
use App\Model\Categories;
use App\Model\UtilityCategory;
use Illuminate\Support\Facades\File;

class UtilityController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(Utility $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index() 
    {
        $post = Input::all();    
        $data = Utility::query();
        
        if (isset($post['utility_name']) && $post['utility_name'] != "") {
            $data->where('utility_name', 'like', '%' . $post['utility_name'] . '%');
        }                    
        if (isset($post['name']) && $post['name'] != '') {
        $p_name = $post['name'];
        $data->whereIn('utility_type_id', function($query) use ($p_name) {
            $query->select('id')
            ->from(with(new UtilityType)->getTable())  
            ->where('name', "like", "%".$p_name."%");
            //->where('active', 1);
        });
        }
        $limit = Input::get('limit');
        $data1 = UtilityType::where('status','1')->get();
        $data2 = Utility::where('status','1')->get();
        $data = $data->orderBy('id', 'desc')->paginate($limit);
        $data->setPath('utility');                        
        return view('master.utility.index', compact('data','data1','data2','limit', 'post'));     
    }

    public function create() {
        $data1 = UtilityType::where('status','1')->get();
        $data2 = Categories::where('status','1')->get();
        return view('master.utility.create', compact('data1', 'data2'));
    }
    
  

    public function store(Request $request) {
        $post = $request->all();
      
        $v = \Validator::make($post, Utility::rules(), $this->model->getCustomMessages());

        if ($v->fails()) {
          
            return redirect()->back()->withErrors($v->errors())
                                     ->withInput($request->input()); 
           
       }else {
            if(Input::file('image_url'))
            {                 
            $image_url = Input::file('image_url');
            $destinationPath = 'resources/assets/images/';
            $post['image_url'] = $image_url->getClientOriginalName();
            //$filename = $post['image_url'];
            
            $filename = substr(number_format(time() * rand(), 0, '', ''), 0, 15)."jpg";
            $image_url->move($destinationPath, $filename);
            
            $post['image_url'] = 'resources/assets/images/' . $filename;
            
            
            $utility = Utility::create($post);
            }else{
                
               $utility = Utility::create($post); 
            }
      
            if ($utility) {
                Session::flash('message', 'Utility has been Save Successfully');
                return redirect('utility');
            }
        }
    }

    public function edit($id) {
        $data1 = Utility::find($id);
              
        $data2 = UtilityType::where('status','1')->get();
        $data3 = Categories::where('status','1')->get();
        return view('master.utility.edit', compact('data1', 'data2', 'data3'));
    }

    public function update($id,Request $request) {
        $post = $request->all();
       
        $v = \Validator::make($post, Utility::rules($id), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {           
           $image_url1 = Input::file('image_url');
           if(Input::file('image_url'))
            {                    
                $image_url = Input::file('image_url');
                $destinationPath = 'resources/assets/images/';
                $post['image_url'] = $image_url->getClientOriginalName();
                $filename = substr(number_format(time() * rand(), 0, '', ''), 0, 15)."jpg";
                $image_url->move($destinationPath, $filename);

                $post['image_url'] = 'resources/assets/images/' . $filename;
            }
            
            $utility = Utility::find($id);
            $utility->fill($post);
            $utility->update();
            
            $uArr = [];
            if (isset($post['cat_id']) && $post['cat_id'] != '') {
                foreach ($post['cat_id'] as $key => $value) {
                    $uArr[] = new UtilityCategory([
                        'cat_id' => $value
                    ]);
                }
            }
            if (sizeof($uArr) > 0) {
                $utility->u_category()->saveMany($uArr);
            }

            if ($utility) {
                Session::flash('message', 'Utility has been Update Successfully');
                return redirect('utility');
            }
        }
    }

    public function view($id) {
        $row = Utility::find($id);
        return view('master.utility.view')->with('row', $row);
    }

    public function uploadexcel() {
        return view('master.utility.uploadexcel');
    }

    public function uploadexcelSave() {
        if (Input::hasFile('uploadexcel')) {
            $path = Input::file('uploadexcel')->getRealPath();
            $data = Excel::load($path, function($reader) {
                        
                    })->get();
            if (!empty($data) && $data->count()) {
                foreach ($data as $key => $value) {
                    foreach ($value as $ddvalue) {
                        $h_code[] = $ddvalue->h_code;
                    }
                }

                $query = DB::table('utility_house_details')->select('h_code')->whereIn('h_code', $h_code)->get();

                $h_data = array();
                foreach ($query as $key => $val) {
                    foreach ($val as $val_new) {
                        $h_data[] = $val_new;
                    }
                }

                foreach ($data as $key => $value) {
                    foreach ($value as $dd) {
                        if (!in_array($dd->h_code, $h_data)) {
                            $insert[] = ['h_code' => $dd->h_code, 'partener' => $dd->partener, 'area' => $dd->area, 'name' => $dd->name, 'address' => $dd->address];
                        }
                    }
                }

                if (!empty($insert)) {
                    $result = UtilityHouseDetails::insert($insert);

                    if ($result) {
                        Session::flash('message', 'Excel Uplaod Successfully');
                        return redirect('uploadexcel');
                    }
                } else {
                    Session::flash('unsuccess_message', 'Data is already uplaoded');
                    return redirect('uploadexcel');
                }
            }
        }
        return back();
    }

    public function elecconsumtion() {
        return view('master.utility.utilityelecon');
    }

    public function utility_year() {
        return view('master.utility_year.index');
    }
    
     public function disable(Request $request)
    {        
        $post = $request->all();  

        $post['status'] = '0';
        $cat = Utility::find($post['u_id']); 
        $cat->fill($post);
        $cat->update();
       
        if ($cat) {
            Session::flash('message', 'Utlity has been disable Successfully');
            return redirect('utility');
        }
    }
    
    public function enable(Request $request)
    {
        $post = $request->all();  
        $post['status'] = '1'; 
        $cat = Utility::find($post['u_id']);  
        $cat->fill($post);
        $cat->update();
       
        if ($cat) {
            Session::flash('message', 'Utlity has been enable Successfully');
            return redirect('utility'); 
        }
    }
}

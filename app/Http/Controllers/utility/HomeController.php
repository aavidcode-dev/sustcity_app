<?php

namespace App\Http\Controllers\utility;
 
use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;  
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Model\User; 

class HomeController extends Controller
{    
    public function forgotPassword(Request $request) {
        $post = $request->all();
        
        $email = $post['email'];
        $user = User::where('email', $email)->first();

        if ($user != null) {
            $password = str_random(6);
            $customer = User::find($user->id);
            $customer->password = bcrypt($password);
            $customer->update();

            if ($customer) {

                $userData = [
                    'email' => $post['email'],
                    'password' => $password
                ];

                \Mail::send('emails.forgotpassword', ['userData' => $userData], function($message) use ($user) {
                    $message->to($user->email)->subject("Sustcity | Forgot Password");
                });
 
                 Session::flash('message', 'Password has been send to your registered email id.');
                 return redirect('login'); 
            }
        } else {
            Session::flash('message', 'Invalid Email Provided');
            return redirect('login'); 
        }
    }

    
}

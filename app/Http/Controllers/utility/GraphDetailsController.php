<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Model\Utility; 
use App\Model\Categories; 
use App\Model\GraphDetail;
use App\Model\Topcredits;

class GraphDetailsController extends Controller { 

    /**
     * Create a new controller instance.
     *
     * @return voiduse Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
     */ 
    private $model; 
 
    public function __construct(Topcredits $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
    public function index() {          
        $post = Input::all();
         
        $data = GraphDetail::orderBy('id','desc')->get();          
        return view('master.graph_details.index', compact('data'));
    }
    
     public function edit($id) {
        $row = GraphDetail::find($id);
        $category = Categories::all();
        $utility =  Utility::all();        
        return view('master.graph_details.edit', compact('category', 'utility','row'));
    }
    
    public function update($id, Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, GraphDetail::rules($id), $this->model->getCustomMessages());
        if ($v->fails()) {

            return redirect()->back()->withErrors($v->errors());
        } else {
            $graph_details = GraphDetail::find($post['g_id']);
            $graph_details->fill($post);
            $graph_details->update();

            if ($graph_details) { 
                Session::flash('message', 'Graph Details has been Update Successfully');
                return redirect('graph_details'); 
            } 
        }
    }
    
    public function downloadXls() {
        $columns[] = ['utility_type_id' => '31','cat_id' => '1', 'month' => '2' ,'year' => '10','dmy' => '2017-02-01','count' => '12','median' => '14','top' => '33'];

        \Excel::create('Graph', function($excel) use($columns) {
            $excel->sheet('Sheet1', function($sheet) use($columns) {
                $sheet->fromArray($columns);
            });  
        })->export('xls');
    }

    public function excel_report() {        
        $users = GraphDetail::select('id', 'cat_id', 'utility_type_id','month','year','dmy','count','median','top','created_at','updated_at')->get();
        Excel::create('Graph details', function($excel) use($users) {  
            $excel->sheet('Sheet 1', function($sheet) use($users) { 
                $sheet->fromArray($users);
            });
        })->export('xls');        
    }
    
    public function upload() {
        return view('master.graph_details.upload');    
    }
    
    public function uploadexcelSave(Request $request) {
        $post = $request->all();
        if (Input::hasFile('uploadexcel')) {

            $path = Input::file('uploadexcel')->getRealPath();
            $data = Excel::load($path, function($reader) {
                
                    })->get();

            $report = [];

            if (!empty($data) && $data->count()) {
                $results = $data->toArray();
                foreach ($results as $entry) {
                    if (isset($entry['cat_id'])) {
                        $h_code = $entry['cat_id'];                       
                        $data = [
                            'cat_id' =>$entry['cat_id'],
                            'utility_type_id' => $entry['utility_type_id'],
                            'month' => $entry['month'],
                            'year' => $entry['year'],
                            'dmy' => $entry['dmy'],
                            'count' => $entry['count'],
                            'median' => $entry['median'],
                            'top' => $entry['top']
                        ];

                        
                            $graphDetails = new GraphDetail();
                            $graphDetails->fill($data);
                            $graphDetails->save();                            
                            $report['success'][] = $h_code;
                        
                    }
                }

                if (!empty($insert)) {
                    $result = GraphDetail::insert($insert);

                    if ($result) {
                        Session::flash('message', 'Excel Upload Successfully');
                        return redirect('graph_details')->with(['report' => $report]);
                    }
                } else {
                    Session::flash('unsuccess_message', $report);
                    return redirect('graph_details');
                }
            }
        }
        return back();
    }    
}
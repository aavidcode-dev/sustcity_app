<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Model\Topcredits; 
use App\Model\UtilityHouseDetails; 
use App\Model\Notification;
use App\Model\ElectricConsumption;

class TopcreditsController extends Controller { 

    /**
     * Create a new controller instance.
     *
     * @return voiduse Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
     */ 
    private $model; 
 
    public function __construct(Topcredits $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
    public function index() {          
        $post = Input::all();
         //dd($post['t_date']);
        $Topcredits = Topcredits::query(); 
        if (isset($post['h_code']) && $post['h_code'] != "") { 
            $Topcredits->where('h_code', 'like', '%' . $post['h_code'] . '%');
        }
        if ((isset($post['t_date']) && $post['t_date'] != "" && $post['t_date'] != "00/00/000") && (isset($post['f_date']) && $post['f_date'] != "" && $post['f_date'] != "00/00/000")) { 
            $from_date = date('Y-m-d H:i:s',strtotime($post['f_date']));
            $till_date = date('Y-m-d H:i:s',strtotime($post['t_date'].' 23:59:59'));
           
            $Topcredits->whereBetween('created_at', [$from_date, $till_date]);
        }
         
        $data = $Topcredits->where('id','!=','null')->orderBy('id', 'desc')->get();    
        // $data = $Topcredits->orderBy('id', 'desc')->paginate($limit);
        $topcredits = Topcredits::all();  
        $top_count = Topcredits::where('is_sent','0')->get();
        return view('master.top_credits.index', compact('data','topcredits','top_count'));
    }
    
    public function create() {
        $data = Topcredits::all();        
        return view('master.top_credits.create', compact('data'));
    }
    
    public function store(Request $request) {
        $post = $request->all(); 
       
        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                                     ->withInput($request->input());
           
        } else {
             
            $topcredits = Topcredits::create($post);           
            
        if ($topcredits) {
            Session::flash('message', 'Top Credits has been Save Successfully');
            return redirect('top-credits');
        }
        }
    }
    
    public function edit($id) {     
        $row = Topcredits::find($id);         
        return view('master.top_credits.edit', compact('row')); 
    }

    public function update(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, Topcredits::rules(), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                             ->withInput($request->input());                        
        } else { 
            $topcredits = Topcredits::find($post['c_id']); 
            $topcredits->fill($post);
            $topcredits->update();
        
            
            if ($topcredits) {
                Session::flash('message', 'Top Credits has been Update Successfully');
                return redirect('top-credits');
            }
        }
    }
    
     public function downloadXls() {
        $columns[] = ['h_code' => 'HOUSECODE1','credit' => '500' ,'dmy' => '2017-02-01'];

        \Excel::create('Topcredits', function($excel) use($columns) {
            $excel->sheet('Sheet1', function($sheet) use($columns) {
                $sheet->fromArray($columns);
            });  
        })->export('xls');
    }

    public function upload() {
        return view('master.top_credits.upload');    
    }
    
    public function uploadexcelSave(Request $request) {
        $post = $request->all();
        if (Input::hasFile('uploadexcel')) {

            $path = Input::file('uploadexcel')->getRealPath();
            $data = Excel::load($path, function($reader) {
                        
                    })->get();

            $report = [];

            if (!empty($data) && $data->count()) {
                $results = $data->toArray();
                foreach ($results as $entry) {
                    if (isset($entry['h_code'])) {
                        $h_code = $entry['h_code'];    
                      
                        $data = [
                            'h_code' => $h_code,
                            'credit' => $entry['credit'],                           
                            'dmy' => $entry['dmy']                           
                        ];

                         $electricConsumption = ElectricConsumption::where('h_code', $h_code)->orderBy('dmy', 'desc')->first();
                         if($electricConsumption->is_top_credit_claimed == 0)
                         {
                            $electricConsumption->is_top_credit_claimed = 1;
                            $electricConsumption->save();
                         } 
                        
                        $eleConsp =  new Topcredits();
                        $eleConsp->fill($data);
                        $eleConsp->save();
                        }
                            $report['success'][] = $h_code;
                        }
                    }
                


                    if ($eleConsp) {
                        Session::flash('message', 'Excel Upload Successfully');
                        return redirect('top-credits')->with(['report' => $report]);
                    }
               
            }
    }
          
    public function excel_report() {        
       $users = Topcredits::select('id', 'h_code', 'credit','is_claimed','dmy','is_sent','created_at','updated_at')->get();
       Excel::create('Top SR Credits details', function($excel) use($users) {  
           $excel->sheet('Sheet 1', function($sheet) use($users) { 
               $sheet->fromArray($users);
           });
       })->export('xls');        
    }
    
    public function update_notification(Request $request)
    {         
        $topcredits = Topcredits::where('is_sent','1')->get();            
        
        if (!empty($topcredits) && count($topcredits) > 0)
        {
            foreach ($topcredits as $topcredits_ids)
            {
               $to_notify_user =  UtilityHouseDetails::with('user')->where('h_code', $topcredits_ids->h_code)->get();
               $get_player_id_notify = $to_notify_user[0]['user']->id;
               $get_credit_notify = $topcredits_ids->credit;

                if ($topcredits_ids != null)
                {
                    $ins_notification = new Notification();
                    $ins_notification->top_credits_id = $topcredits_ids->id;
                    $ins_notification->h_code = $topcredits_ids->h_code;
                    $ins_notification->type = 1;
                    $ins_notification->dmy = date('Y-m-d');
                    $save_notification = $ins_notification->save();                   
                                        
                    $topcredits_upd = Topcredits::find($topcredits_ids->id);
                    $topcredits_upd->is_sent = 0;
                    $topcredits_upd->sent_date = date('Y-m-d');
                    $topcredits_upd->update(); 

                    if($save_notification)
                    {
                        $get_notification_detail = Notification::with('notification_code')->where('top_credits_id', $topcredits_ids->id)->where('type', 1)->get();
                        $get_notification_code = $get_notification_detail[0]['notification_code']->notification_code;
                        $get_notification_code_id = $get_notification_detail[0]['notification_code']->id;
                        $get_notification_id = $get_notification_detail[0]->id;
                        //dd($to_notify_user[0]['user']->player_id);
                        $player_id = array($to_notify_user[0]['user']->player_id);
                        //$player_id = array('f55fa3c2-ee30-46b8-8b5b-d99d7be95d64');

                        $notify_data['credit'] = $get_credit_notify;
                        $notify_data['notification_code'] = $get_notification_code;
                        $notify_data['id'] = $get_notification_id;
                        $notify_data['notification_code_id'] = $get_notification_code_id;
                        //$notification = new Notification();
                        $notify_return = $ins_notification->notification_for_app($player_id,$notify_data);
                        //dd($notify_return);                        
                    }
                    
                }
            }
            //dd($notify_data,$player_id);
            Session::flash('message_notification', 'Notifications Sent Successfully');
            return redirect('top-credits');           
        }   
        else 
        {
            Session::flash('message_notification', 'Notifications Already Sent');
            return redirect('top-credits');   
        }
    }
}
<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\MassAssignmentException; 
use App\Model\Paragraph;    
use App\Model\Utility;   
use App\Model\Aboutus;
use Illuminate\Support\Facades\File;

class AboutusController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(Aboutus $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
     public function index() {     
        $data = Aboutus::all();       
        return view('master.about_us.index',compact('data'));
    }
    
    public function store(Request $request) {
       $post = $request->all();

        $v = \Validator::make($post,  Aboutus::rules($id), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                             ->withInput($request->input());                        
        } else { 
            $aboutus = Aboutus::find($post['a_id']); 
            $aboutus->fill($post);
            $aboutus->update();
                               
            $aboutus_id = $post['a_id'];
            $image_url = Input::file('logo');
            $destinationPath = 'resources/assets/images/' . $aboutus_id . '/';
            $filename = $image_url->getClientOriginalName();
            $image_url->move($destinationPath, $filename);
            
            if ($aboutus) {
                Session::flash('message', 'About us has been Update Successfully');
                return redirect('about_us');
            }
        }
    }
    
    
    
    public function update($id, Request $request) {
        $post = $request->all();

        $v = \Validator::make($post,  Aboutus::rules($id), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                             ->withInput();                        
        } else { 
            
            if(Input::file('logo'))
            {     
            $aboutus_id = $post['a_id']; 
            $image_url = Input::file('logo');
            $destinationPath = 'resources/assets/images/' . $aboutus_id . '/';
            $post['logo'] = $image_url->getClientOriginalName();
            $filename = $post['logo'];
            $image_url->move($destinationPath, $filename);
            
            $post['logo_url'] = 'resources/assets/images/' . $aboutus_id . '/';
            
            
            $aboutus = Aboutus::find($post['a_id']); 
            $aboutus->fill($post);
            $aboutus->update();
            }else{
                $aboutus = Aboutus::find($post['a_id']); 
                $aboutus->fill($post);
                $aboutus->update();
            }
            
            
            if ($aboutus) {
                Session::flash('message', 'About us has been Update Successfullyy'); 
                return redirect('about_us');
            }
        }
    } 
  }

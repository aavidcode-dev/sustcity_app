<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Model\Topcredits; 
use App\Model\UtilityHouseDetails; 
use App\Model\Notification;
use App\Model\Knowmore;

class KnowmoreController extends Controller { 

    /**
     * Create a new controller instance.
     *
     * @return voiduse Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
     */ 
    private $model; 
 
    public function __construct(Knowmore $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
    public function index() {          
        $post = Input::all();
         
        $konw_more = Knowmore::query(); 
        if (isset($post['h_code']) && $post['h_code'] != "") { 
            $konw_more->where('h_code', 'like', '%' . $post['h_code'] . '%');
        }
         
        $data = $konw_more->where('id','!=','null')->orderBy('id','desc')->get();          
        $konw_more = Knowmore::all();           
        return view('master.knowmore.index', compact('data','konw_more'));
    }            
    
    public function excel_report() {        
        $users = Knowmore::select('id', 'h_code', 'mobile','created_at','updated_at')->get();
        Excel::create('Know more details', function($excel) use($users) {  
            $excel->sheet('Sheet 1', function($sheet) use($users) { 
                $sheet->fromArray($users); 
            });
        })->export('xls');        
    }   
}
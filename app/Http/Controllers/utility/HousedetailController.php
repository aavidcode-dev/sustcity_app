<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\UtilityHouseDetails;
use App\Model\Utility;
use App\Model\UtilityCategory;
use App\Model\Categories;
use Maatwebsite\Excel\Facades\Excel as Excel;

class HousedetailController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(UtilityHouseDetails $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index() {
        $limit = Input::get('limit');

        $post = Input::all();
        $data = UtilityHouseDetails::query();
        if (isset($post['h_code']) && $post['h_code'] != "") {
            $data->where('h_code', 'like', '%' . $post['h_code'] . '%');
        }      
        if (isset($post['name']) && $post['name'] != "") {   
            $data->where('name', 'like', '%' . $post['name'] . '%'); 
        } 
        $data1 = Utility::all();
        $data = $data->orderBy('id', 'desc')->paginate($limit);
        $data->setPath('housedetails');
        return view('master.utility_house_details.index', compact('data','data1', 'limit', 'post'));
    } 

    public function create() {
        $data1 = Utility::all();
        $data2 = Categories::all();
        return view('master.utility_house_details.create', compact('data1', 'data2'));
    }

    public function store(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, UtilityHouseDetails::rules(), $this->model->getCustomMessages());

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                            ->withInput($request->input());
        } else {
            $hdetails = UtilityHouseDetails::create($post);

            if ($hdetails) {
                Session::flash('message', 'House Details has been save successfully');
                return redirect('housedetails');
            }
        }
    }

    

    public function edit($id) {
        $row = UtilityHouseDetails::find($id);
        $data1 = Utility::all();
        $data2 = Categories::all();
        return view('master.utility_house_details.edit', compact('data1', 'data2', 'row'));
    }

    public function update($id, Request $request) {      
        $post = $request->all();
        $v = \Validator::make($post, UtilityHouseDetails::rules($id), $this->model->getCustomMessages());
        if ($v->fails()) {
             return redirect()->back()->withErrors($v->errors())
                                     ->withInput($request->input());  
        } else {
            $category = UtilityHouseDetails::find($post['u_id']);
            $category->fill($post);
            $category->update();

            if ($category) {
                Session::flash('message', 'House Details has been Update Successfully');
                return redirect('housedetails');
            }
        }
    }

    public function downloadXls() {
        //$columns[] = ['h_code' => '','partner' => '', 'name' => '', 'address' => '', 'city' => '' , 'mobile' => '','category_id' => '' ,'utility_id' => ''];
       // $columns[] = ['h_code' => '','partner' => '', 'name' => '', 'address' => '', 'city' => '' , 'mobile' => '','category_id' => '' ,'utility_id' => ''];        
        $columns = array(
            array('h_code' => 'HOUSECODE1','partner' => '434', 'name' => 'ram', 'address' => '202-dahisar accord, mumbai', 'city' => 'mumbai' , 'mobile' => '9999999999','category_id' => '1' ,'utility_id' => '31')
              
        );
             
       
        \Excel::create('House_details', function($excel) use($columns) {
            $excel->sheet('Sheet1', function($sheet) use($columns) {
                $sheet->fromArray($columns);
            });
        })->export('xls');
    }
    
      public function excel_report() {        
        $users = UtilityHouseDetails::select('h_code', 'partner', 'name','address','city','mobile','utility_id','cat_id','user_id','ref_code','is_paper_bill','is_paper_claimed','is_claimed_yes','is_claimed_no','total_credits','created_at','updated_at')->get();
        Excel::create('House details', function($excel) use($users) {
            $excel->sheet('Sheet 1', function($sheet) use($users) {
                $sheet->fromArray($users);
            });
        })->export('xls');        
    }
    
    public function upload() {
        return view('master.utility_house_details.upload');    
    }
    
    public function uploadexcelSave() {
        if (Input::hasFile('uploadexcel')) {
            $path = Input::file('uploadexcel')->getRealPath();
            $data = Excel::load($path, function($reader) {
                        
                    })->get();
            if (!empty($data) && $data->count()) {
                $results = $data->toArray();
                foreach ($results as $entry) {
                    if (isset($entry['h_code'])) {
                        $h_code = $entry['h_code'];
                        $h_details = UtilityHouseDetails::where('h_code', $h_code)->first();
                        $data = [
                            'h_code' => $h_code,
                            'partner' => $entry['partner'],                            
                            'name' => $entry['name'],
                            'address' => $entry['address'],                         
                            'mobile' => $entry['mobile'],
                            'city' => $entry['city']
                        ];
                        
                         $v = \Validator::make($data, UtilityHouseDetails::rules());

                        if ($v->fails()) {
                            $report['fail'][$h_code] = $v->errors()->toArray();
                        } else {
                            if ($housedetails == null) {
                                $insert[] = $data;
                            } else {
                                $housedetails->fill($data);
                                $housedetails->save();
                            }
                            $report['success'][] = $h_code;
                        }
                        
                        if ($h_details == null) {
                            $utility = Utility::where('id', $entry['utility_id'])->first();
                            if ($utility) {
                                $data['utility_id'] = $utility->id;
                            }

                            $category = Categories::where('name', $entry['category_id'])->first();
                            if (!$category) {
                                $category = Categories::create([
                                            'name' => $entry['category_id']
                                ]);
                            }
                            $data['cat_id'] = $category->id;
                            $insert[] = $data;
                        } else {
                            $h_details->fill($data);
                            $h_details->save();
                        }
                    }
                }
                
                if (!empty($insert)) {
                    $result = UtilityHouseDetails::insert($insert);

                    if ($result) {
                        Session::flash('message', 'Excel Uplaod Successfully',$report);
                        return redirect('housedetails');
                    }
                } else {
                    Session::flash('unsuccess_message', $report);
                    return redirect('housedetails');
                }
            }
        }
        return back();
    }

    public function discontinuepaperbill() {

        $limit = !empty(Input::get('limit')) ? Input::get('limit'): 15;
        $post = Input::all();
        $data = utilityhousedetails::with('utility')->where('is_paper_bill', '=', 0);
        if(isset($post['name']) && $post['name'] != ""){
            $data->where('name', 'like', '%' . $post['name'] . '%');
        }
        if(isset($post['mobile']) && $post['mobile'] != ""){
            $data->where('mobile', '=', $post['mobile']);
        }
        if (isset($post['h_code']) && $post['h_code'] != "") {
            $data->where('h_code', 'like', '%' . $post['h_code'] . '%');
        }
        $data = $data->orderBy('id', 'desc')->paginate($limit);
        //dd($data);
        //dd( utilityhousedetails::with('utility')->where('name', 'like', 'Pratik')->paginate($limit));
        return view('master.utility_house_details.discontinuepaperbill', compact('data', 'limit', 'post'));
    }
    
    
}

<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
 
use App\Model\Rewards; 


class RewardsController extends Controller { 

    /**
     * Create a new controller instance.
     *
     * @return voiduse Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
     */ 
    private $model; 
 
    public function __construct(Rewards $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
    public function index() {                      
        $data = Rewards::orderBy('id','desc')->get();            
        return view('master.rewards.index', compact('data'));
    }
    
    public function create() {              
        return view('master.rewards.create', compact('data'));
    }  
    
    public function store(Request $request) {
        $post = $request->all(); 
       
        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                                     ->withInput($request->input());
           
        } else {
             
        $rewards = Rewards::create($post);           
            
        if ($rewards) {
            Session::flash('message', 'Rewards has been Save Successfully');
            return redirect('rewards');
        }
        }
    }
    
    public function edit($id) {     
        $row = Rewards::find($id);         
        return view('master.rewards.edit', compact('row')); 
    }

    public function update(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                             ->withInput($request->input());
            
            
        } else { 
            $rewards = Rewards::find($post['r_id']); 
            $rewards->fill($post);
            $rewards->update();
        
            
            if ($rewards) {
                Session::flash('message', 'Rewards has been Update Successfully');
                return redirect('rewards');
            }
        }
    }           
}

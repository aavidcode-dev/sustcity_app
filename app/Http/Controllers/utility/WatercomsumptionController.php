<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Model\Watercomsumption; 
use App\Model\UtilityType;
use App\Model\Categories;
use App\Model\Utility; 


class WatercomsumptionController extends Controller { 

    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    private $model; 
 
    public function __construct(Watercomsumption $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
    public function index() {                      
//        $data = Watercomsumption::all();
//        $data1 = Utility::all();
//        return view('master.water_comsumption.index', compact('data','data1'));
                      
        $limit = Input::get('limit');   
        $post = Input::all();
        $data = Watercomsumption::query();
         if (isset($post['h_code']) && $post['h_code'] != "") {
            $data->where('h_code', 'like', '%' . $post['h_code'] . '%');
        }      
        if (isset($post['utility_year']) && $post['utility_year'] != "") {
            $data->where('utility_year', 'like', '%'.$post['utility_year'].'%');
        } 
        
         if (isset($post['name']) && $post['name'] != '') {
            $p_name = $post['name'];
            $data->whereIn('utility_id', function($query) use ($p_name) {
                $query->select('id')
                ->from(with(new Utility)->getTable())
                ->where('utility_name', "like", "%".$p_name."%");
                //->where('active', 1);
            });
        }         
        
        $data = $data->orderBy('created_at', 'desc')->paginate($limit);
        $data1 = Utility::all();
        $data2 = Categories::all();
        $data->setPath('water-comsumption'); 
        return view('master.water_comsumption.index', compact('data','data1','data2','limit','post'));         
    }
    
    public function create() {
        $data = Watercomsumption::all();  
        $data1 = Utility::all(); 
        $data2 = Categories::all();
        return view('master.water_comsumption.create', compact('data','data1','data2'));
    }
    
    public function store(Request $request) {
        $post = $request->all(); 
       
        $v = \Validator::make($post, Watercomsumption::rules(), $this->model->getCustomMessages());

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                                     ->withInput($request->input());           
        } else {             
            $temperature = Watercomsumption::create($post);                       
        if ($temperature) {
            Session::flash('message', 'Water Comsumption has been Save Successfully');
            return redirect('water-comsumption');
        }
        }
    }
    
//        public function search() {
//        $post = Input::all();
//        $data = Watercomsumption::query();
//        if (isset($post['h_code']) && $post['h_code'] != "") {
//            $data->where('h_code', 'like', '%'.$post['h_code'].'%');
//        }
//        if (isset($post['utility_year']) && $post['utility_year'] != "") {
//            $data->where('utility_year', 'like', '%'.$post['utility_year'].'%');
//        } 
//        
//         if (isset($post['name']) && $post['name'] != '') {
//            $p_name = $post['name'];
//            $data->whereIn('utility_id', function($query) use ($p_name) {
//                $query->select('id')
//                ->from(with(new Utility)->getTable())
//                ->where('utility_name', "like", "%".$p_name."%");
//                //->where('active', 1);
//            });
//        }
//        
//        $limit = ''; 
//        $data = $data->paginate($limit);
//        $data1 = Utility::all();
//        $data2 = Categories::all();
//        $data->setPath('water-comsumption');  
//        return view('master.water_comsumption.index', compact('data','data1','data2' ,'limit', 'post'));
//    }
    
    public function edit($id) {     
        $row = Watercomsumption::find($id);      
         $data1 = Utility::all(); 
        $data2 = Categories::all();
        return view('master.water_comsumption.edit', compact('row','data1','data2'));
    }

    public function update($id,Request $request) {
        $post = $request->all();  
  
        $v = \Validator::make($post, Watercomsumption::rules($id), $this->model->getCustomMessages());
        if ($v->fails()) { 
            return redirect()->back()->withErrors($v->errors())
                             ->withInput($request->input()); 
                        
        } else { 
            $water_com = Watercomsumption::find($post['e_id']); 
            $water_com->fill($post);
            $water_com->update();
        
            
            if ($water_com) {
                Session::flash('message', 'Water Comsumption has been Update Successfully');
                return redirect('water-comsumption'); 
            }
        }
    }
    
    public function uploadexcelSave(Request $request)
    {
        $post = $request->all();
        if (Input::hasFile('uploadexcel')) {
            $path = Input::file('uploadexcel')->getRealPath();
            $data = Excel::load($path, function($reader) {
                    })->get();

            if (!empty($data) && $data->count()) {
                $results = $data->toArray();
                foreach ($results as $entry) {
                    if (isset($entry['h_code'])) {
                        $h_code = $entry['h_code'];
                        $eleConsp = Watercomsumption::where('h_code', $h_code)->where('utility_year', $entry['utility_year'])->first();
                        $data = [
                            'bp' => 'ss',
                            'rank' => $entry['rank'],
                            'h_code' => $entry['h_code'],
                            'january' => $entry['january'],
                            'february' => $entry['february'],
                            'march' => $entry['march'],
                            'april' => $entry['april'],
                            'may' => $entry['may'],
                            'june' => $entry['june'],
                            'july' => $entry['july'],
                            'august' => $entry['august'],
                            'september' => $entry['september'],
                            'october' => $entry['october'],
                            'november' => $entry['november'],
                            'december' => $entry['december'],
                            'utility_year' => $entry['utility_year'],
                             'utility_id' => $post['utility_id']
                        ];
                        if ($eleConsp == null) {
                            $insert[] = $data;
                        } else {
                            $eleConsp->fill($data);
                            $eleConsp->save();
                        }
                    }
                }

                if (!empty($insert)) {
                    $result = Watercomsumption::insert($insert);

                    if ($result) {
                        Session::flash('message', 'Excel Upload Successfully');
                        return redirect('water-comsumption');
                    }
                } else {
                    Session::flash('unsuccess_message', 'Data is already uplaoded');
                    return redirect('water-comsumption');
                }
            }
        }
        return back();
    }
    
      public function downloadXls() {
        $columns[] = ['bp' => '','rank' => '', 'h_code' => '', 'january' => '', 'february' => '', 'march' => '', 'april' => '', 'may' => '', 'june' => '', 'july' => '', 'august' => '', 'september' => '', 'october' => '', 'november' => '', 'december' => '', 'utility_year' => ''];

        \Excel::create('Water_consumption', function($excel) use($columns) {
            $excel->sheet('Sheet1', function($sheet) use($columns) {
                $sheet->fromArray($columns);
            });
        })->export('xls');
    }
    
     public function upload() {        
        $data = Utility::all();        
        return view('master.water_comsumption.upload', compact('data'));
    }
}

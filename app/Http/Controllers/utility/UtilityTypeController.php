<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\UtilityType;

use Maatwebsite\Excel\Facades\Excel as Excel;


class UtilityTypeController extends Controller { 

    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    private $model;
 
    public function __construct(UtilityType $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index() {
        $data = UtilityType::orderBy('id','desc')->get();           
        return view('master.utility_type.index', compact('data')); 
    }
      
     public function create() {
        return view('master.utility_type.create'); 
    }
    
    public function store(Request $request) {
        $post = $request->all(); 
        
        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());
        
        if ($v->fails()) { 
            return redirect()->back()->withErrors($v->errors()); 
        } else {
            
            $utility = UtilityType::create($post);

            if ($utility) {
                Session::flash('message', 'Utility Type has been Save Successfully');
                return redirect('utility_type');
            }
        }
    }
    
     public function edit($id) {
        $row = UtilityType::find($id);
        return view('master.utility_type.edit')->with('row', $row);
    }

    public function update(Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, $this->model->getRules(), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $category = UtilityType::find($post['u_id']);
            $category->fill($post);
            $category->update();

            if ($category) {
                Session::flash('message', 'Utility Type has been Update Successfully');
                return redirect('utility_type');
            }
        }
    }
    
    public function disable(Request $request)
    {        
        $post = $request->all();  

        $post['status'] = '0';
        $cat = UtilityType::find($post['u_id']); 
        $cat->fill($post);
        $cat->update();
       
        if ($cat) {
            Session::flash('message', 'Utlity Type has been disable Successfully');
            return redirect('utility_type');
        }
    }
    
    public function enable(Request $request)
    {
        $post = $request->all();  
        $post['status'] = '1';
        $cat = UtilityType::find($post['u_id']); 
        $cat->fill($post);
        $cat->update();
       
        if ($cat) {
            Session::flash('message', 'Utlity Type has been enable Successfully');
            return redirect('utility_type'); 
        }
    }
    
}

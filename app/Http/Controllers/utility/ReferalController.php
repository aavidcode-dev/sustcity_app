<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Model\Topcredits; 
use App\Model\UtilityHouseDetails; 
use App\Model\Referrals;

class ReferalController extends Controller { 

    /**
     * Create a new controller instance.
     *
     * @return voiduse Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
     */ 
    private $model; 
 
    public function __construct(Referrals $model) {
        $this->middleware('auth');
        $this->model = $model;
    } 
    
    public function index() {          
        $post = Input::all();
        
        $data = Referrals::query();
        if (isset($post['h_code']) && $post['h_code'] != "") {
            $data->where('h_code', 'like', '%' . $post['h_code'] . '%');
        }   
        $data = $data->where('id','!=','null')->orderBy('id','desc')->get();                 
        return view('master.referal.index', compact('data'));
    }
    
    public function excel_report() {        
        $users = Referrals::select('id', 'referee_mobile', 'referred_mobile','ref_code','created_at','updated_at')->get();
        Excel::create('Referrals details', function($excel) use($users) {  
            $excel->sheet('Sheet 1', function($sheet) use($users) { 
                $sheet->fromArray($users);
            });
        })->export('xls');        
    }
}
<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Auth\Access\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\ElectricConsumption;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Model\UtilityType;
use App\Model\Categories;
use App\Model\Utility;

class ElectricconsumptionController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;

    public function __construct(ElectricConsumption $model) {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index() {
        $limit = Input::get('limit');

        $post = Input::all();
        $data = ElectricConsumption::query();
        if (isset($post['h_code']) && $post['h_code'] != "") {
            $data->where('h_code', 'like', '%' . $post['h_code'] . '%');
        }
        if (isset($post['year']) && $post['year'] != "") {
            $data->where('year', 'like', '%' . $post['year'] . '%');
        }

        if (isset($post['name']) && $post['name'] != '') {
            $p_name = $post['name'];
            $data->whereIn('utility_id', function($query) use ($p_name) {
                $query->select('id')
                        ->from(with(new Utility)->getTable())
                        ->where('utility_name', "like", "%" . $p_name . "%");
                //->where('active', 1);
            });
        }

        $data = $data->orderBy('id', 'desc')->paginate($limit);
        $data1 = Utility::all();
        $data2 = Categories::all();
        $data->setPath('electricconsumption');
        return view('master.utility_electric_consumption.index', compact('data', 'data1', 'data2', 'limit', 'post'));
    }

    public function create() {
        $data = ElectricConsumption::all();
        $data1 = Utility::all();
        $data2 = Categories::all();
        return view('master.utility_electric_consumption.create', compact('data', 'data1', 'data2'));
    }

    public function store(Request $request) {

        $post = $request->all();

        $v = \Validator::make($post, ElectricConsumption::rules(), $this->model->getCustomMessages());
 
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                            ->withInput($request->input());
        } else {

            $temperature = ElectricConsumption::create($post);
            if ($temperature) {
                Session::flash('message', 'Electric Comsumption has been Save Successfully');
                return redirect('electricconsumption');
            }
        }
    }

    public function search() {
        $post = Input::all();
        $data = ElectricConsumption::query();
        if (isset($post['h_code']) && $post['h_code'] != "") {
            $data->where('h_code', 'like', '%' . $post['h_code'] . '%');
        }
        if (isset($post['utility_year']) && $post['utility_year'] != "") {
            $data->where('utility_year', 'like', '%' . $post['utility_year'] . '%');
        }

        if (isset($post['name']) && $post['name'] != '') {
            $p_name = $post['name'];
            $data->whereIn('utility_id', function($query) use ($p_name) {
                $query->select('id')
                        ->from(with(new Utility)->getTable())
                        ->where('utility_name', "like", "%" . $p_name . "%");
                //->where('active', 1);
            });
        }

        $limit = '';
        $data = $data->paginate($limit);
        $data1 = Utility::all();
        $data2 = Categories::all();
        $data->setPath('electricconsumption');
        return view('master.utility_electric_consumption.index', compact('data', 'data1', 'data2', 'limit', 'post'));
    }

    public function uploadexcelSave(Request $request) {
        $post = $request->all();
        if (Input::hasFile('uploadexcel')) {

            $path = Input::file('uploadexcel')->getRealPath();
            $data = Excel::load($path, function($reader) {
                        
                    })->get();

            $report = [];

            if (!empty($data) && $data->count()) {
                $results = $data->toArray();
                foreach ($results as $entry) {
                    if (isset($entry['h_code'])) {
                        $h_code = $entry['h_code'];
                        $eleConsp = ElectricConsumption::where('h_code', $h_code)->where('year', $entry['year'])->first();
                        $data = [
                            'h_code' => $h_code,
                            'curr_reading' => $entry['curr_reading'],
                            'pre_reading' => $entry['pre_reading'],
                            'unit' => $entry['unit'],
                            'ep_amount' => $entry['ep_amount'],
                            'ed_amount' => $entry['ed_amount'],
                            'mh_amount' => $entry['mh_amount'],
                            'fix_amount' => $entry['fix_amount'],
                            'other_amount' => $entry['other_amount'],
                            'total_amt' => $entry['total_amt'],
                            'avg_cost_per_unit' => $entry['avg_cost_per_unit'],
                            'rank' => $entry['rank'],
                            'month' => $entry['month'],
                            'year' => $entry['year'],
                            'sp_units' => $entry['sp_units'],
                            'sp_cost' => $entry['sp_cost'],
                            'dmy' => date('Y-m-d', strtotime($entry['year'].'-'.$entry['month'].'-01'))
                        ];

                        $v = \Validator::make($data, ElectricConsumption::rules());

                        if ($v->fails()) {
                            $report['fail'][$h_code] = $v->errors()->toArray();
                        } else {
                            if ($eleConsp == null) {
                                $insert[] = $data;
                            } else {
                                $eleConsp->fill($data);
                                $eleConsp->save();
                            }
                            $report['success'][] = $h_code;
                        }
                    }
                }

                if (!empty($insert)) {
                    $result = ElectricConsumption::insert($insert);

                    if ($result) {
                        Session::flash('message', 'Excel Upload Successfully');
                        return redirect('electricconsumption')->with(['report' => $report]);
                    }
                } else {
                    Session::flash('unsuccess_message', $report);
                    return redirect('electricconsumption');
                }
            }
        }
        return back();
    }

    public function edit($id) {
        $row = ElectricConsumption::find($id);
        $data1 = Utility::all();
        $data2 = Categories::all();
        return view('master.utility_electric_consumption.edit', compact('row', 'data1', 'data2'));
    }

    public function update($id, Request $request) {
        $post = $request->all();

        $v = \Validator::make($post, ElectricConsumption::rules($id), $this->model->getCustomMessages());
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {
            $category = ElectricConsumption::find($post['u_id']);
            $category->fill($post);
            $category->update();
            
            if ($category) {
                Session::flash('message', 'Electric Consumption has been Update Successfully');
                return redirect('electricconsumption');
            }
        }
    }

    public function downloadXls() {
        $columns[] = ['h_code' => 'housecode1', 'curr_reading' => '434', 'pre_reading' => '34', 'unit' => '50', 'ep_amount' => '455', 'ed_amount' => '545', 'mh_amount' => '876', 'fix_amount' => '43','other_amount' => '', 'total_amt' => '687', 'avg_cost_per_unit' => '6', 'rank' => '12', 'month' => '2', 'year' => '2017', 'sp_units' => '15', 'sp_cost' => '83'];
        
        \Excel::create('Electric_consumption', function($excel) use($columns) {
            $excel->sheet('Sheet1', function($sheet) use($columns) {
                $sheet->fromArray($columns);
            });
        })->export('xls');
        
        
 
    }
    public function excel_report() {        
        $users = ElectricConsumption::select('id', 'h_code', 'curr_reading', 'unit','ep_amount','ed_amount','mh_amount','fix_amount','other_amount','total_amt','avg_cost_per_unit','rank','month','year','sp_cost','sp_units','dmy','created_at','updated_at')->get();
        Excel::create('Electric_consumption', function($excel) use($users) {
            $excel->sheet('Sheet 1', function($sheet) use($users) {
                $sheet->fromArray($users);
            });
        })->export('xls');        
    }

    public function upload() {
        $data = Utility::all();
        return view('master.utility_electric_consumption.upload', compact('data'));
    }

}

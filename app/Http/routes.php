<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

// return view('welcome');
Route::get('/sms_test', function(Request $request) {
    $mobile = 919689326397;
    $message = "Your mobile verification code is 34534";
    $sms = new \App\Helpers\Sms();
    $resp = $sms->send($mobile, $message);
});

Route::get('test', function() {
    $dt = DateTime::createFromFormat('!m', 3);
    echo $dt->format('M') . '<br>';
    $dt = DateTime::createFromFormat('Y', 2016);
    echo $dt->format('y') . '<br>';
});

Route::get('/mac_test', function(Request $request) {
    echo $hashedpassword = bcrypt('demo123');
});

Route::get('/', 'HomeController@index');


Route::group(array('prefix' => 'auth/'), function() {
    Route::get('logout', 'Auth\AuthController@getLogout');
});

Route::group(array('prefix' => 'utility/'), function() {
    Route::get('{id}/view', 'utility\UtilityController@view');  
    Route::resource('disable', 'utility\UtilityController@disable');   
    Route::resource('enable', 'utility\UtilityController@enable');  
});

Route::group(array('prefix' => 'utility_category/'), function() {
    Route::resource('uploadexcelSave', 'utility\UtilityCategoryController@uploadexcelSave');
    Route::resource('downloadXls', 'utility\UtilityCategoryController@downloadXls');
    Route::resource('upload', 'utility\UtilityCategoryController@upload');
    Route::resource('excel_report', 'utility\UtilityCategoryController@excel_report'); 
    Route::resource('disable', 'utility\UtilityCategoryController@disable'); 
    Route::resource('enable', 'utility\UtilityCategoryController@enable'); 
});

Route::group(array('prefix' => 'utility_type/'), function() {  
    Route::resource('disable', 'utility\UtilityTypeController@disable'); 
    Route::resource('enable', 'utility\UtilityTypeController@enable'); 
});

Route::group(array('prefix' => 'top-credits/'), function() {
    Route::resource('uploadexcelSave', 'utility\TopcreditsController@uploadExcelSave');
    Route::resource('downloadXls', 'utility\TopcreditsController@downloadXls');
    Route::resource('upload', 'utility\TopcreditsController@upload'); 
    Route::resource('update_notification', 'utility\TopcreditsController@update_notification');
    Route::resource('excel_report', 'utility\TopcreditsController@excel_report'); 
});
Route::group(array('prefix' => 'housedetails/'), function() {
    Route::resource('uploadexcelSave', 'utility\HousedetailController@uploadExcelSave');
    Route::resource('downloadXls', 'utility\HousedetailController@downloadXls');
    Route::resource('upload', 'utility\HousedetailController@upload');
    Route::resource('excel_report', 'utility\HousedetailController@excel_report');
});
Route::group(array('prefix' => 'electricconsumption/'), function() {
    Route::resource('uploadexcelSave', 'utility\ElectricconsumptionController@uploadExcelSave');
    Route::resource('downloadXls', 'utility\ElectricconsumptionController@downloadXls');
    Route::resource('search', 'utility\ElectricconsumptionController@search');
    Route::resource('upload', 'utility\ElectricconsumptionController@upload');
    Route::resource('excel_report', 'utility\ElectricconsumptionController@excel_report');
});

Route::group(array('prefix' => 'water-comsumption/'), function() {
    Route::resource('uploadexcelSave', 'utility\WatercomsumptionController@uploadExcelSave');
    Route::resource('downloadXls', 'utility\WatercomsumptionController@downloadXls');
    Route::resource('search', 'utility\WatercomsumptionController@search');
    Route::resource('upload', 'utility\WatercomsumptionController@upload');
});

Route::group(array('prefix' => 'temperature/'), function() {
    Route::resource('uploadcity', 'utility\TemperatureController@uploadcity');
    Route::resource('downloadXls', 'utility\TemperatureController@downloadXls');
    Route::resource('search', 'utility\TemperatureController@search');
    Route::resource('upload', 'utility\TemperatureController@upload');
});

Route::group(array('prefix' => 'graph_details/'), function() {
    Route::resource('uploadcity', 'utility\GraphDetailsController@uploadcity');
    Route::resource('downloadXls', 'utility\GraphDetailsController@downloadXls');
    Route::resource('upload', 'utility\GraphDetailsController@upload');
     Route::resource('uploadexcelSave', 'utility\GraphDetailsController@uploadexcelSave');    
});

Route::group(array('prefix' => 'contact_us/'), function() {
    Route::resource('excel_report', 'utility\ContactusController@excel_report'); 
    Route::resource('status', 'utility\ContactusController@changeStatus');  
});

//Route::post('contact_us/status', 'utility\ContactusController@changeStatus');

Route::group(array('prefix' => 'theft/'), function() {
    Route::resource('excel_report', 'utility\TheftController@excel_report');    
});

Route::group(array('prefix' => 'referal/'), function() {
    Route::resource('excel_report', 'utility\ReferalController@excel_report');    
});

Route::group(array('prefix' => 'graph_details/'), function() {
    Route::resource('excel_report', 'utility\GraphDetailsController@excel_report');    
});

Route::group(array('prefix' => 'user-details/'), function() {  
    Route::resource('excel_report', 'utility\UserController@excel_report'); 
});
Route::group(array('prefix' => 'know_more/'), function() {  
    Route::resource('excel_report', 'utility\KnowmoreController@excel_report'); 
});


Route::group(array('prefix' => 'bill_notification/'), function() {  
    Route::resource('send_notification', 'utility\NewbillNotificationController@send_notification'); 
});



    Route::resource('utility', 'utility\UtilityController');
    Route::resource('uploadexcel', 'utility\UtilityController@uploadExcel');
    Route::resource('elecconsumtion', 'utility\UtilityController@elecconsumtion');
    Route::resource('housedetails', 'utility\HousedetailController');
    Route::resource('electricconsumption', 'utility\ElectricconsumptionController');
    //Discontiue paper bill
    Route::resource('discontinue-paper-bill', 'utility\HousedetailController@discontinuepaperbill');

    Route::resource('utility_category', 'utility\UtilityCategoryController');
    Route::resource('utility_type', 'utility\UtilityTypeController');
    Route::resource('temperature', 'utility\TemperatureController');
    Route::resource('water-comsumption', 'utility\WatercomsumptionController');
    Route::resource('city', 'utility\CityController');
    Route::resource('faq', 'utility\FaqController');
    Route::resource('contact_us', 'utility\ContactusController');

    Route::resource('advertisement', 'utility\AdvertisementController'); 
    Route::resource('about_us', 'utility\AboutusController'); 
    Route::resource('theft', 'utility\TheftController'); 
    Route::resource('referal', 'utility\ReferalController');
    Route::resource('rewards', 'utility\RewardsController');
    Route::resource('graph_details', 'utility\GraphDetailsController');
    
    Route::resource('change-password', 'HomeController@changePassword');
    Route::resource('updatepassword', 'HomeController@updatePassword');
    Route::resource('forgotpassword', 'utility\HomeController@forgotPassword');
    Route::resource('user-details', 'utility\UserController');
    Route::resource('top-credits', 'utility\TopcreditsController');
    Route::resource('bill_notification', 'utility\NewbillNotificationController');
    Route::resource('know_more', 'utility\KnowmoreController');
    
    Route::auth();

    Route::get('/home', 'HomeController@index');

     

/** API * */
Route::group(array('prefix' => 'api/v1/'), function() {
    Route::group(array('prefix' => 'auth/'), function() { 
        Route::post('login', 'Api\AuthController@postLogin');
        Route::post('register', 'Api\AuthController@postRegister');
        Route::post('verified', 'Api\AuthController@verify');
    });
    Route::get('utilities', 'Api\ApiController@getUtilities');
    Route::get('getSubscriptions', 'Api\ApiController@getSubscriptions');
    Route::post('subscribUser', 'Api\ApiController@subscribUser');
    Route::post('verifyDashboard', 'Api\ApiController@verifyDashboard'); 
    Route::get('electric_usage/{h_code}', 'Api\AuthController@electric_usage');
    Route::get('energyConsumedashboard/{h_code}', 'Api\ApiController@energyConsumedashboard');
    Route::get('resendVerificationCode/{mobile}', 'Api\AuthController@resendVerificationCode');  
    Route::post('Updatetheftreports', 'Api\ApiController@Updatetheftreports');
    Route::get('getReferalcode/{h_code}/{mobile}', 'Api\ApiController@getReferalcode');
    Route::get('paperBill/{h_code}/{mobile}', 'Api\ApiController@paperBill');
    Route::post('updatepaperBill', 'Api\ApiController@updatepaperBill');
    Route::get('sendsms/{h_code}', 'Api\ApiController@sendsms');
    Route::get('is_paper_claimed/{h_code}/{mobile}', 'Api\ApiController@is_paper_claimed');
    Route::get('discontinue_paper_bill_amount', 'Api\ApiController@discontinue_paper_bill_amount');
    Route::get('nameUpdate/{h_code}/{name}', 'Api\ApiController@nameUpdate');
    
    
    Route::get('graph_details/{h_code}/{limit}', 'Api\ApiController@getGraphDetails');
    Route::get('getAccountdetails/{h_code}', 'Api\ApiController@getAccountdetails');   
    Route::post('updateAccountdetails', 'Api\ApiController@updateAccountdetails');   
    Route::get('getClaimdetails/{h_code}/{notification_id?}', 'Api\ApiController@getClaimdetails');  
    Route::get('getNotification/{h_code}', 'Api\ApiController@getNotification'); 
    Route::get('theftAmount', 'Api\ApiController@theftAmount'); 
    
    
    Route::post('updateClaimdetails', 'Api\ApiController@updateClaimdetails');
    Route::post('updateReferal', 'Api\ApiController@updateReferal');
    Route::get('aboutUs', 'Api\ApiController@aboutUs');
    Route::get('Faq', 'Api\ApiController@Faq'); 
    Route::get('Add/{h_code}/{mobile}', 'Api\ApiController@Add'); 
    Route::post('updateClaimed_y', 'Api\ApiController@updateClaimed_y');    
    Route::post('updateClaimed_n', 'Api\ApiController@updateClaimed_n');    
    Route::post('Contactus', 'Api\ApiController@Contactus');  
    Route::post('spendMoney', 'Api\ApiController@spendMoney'); 
    Route::get('send_email/{email}', 'Api\ApiController@send_email');
    
    Route::get('Name/{h_code}/{mobile}', 'Api\ApiController@Name');  
    
    Route::get('water_usage/{h_code}', 'Api\AuthController@water_usage');
    Route::get('electric_details/{h_code}', 'Api\ApiController@electric_details');
    Route::resource('update_profile', 'Api\ApiController@update_profile');
    Route::resource('forget_password', 'Api\ApiController@forget_password'); 
});
/** API **/

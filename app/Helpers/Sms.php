<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helpers;

class Sms {

    private $domain = '';
    private $uname = '';
    private $pwd = '';
    private $is_send = '';

    public function __construct() {
        $config = app('config')['services.sms'];
        $this->domain = $config['domain'];
        $this->uname = $config['username'];
        $this->pwd = $config['password'];
        $this->is_send = $config['is_send'];
    }

    public function send($mobile, $text, $is_mul = false) {
        if ($this->is_send)
        {
            $text = urlencode($text);
            $mobile = $is_mul ? $mobile : $this->formatNumber($mobile);
            $parameters = "ID=" . $this->uname . "&Pwd=" . $this->pwd . "&PhNo=$mobile&Text=$text";
            $url = $this->domain . "/SMS.aspx?$parameters";
            $fp = fopen($url, "r");
            stream_get_contents($fp); 
            return $url;

//            $seekermobile=$jobseekers['emp_mobile'];
//            $msg="Dear Candidate, .".$job_cmp_name.". is looking .".$jobrolename.". like u, for more details logon www.venkymama.com / www.lifemadeeasyglobal.com";
//            $msg=urlencode($msg);
//            $sms_file="http://tra.bulksmshyderabad.co.in/websms/sendsms.aspx?userid=$user&password=$password&sender=atmm&mobileno=".$seekermobile."&msg=$msg.";
//            $sms_h=fopen($sms_file,"r");
//            fclose($sms_h);

//            $text = urlencode($text);
//            $mobile = $is_mul ? $mobile : $this->formatNumber($mobile);
//            $parameters = "userid=" . $this->uname . "&password=" . $this->pwd . "&sender=BALLEB&mobileno=$mobile&msg=$text";
//            $url = $this->domain . "/websms/sendsms.aspx?$parameters";
//            $fp = fopen($url, "r");
//            stream_get_contents($fp);
//            return $url;
        }
        return '';
    }

    public function formatNumber($mobile) {
        $len = strlen($mobile);
        if ($len == 10) {
            return '91' . $mobile;
        } else if ($len > 10) {
            return '91' . substr($mobile, -10);
        }
        return $mobile;
    }

}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
//use Illuminate\Foundation\Inspiring;

class HappyBirthday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display an inspiring quote';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
   public function handle()
    {
      $post = 'sksuthar09@gmail.com';
        
        $email = $post['email'];
        $user = User::where('email', $email)->first();

        if ($user != null) {
            $password = str_random(6);
            $customer = User::find($user->id);
            $customer->password = bcrypt($password);
            $customer->update();

            if ($customer) {

                $userData = [
                    'email' => $post['email'],
                    'password' => $password
                ];

                \Mail::send('master.emails.forgotpassword', ['userData' => $userData], function($message) use ($user) {
                    $message->to($user->email)->subject("Sustcity | Forgot Password");
                });
 
                 Session::flash('message', 'Password has been changed successfully');
                 return redirect('login'); 
            }
        } else {
            Session::flash('message', 'Invalid Email Provided');
            return redirect('login'); 
        }

    }
}

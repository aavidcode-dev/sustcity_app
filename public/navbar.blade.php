<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- search form -->
        <!--<form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="customer/home">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
                </a>
            </li>
            <li class="treeview active">                
                <ul class="treeview-menu menu-open" style="display: block;">
                    <li class="treeview"><a href="#"><i class="fa fa-circle-o"></i> <span>Place order</span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="customer/products/ready/current"><i class="fa fa-circle-o"></i>Ready</a></li>
                            <li><a href="customer/products/future"><i class="fa fa-circle-o"></i>Future</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </section><!-- /.sidebar -->
</aside>
$(function () {
    $('body').on('keydown', '.numbers-only', function (event) {
        // Prevent shift key since its not needed
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        // Allow Only: keyboard 0-9, numpad 0-9, backspace, tab, left arrow, right arrow, delete
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 ||
                event.keyCode == 9 ||
                event.keyCode == 37 ||
                event.keyCode == 39 ||
                event.keyCode == 46 ||
                ($(this).data('decimal') && (event.keyCode == 190 || event.keyCode == 110) && $(this).val().indexOf('.') === -1)) {
            // Allow normal operation
        } else {
            // Prevent the rest
            event.preventDefault();
        }
    });

    $('body').on('blur', '.numbers-only', function (event) {
        var val = $(this).val();
        if ($(this).data('skip-zero')) {
            if (parseFloat(val) === 0) {
                $(this).val('').focus();
                return false;
            }
        }

        if (val !== '') {
            var dec = ($(this).data('decimal-len') != undefined ? $(this).data('decimal-len') : 2);
            if ($(this).data('decimal')) {
                $(this).val(parseFloat(val).toFixed(dec));
            }
        }
    });

    activateNavBar();

    function activateNavBar() {
        var url = window.location.href;
        var ref_url = url.replace(BASE_URL, '');
        $('.sidebar-menu li ul li').each(function () {
            $(this).find('ul li a').each(function () {
                var obj = $(this);
                var href = obj.attr('href');
                if (href == ref_url) {
                    obj.addClass('nav-active');
                    obj.closest('li.treeview').addClass('active');
                }
            });
        });
    }
});